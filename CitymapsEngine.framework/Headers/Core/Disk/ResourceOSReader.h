/*
 * FileOSReader.h
 *
 *  Created on: Jul 15, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/Core/Disk/ResourceOSReader.h>

namespace citymaps
{
	template<typename T>
	class ResourceOSReader
	{
        static TByteArray LoadResource(const std::string& file) { return TByteArray(); }
        static TByteArray LoadImageResource(const std::string& file, Size& outSize, float& outScale) { return TByteArray(); }
        static std::string GetCacheDirectory() { return std::string(); }
	};
}
