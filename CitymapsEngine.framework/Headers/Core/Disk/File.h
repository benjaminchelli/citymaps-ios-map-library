/*
 * File.h
 *
 *  Created on: Jul 15, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	class File
	{
    public:
        
        /**
         * Create a new File object.
         *
         * This constructor will not open the file at filePath until an operation is performed
         * on File.
         */
        File(const std::string& filePath);
        
        ~File();

        bool Exists();
        bool Load();
        bool Write(const TByteArray &data);
        bool Write(const byte *data, size_t size);
        bool Save();
        bool SaveAs(const std::string &filepath);
        
        static bool Exists(const std::string& file);
        static bool SaveAs(const TByteArray& data, const std::string& filepath);
        static bool SaveAs(const void* data, size_t count, const std::string& filepath);
        static void Delete(const std::string& file);
        static uint64_t AgeSinceLastModified(const std::string& file);
        
        void Delete();
        uint64_t GetAgeSinceLastModified();
        
        const std::string& StringData();
        TByteArray& Data() { return mData;}
        const TByteArray& Data() const { return mData;}
        const std::string& FilePath() const {return mFilePath;}
        
        static std::string GetCacheDirectory();

        static bool CreateDirectory(const std::string& dir);
        
    private:
        
        TByteArray mData;
        std::string mFilePath;
        std::string mStringData;
	};
}
