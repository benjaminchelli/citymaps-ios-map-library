//
//  ImageResource.h
//  vectormap2
//
//  Created by Eddie Kimmel on 7/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <string>

#include <CitymapsEngine/CitymapsEngine.h>

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	class ImageResource
	{
    public:
        
        ImageResource(const std::string& file, bool loadNow = true);
        
        ~ImageResource();
        
        bool Load();
        
        const TByteArray& Data() const
        {
            return mData;
        }
        
        const std::string& GetFilePath() const
        {
            return mFilePath;
        }
        
        const Size& GetImageSize() const
        {
        	return mSize;
        }
        
        const float GetImageScale() const
        {
            return mImageScale;
        }

    private:
        
        TByteArray mData;
        std::string mFilePath;
        Size mSize;
        float mImageScale;
	};
}
