//
//  Application.cpp
//  vectormap2
//
//  Created by Adam Eskreis on 6/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//
#include <rapidxml/rapidxml.hpp>
#include <iostream>
#include <string>

#include <CitymapsEngine/Core/Graphics/Effect.h>
#include <CitymapsEngine/Core/Graphics/Technique.h>
#include <CitymapsEngine/Core/Graphics/Pass.h>
#include <CitymapsEngine/Core/Input/InputReader.h>
#include <CitymapsEngine/Core/Input/TouchListener.h>
#include <CitymapsEngine/Core/Input/GPS/GPSReader.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Font/FontManager.h>
#include <CitymapsEngine/Core/Util/UnicodeUtil.h>
#include <CitymapsEngine/Core/Network/HTTPConnection.h>

#include <CitymapsEngine/Core/Threading/Dispatcher.h>

namespace citymaps
{

template <typename T>
    Application<T>::Application() :
        mInputReader(NULL),
        mGPSReader(NULL),
        mDevice(NULL),
        mDeviceContext(NULL),
        mPreviousUpdateTime(0),
        mFontsLoaded(false)
    {
    }
    
    template <typename T>
    Application<T>::~Application()
    {
        delete mInputReader;
        delete mGPSReader;
    }
    
    template <typename T>
    bool Application<T>::Initialize(int width, int height, int immediateThreads, int longTermThreads)
    {
        // Init Input and GPS Reader
    	if (!mInputReader)
    		mInputReader = new InputReader();
    	
    	if (!mGPSReader)
    		mGPSReader = new GPSReader();
        
        // Load Cache Directory
        std::string cacheDir = File::GetCacheDirectory();
        std::string networkDir = cacheDir;
        networkDir.append("Network/");
        File::CreateDirectory(networkDir);
        
        if (!mFontsLoaded)
        {
            mFontsLoaded = true;
            // Load System Fonts if needed
            FontManager::LoadSystemFont(T::Name);
        }
        
        //UnicodeUtil::LoadUnicodeCaseMap("unicode_case_map.dat");
        
        HTTPConnection::Initialize();
        
        return true;
    }
    
    template <typename T>
    void Application<T>::Resize(int width, int height)
    {
        DispatchOnMainThread([=]()
       {
           auto sharedListener = mListener.lock();
           if (!mListener.expired())
           {
               sharedListener->OnResize(width, height);
           }
           
           if (mDevice)
               mDevice->Resize(Size(width, height));
           this->Invalidate();

       });
    }
    
    template <typename T>
    void Application<T>::SetNetworkStatus(NetworkStatus status)
    {
        DispatchOnMainThread([=]()
           {
               if (NetworkConnectivity::GetCurrentStatus() != status)
               {
                   NetworkConnectivity::UpdateStatus(status);
                   auto sharedListener = mListener.lock();
                   if (sharedListener && !mListener.expired())
                   {
                       sharedListener->OnNetworkStatusChanged(this, status);
                   }
                   this->Invalidate();
               }
           });
    }
    
    template <typename T>
    void Application<T>::SetNetworkCacheEnabled(bool enabled)
    {
        HTTPConnection::SetNetworkCacheEnabled(enabled);
    }
    
    template <typename T>
    void Application<T>::RunMainLoop(bool run)
    {
        mRunMainLoop = run;
    }
    
    template <typename T>
    void Application<T>::OnTouchStart(const std::vector<Point>& touches)
    {
    	if (touches.size() && mInputReader)
    	    mInputReader->ProcessTouchEvent(TouchEventStart, touches);
    }
    
    template <typename T>
    void Application<T>::OnTouchMove(const std::vector<Point>& touches)
    {
    	if (touches.size() && mInputReader)
    	    mInputReader->ProcessTouchEvent(TouchEventMove, touches);
    }
    
    template <typename T>
    void Application<T>::OnTouchEnd(const std::vector<Point>& touches)
    {
    	if (touches.size() && mInputReader)
    	    mInputReader->ProcessTouchEvent(TouchEventEnd, touches);
    }
    
    template <typename T>
    void Application<T>::OnTouchCancel(const std::vector<Point>& touches)
    {
        if (touches.size() && mInputReader)
            mInputReader->ProcessTouchEvent(TouchEventCancel, touches);
    }
    
    template <typename T>
    void Application<T>::OnPositionChanged(double longitude, double latitude, double accuracyMeters)
    {
    	Point point(longitude, latitude);
		if (mGPSReader)
	        mGPSReader->PositionChanged(point, accuracyMeters);
    }
    
    template <typename T>
    void Application<T>::OnDirectionChanged(double direction)
    {
    	if (mGPSReader)
            mGPSReader->DirectionChanged(direction);
    }
    
    template <typename T>
    void Application<T>::OnAltitudeChanged(double altitude)
    {
    	if (mGPSReader)
            mGPSReader->AltitudeChanged(altitude);
    }
    
    template <typename T>
    void Application<T>::OnLocationStatusChanged(LocationStatus status)
    {
        if (mGPSReader)
            mGPSReader->LocationStatusChanged(status);
    }

    template <typename T>
    void Application<T>::OnWindowDestroyed()
    {
        if (mDevice)
            mDevice->OnWindowDestroyed();
        mPreviousUpdateTime = 0;
    }
    
    template <typename T>
    void Application<T>::Step()
    {
    	this->Update();
    	this->Render();
    }
    
    template <typename T>
    void Application<T>::Update()
    {
        // This is how we tell that we're on the first frame of this application.
        if (mPreviousUpdateTime < 0.1)
        {
            Dispatcher::SetThisAsMainThread();
            mPreviousUpdateTime = Util::GetSystemTime();
        }
        
        double now = Util::GetSystemTime();
        double delta = now - mPreviousUpdateTime;
        mCurrentStepMS = delta * 1000;
        mPreviousUpdateTime = now;
        
        std::vector<DispatchCallback> callbacks;
        
        Dispatcher::ProcessSynchronousCallbacks();
        
        mInputReader->Update();
        
        auto sharedListener = mListener.lock();
        if (mRunMainLoop && !mListener.expired())
        {
            sharedListener->OnUpdate(this, mCurrentStepMS);
        }
        
        Dispatcher::WaitForImmediateJobs();
    }
    
    template <typename T>
    void Application<T>::Render()
    {
        if (mPreviousUpdateTime < 0.1)
            mPreviousUpdateTime = Util::GetSystemTime();
        
        double now = Util::GetSystemTime();
        double delta = now - mPreviousUpdateTime;
        int msSinceLastUpdate = delta * 1000;
        
    	bool wasValid = this->IsValid(true);
        
    	if (!wasValid)
    	{
            mDevice->PreRender();
        
        	auto sharedListener = mListener.lock();
        	if (mRunMainLoop && !mListener.expired())
        	{
        		sharedListener->OnRender(this, msSinceLastUpdate);
        	}
        	
            mDevice->PostRender();
    	}
    }
    
    template <typename T>
    void Application<T>::AddTouchListener(std::shared_ptr<ITouchListener> listener)
    {
        if(mInputReader) {
            mInputReader->AddTouchListener(listener);
        }
    }
    
    template <typename T>
    void Application<T>::AddGPSListener(std::shared_ptr<IGPSListener> listener)
    {
        if (mGPSReader) {
            mGPSReader->AddListener(listener);
            listener->OnPositionChange(mGPSReader->GetLastPosition());
            listener->OnDirectionChange(mGPSReader->GetLastDirection());
            listener->OnAltitudeChange(mGPSReader->GetLastAltitude());
        }
    }
    
    template <typename T>
    void Application<T>::RemoveTouchListener(std::shared_ptr<ITouchListener> listener)
    {
        if(mInputReader) {
            mInputReader->RemoveTouchListener(listener);
        }
    }
    
    template <typename T>
    void Application<T>::RemoveGPSListener(std::shared_ptr<IGPSListener> listener)
    {
        if (mGPSReader) {
            mGPSReader->RemoveListener(listener);
        }
    }
    
    template <typename T>
    void Application<T>::SetGraphicsDevice(IGraphicsDevice *device)
    {
        mDevice = device;
        if (mDevice != NULL)
        	mDeviceContext = device->GetContext();
    }
    
    template <typename T>
    void Application<T>::LoadShaderConfig(const std::string &resourceName)
    {
        Resource shadersXml(resourceName);
        const TByteArray& data = shadersXml.Data();
        if(data.size())
        {
            rapidxml::xml_document<> doc;
            doc.parse<0>((char*)&data[0]);
            
            rapidxml::xml_node<> *root = doc.first_node("ShaderConfig");
            
            rapidxml::xml_node<> *shadersNode = root->first_node("Shaders");
            rapidxml::xml_node<> *shaderNode = shadersNode->first_node("Shader");
            
            while (shaderNode)
            {
                if (shaderNode->type() == rapidxml::node_element)
                {
                    std::string name(shaderNode->first_attribute("name")->value());
                    // Don't reload the same shader.
                    if (mDeviceContext->ContainsShader(name))
                    {
                       shaderNode = shaderNode->next_sibling();
                        continue;
                    }
                    std::string filename(shaderNode->first_attribute("filename")->value());
                    std::string typeName(shaderNode->first_attribute("type")->value());
                    
                    ShaderType type = ShaderTypeVertexShader;
                    if (typeName.compare("vertex") == 0) {
                        type = ShaderTypeVertexShader;
                    } else if (typeName.compare("pixel") == 0) {
                        type = ShaderTypePixelShader;
                    } else if (typeName.compare("geometry") == 0) {
                        type = ShaderTypeGeometryShader;
                    } else if (typeName.compare("domain") == 0) {
                        type = ShaderTypeDomainShader;
                    } else if (typeName.compare("hull") == 0) {
                        type = ShaderTypeHullShader;
                    } else if (typeName.compare("compute") == 0) {
                        type = ShaderTypeComputeShader;
                    }

                    IShader *shader = mDevice->CreateShader(filename, type);
                    
                    rapidxml::xml_node<> *globalDataNode = shaderNode->first_node("GlobalData");
                    
                    while (globalDataNode)
                    {
                        if (globalDataNode->type() == rapidxml::node_element)
                        {
                            std::string globalData(globalDataNode->first_attribute("name")->value());
                            int index = mDeviceContext->RegisterGlobalData(globalData);
                            shader->AddGlobalData(index, globalData);
                            
                        }
                        globalDataNode = globalDataNode->next_sibling();
                    }
                    
                    shader->Load();
                    mDeviceContext->AddShader(name, shader);
                }
                
                shaderNode = shaderNode->next_sibling();
            }
            
            rapidxml::xml_node<> *shaderProgramsNode = root->first_node("ShaderPrograms");
            rapidxml::xml_node<> *shaderProgramNode = shaderProgramsNode->first_node("ShaderProgram");
            
            while (shaderProgramNode)
            {
                if (shaderProgramNode->type() == rapidxml::node_element)
                {
                    std::string programName(shaderProgramNode->first_attribute("name")->value());
                    
                    // Don't load the same shader program more than once.
                    if (mDeviceContext->ContainsShaderProgram(programName))
                    {
                        shaderProgramNode = shaderProgramNode->next_sibling();
                        continue;
                    }
                    
                    IShaderProgram *program = mDevice->CreateShaderProgram();
                    
                    rapidxml::xml_node<> *shadersNode = shaderProgramNode->first_node("Shaders");
                    rapidxml::xml_node<> *shaderNode = shadersNode->first_node("Shader");
                    while (shaderNode)
                    {
                        if (shaderNode->type() == rapidxml::node_element)
                        {
                            std::string name(shaderNode->first_attribute("name")->value());
                            
                            IShader *shader = mDeviceContext->GetShader(name);
                            if (shader)
                            {
                                program->SetShader(shader);
                            }
                        }
                        
                        shaderNode = shaderNode->next_sibling();
                    }
                    
                    InputLayout *inputLayout = new InputLayout();
                    
                    rapidxml::xml_node<> *inputLayoutNode = shaderProgramNode->first_node("InputLayout");
                    rapidxml::xml_node<> *inputElementNode = inputLayoutNode->first_node("InputElement");
                    while (inputElementNode) {
                        if (inputElementNode->type() == rapidxml::node_element) {
                            std::string name(inputElementNode->first_attribute("name")->value());
                            int size = std::atoi(inputElementNode->first_attribute("size")->value());
                            int offset = std::atoi(inputElementNode->first_attribute("offset")->value());
                            
                            inputLayout->AddElement(name, size, offset);
                        }
                        
                        inputElementNode = inputElementNode->next_sibling();
                    }
                    
                    program->SetInputLayout(inputLayout);
                    
                    program->Build();
                    
                    mDeviceContext->AddShaderProgram(programName, program);
                }
                
                shaderProgramNode = shaderProgramNode->next_sibling();
            }
        }
    }
    
    template <typename T>
    void Application<T>::LoadEffectsConfig(const std::string &resourceName)
    {
        Resource effectsXml(resourceName);
        const TByteArray& data = effectsXml.Data();
        
        if(data.size())
        {
            rapidxml::xml_document<> doc;
            doc.parse<0>((char*)&data[0]);
            
            rapidxml::xml_node<> *root = doc.first_node("EffectConfig");
            rapidxml::xml_node<> *effectsNode = root->first_node("Effect");
            
            while (effectsNode)
            {
                if (effectsNode->type() == rapidxml::node_element)
                {
                    
                    std::string effectName(effectsNode->first_attribute("name")->value());
                    if (mDeviceContext->ContainsEffect(effectName))
                    {
                        effectsNode = effectsNode->next_sibling();
                        continue;
                    }
                    
                    Effect* effect = new Effect(effectName);
                    
                    rapidxml::xml_node<> *techniqueNode = effectsNode->first_node("Technique");
                    
                    while (techniqueNode)
                    {
                        if (techniqueNode->type() == rapidxml::node_element)
                        {
                            std::string techniqueName(techniqueNode->first_attribute("name")->value());
                            
                            Technique* technique = new Technique(techniqueName);
                            
                            rapidxml::xml_node<> * passNode = techniqueNode->first_node("Pass");
                            
                            while (passNode)
                            {
                                if (passNode->type() == rapidxml::node_element)
                                {
                                    std::string program(passNode->first_attribute("program")->value());
                                    
                                    IShaderProgram* shaderProgram = mDeviceContext->GetShaderProgram(program);
                                    
                                    if (!shaderProgram)
                                    {
                                        std::stringstream ss;
                                        ss << "Shader Program not found: " << program;
                                        
                                        throw Exception(InvalidOperationException, "Effects Config", ss.str().c_str());
                                        return;
                                    }
                                    Pass* pass = new Pass(shaderProgram);
                                    
                                    technique->AddPass(pass);
                                } // If passnode type
                                
                                passNode = passNode->next_sibling();
                            } // While passnode
                            
                            effect->AddTechnique(technique);
                        } // if technique type
                        
                        techniqueNode = techniqueNode->next_sibling();
                    } // while technique
                    
                    mDeviceContext->AddEffect(effectName, effect);
                    
                } // if effect type
                
                effectsNode = effectsNode->next_sibling();
            } // while effect
        } // if data.size()
    }
};