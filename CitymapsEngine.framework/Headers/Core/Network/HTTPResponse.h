//
//  HTTPResponse.h
//  vectormap2
//
//  Created by Lion User on 05/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct HTTPStatus
    {
        HTTPStatus() :
            code(0), version(""), description("") {}
        
        int code;
        std::string version;
        std::string description;
    };
    
    struct HTTPHeader
    {
        HTTPHeader() :
            field(""), value("") {}
        HTTPHeader(std::string _field, std::string _value) :
            field(_field), value(_value) {}
        
        std::string field;
        std::string value;
    };
    
    typedef std::vector<HTTPHeader> THeaderList;
    
    class HTTPResponse
    {
        friend class HTTPResponseParser;
        friend class HTTPConnectionImpl;
    public:
        HTTPResponse() :
            mResponseData(new TByteArray()),
            mResponseBody(new TByteArray())
        {}
        
        HTTPResponse(std::shared_ptr<TByteArray> responseData, std::shared_ptr<TByteArray> responseBody) :
            mResponseData(responseData),
            mResponseBody(responseBody)
        {}
        
        ~HTTPResponse() {}
        
        int GetStatusCode() { return mStatus.code; }
        std::shared_ptr<TByteArray> GetResponseBody() const { return mResponseBody; }
        std::shared_ptr<TByteArray> GetResponseData() const { return mResponseData; }
        const std::string& GetURL() const { return mURL;}
        
        bool FindHeader(std::string field, HTTPHeader *outHeader);
        
        void Clear();
        bool Inflate();
        
    private:
        
        THeaderList mHeaders;
        HTTPStatus mStatus;
        std::shared_ptr<TByteArray> mResponseBody;
        std::shared_ptr<TByteArray> mResponseData;
        std::string mURL;
        
        void SetStatus(HTTPStatus status) { mStatus = status; }
        void AddHeader(std::string field, std::string value)
        {
            mHeaders.push_back(HTTPHeader(field, value));
        }
        void AddHeader(HTTPHeader header) { mHeaders.push_back(header); }
    };
};
