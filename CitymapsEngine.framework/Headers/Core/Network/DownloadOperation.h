//
//  DownloadOperation.h
//  vectormap2
//
//  Created by Lion User on 07/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/OperationQueue.h>
#include <CitymapsEngine/Core/Network/HTTPConnectionImpl.h>

namespace citymaps
{
    class SocketConnection;
    class SocketConnectionPool;
    class FileCache;
    class HTTPResponse;
    
    class DownloadOperation : public Operation
    {
    public:
        DownloadOperation(HTTPConnectionImpl *connection, const HTTPRequest &request, THTTPSuccessCallback success, THTTPFailureCallback failure, double timeout);
        ~DownloadOperation();
        
        void Start();
        void Cancel();
        void ClearCallbacks()
        {
            mSuccessCallback = NULL;
            mFailureCallback = NULL;
        }
        
    private:
        HTTPConnectionImpl *mConnection;
        HTTPRequest mRequest;
        THTTPSuccessCallback mSuccessCallback;
        THTTPFailureCallback mFailureCallback;
        int mCurrentTry;
        double mTimeout;
        
        bool CacheResponse(HTTPResponse* response);
    };
};

