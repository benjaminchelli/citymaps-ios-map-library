//
//  HTTPRequest.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Network/URL.h>

namespace citymaps
{
    class HTTPRequest
    {
    public:
        HTTPRequest(const URL &url, const std::string &method = "GET", const std::string &query = "") :
            mURL(url),
            mMethod(method),
            mQuery(query)
        {
        }
        
        HTTPRequest(const std::string &url, const std::string &method = "GET", const std::string &query = "") :
            HTTPRequest(URL(url), method, query)
        {
        }
        
        ~HTTPRequest()
        {
        }
        
        void AddHeader(const std::string &name, const std::string &value) { mHeaders[name] = value; }
        void AddPostField(const std::string &name, const std::string &value) { mPostFields[name] = value; }
        
        void SetMethod(const std::string &method) { mMethod = method; }
        const std::string& GetMethod() const { return mMethod;}
        
        std::string GetRequestString() const;
        
        const URL& GetURL() const { return mURL; }
        const std::string GetURLString() const { return mURL.GetURL(); }
        
        void SetRedirectAttempt(uint32_t attempt) {mRedirectAttempt = attempt;}
        uint32_t GetRedirectAttempt() {
            return mRedirectAttempt;
        }
        
    private:
        URL mURL;
        std::string mMethod;
        std::string mQuery;
        std::map<std::string, std::string> mHeaders;
        std::map<std::string, std::string> mPostFields;
        uint32_t mRedirectAttempt = 0;
    };
}
