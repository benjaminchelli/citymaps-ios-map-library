//
//  HTTPKeyValueList.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/18/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <string>
#include <map>

namespace citymaps
{
    class HTTPKeyValueList
    {
        public:
        
            HTTPKeyValueList(const std::string& data);
        
            std::string& operator[](const std::string& key)
            {
                return mKeyValuePairs[key];
            }

        private:

            std::map<std::string, std::string> mKeyValuePairs;
    };
}