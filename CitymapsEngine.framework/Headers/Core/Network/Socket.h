//
//  Socket.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct SocketAddress
    {
        SocketAddress() :
            ip(""), port(80) {}
        
        SocketAddress(const std::string &_ip, int _port) :
            ip(_ip), port(_port) {}
            
        std::string ip;
        int port;
    };
    
    enum SocketFlags
    {
        SocketNonBlocking = 1
    };
    
    static const double kDefaultTimeout = 15.0;
    
    class TCPSocket
    {
    public:
        TCPSocket();
        ~TCPSocket();
        
        bool Connect(SocketAddress &address, int flags = 0, double timeout = kDefaultTimeout);
        ssize_t Send(const char *data, size_t size);
        ssize_t Recv(char *buf, size_t size);
        
        void Close();
        
    private:
        int mSockFd;
    };
    
    class UDPSocket
    {
    public:
        UDPSocket();
        ~UDPSocket();
        
        bool Bind(SocketAddress &address);
        int SendTo(SocketAddress &address, char *data, size_t size);
        int RecvFrom(char *buf, size_t size, SocketAddress &fromAddr);
        
        void Close();
        
    private:
        int mSockFd;
    };
};
