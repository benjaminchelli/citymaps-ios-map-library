//
//  URL.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/24/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class URL
    {
    public:
        URL(const std::string &url);
        
        ~URL()
        {
        }
        
        const std::string& GetURL() const { return mURL; }
        const std::string& GetProtocol() const { return mProtocol; }
        const std::string& GetHost() const { return mHost; }
        uint32_t GetPort() const { return mPort; }
        const std::string& GetURI() const { return mURI; }
        
        const std::string& GetIpAddress() const { return mIPAddress;}
        
    private:
        std::string mURL;
        std::string mProtocol;
        std::string mHost;
        uint32_t mPort = 0;
        std::string mURI;
        std::string mIPAddress;
        
    };
}
