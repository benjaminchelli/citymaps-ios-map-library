//
//  HTTPConnectionImpl.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Network/HTTPRequest.h>
#include <CitymapsEngine/Core/Network/HTTPResponse.h>
#include <CitymapsEngine/Core/Network/HTTPConnection.h>
#include <Poco/Net/IPAddress.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>

namespace citymaps
{
    class DownloadOperation;
    class FileCache;

    class HTTPConnectionImpl
    {
        friend class HTTPConnection;
        friend class DownloadOperation;
        
    public:
        ~HTTPConnectionImpl();
        
    private:

        bool mAbort;
        bool mRunning;
        std::shared_ptr<DownloadOperation> mCurrentOperation;
        std::vector<ICacheFilenameTransformer *> mTransformers;
        
        typedef SafeObject<std::map<std::string, Poco::Net::IPAddress> > HostnameCacheMap;
        
        static FileCache *msFileCache;
        static HostnameCacheMap msHostnameIPCache;
        static OperationQueue msOperationQueue;
        static bool msFileCacheEnabled;
        
        HTTPConnectionImpl();
        
        NetworkResponseStatus Execute(HTTPRequest &request, HTTPResponse &outResponse, double timeout = kDefaultTimeout, uint32_t flags = LOG_BIT);
        
        bool ExecuteAsync(HTTPRequest &request, THTTPSuccessCallback success, THTTPFailureCallback failure, double timeout = kDefaultTimeout, uint32_t priority = 0);
        
        void Cancel(bool clearCallbacks = false);
        bool IsCancelled() { return mAbort; }
        
        bool ProcessResponse(HTTPResponse &response);
        bool CacheResponse(HTTPResponse &response, const std::string &url);
        void Cleanup();
        
        void AddCacheFilenameTransformer(ICacheFilenameTransformer *transformer);
        
        std::string CreateCacheFilename(const std::string &url);
        
        static void Initialize();
        static void SetCacheEnabled(bool enabled);
        static void CancelAllDownloads();
        static bool IsCacheEnabled();
        
        static bool ResolveHostname(const std::string& hostname, Poco::Net::IPAddress& outIp);
        static bool IsIPAddress(std::string hostname);
    };
};
