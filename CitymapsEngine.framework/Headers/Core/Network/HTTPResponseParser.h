//
//  HTTPResponseParser.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Network/HTTPResponse.h>
#include <CitymapsEngine/Core/Disk/FileInputStream.h>

namespace citymaps
{
    class HTTPResponseParser
    {
    public:
        HTTPResponseParser(HTTPResponse *responseObj);
        ~HTTPResponseParser();
        
        NetworkResponseStatus AppendBytes(const byte *bytes, int size);
        
        NetworkResponseStatus ParseFullResponse(const byte *bytes, int size);
        
        bool IsBodyDone() { return mBodyDone; }
        
    private:
        HTTPResponse *mResponseObj;
        
        bool mLastCharNewline;
        bool mLastCharColon;
        bool mFinishHeaders;
        bool mStatusDone;
        bool mHeadersDone;
        bool mBodyStarted;
        bool mBodyDone;
        int mContentLength;
        int mContentRead;
        
        HTTPStatus mStatus;
        HTTPHeader mTmpHeader;
        std::stringstream mReadBuf;
    };
};