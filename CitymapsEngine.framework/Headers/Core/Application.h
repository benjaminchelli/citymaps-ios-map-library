//
//  Application.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Util/SafeObject.h>
#include <CitymapsEngine/Core/EngineTypes.h>
#include <CitymapsEngine/Core/Util/AsyncJobQueue.h>

namespace citymaps
{
    class IApplication;
	class IApplicationListener
	{
		public:
			virtual ~IApplicationListener(){}
			virtual void OnResize(int width, int height) = 0;
			virtual void OnUpdate(IApplication* application, int deltaMS) = 0;
			virtual void OnRender(IApplication* application, int deltaMS) = 0;
            virtual void OnNetworkStatusChanged(IApplication* application, NetworkStatus status) = 0;
	};

    class IGraphicsDevice;
    class IGraphicsContext;
    class InputReader;
    class GPSReader;
    class ITouchListener;
    class IKeyboardListener;
    class IMouseListener;
    class IGPSListener;
    
    typedef std::function<void()> DispatchCallback;

    class IApplication : public Validatable
    {
    public:

    	virtual ~IApplication(){}

        virtual bool Initialize(int width, int height, int immediateJobThreads = 2, int longTermJobThreads = 1) = 0;
        virtual void Resize(int width, int height) = 0;
        virtual void SetNetworkStatus(NetworkStatus status) = 0;
        virtual void SetNetworkCacheEnabled(bool enabled) = 0;

        virtual void RunMainLoop(bool run) = 0;
        
        virtual void SetListener(std::shared_ptr<IApplicationListener> listener) = 0;
        virtual void AddTouchListener(std::shared_ptr<ITouchListener> listener) = 0;
        virtual void AddKeyboardListener(std::shared_ptr<IKeyboardListener> listener) = 0;
        virtual void AddMouseListener(std::shared_ptr<IMouseListener> listener) = 0;
        virtual void AddGPSListener(std::shared_ptr<IGPSListener> listener) = 0;
        
        virtual void RemoveTouchListener(std::shared_ptr<ITouchListener> listener) = 0;
        virtual void RemoveKeyboardListener(std::shared_ptr<IKeyboardListener> listener) = 0;
        virtual void RemoveMouseListener(std::shared_ptr<IMouseListener> listener) = 0;
        virtual void RemoveGPSListener(std::shared_ptr<IGPSListener> listener) = 0;
        
        virtual void Step() = 0;
        virtual void Update() = 0;
        virtual void Render() = 0;
        
        virtual void LoadShaderConfig(const std::string &resourceName) = 0;
        virtual void LoadEffectsConfig(const std::string &resourceName) = 0;
        
        virtual IGraphicsDevice* GetGraphicsDevice() = 0;
    };
    
    template <typename Platform>
    class Application : public IApplication
    {
    public:
        
        Application();
        ~Application();

        virtual bool Initialize(int width, int height, int immediateJobThreads = 2, int longTermJobThreads = 1);
        virtual void Resize(int width, int height);
        virtual void SetNetworkStatus(NetworkStatus status);
        virtual void SetNetworkCacheEnabled(bool enabled);
    
        void SetListener(std::shared_ptr<IApplicationListener> listener)
        {
        	mListener = listener;
        }
        
        void AddTouchListener(std::shared_ptr<ITouchListener> listener);
        void AddKeyboardListener(std::shared_ptr<IKeyboardListener> listener) {}
        void AddMouseListener(std::shared_ptr<IMouseListener> listener) {}
        void AddGPSListener(std::shared_ptr<IGPSListener> listener);
        
        virtual void RemoveTouchListener(std::shared_ptr<ITouchListener> listener);
        virtual void RemoveKeyboardListener(std::shared_ptr<IKeyboardListener> listener) {};
        virtual void RemoveMouseListener(std::shared_ptr<IMouseListener> listener) {};
        virtual void RemoveGPSListener(std::shared_ptr<IGPSListener> listener);
        
        void LoadShaderConfig(const std::string &resourceName);
        void LoadEffectsConfig(const std::string &resourceName);

        void OnWindowDestroyed();

        void RunMainLoop(bool run);
        
        void Step();
        void Update();
        void Render();

        void OnTouchStart(const std::vector<Point>& touches);
        void OnTouchMove(const std::vector<Point>& touches);
        void OnTouchEnd(const std::vector<Point>& touches);
        void OnTouchCancel(const std::vector<Point>& touches);

        void OnPositionChanged(double longitude, double latitude, double accuracyMeters = 0.0);
        void OnDirectionChanged(double direction);
        void OnAltitudeChanged(double altitude);
        void OnLocationStatusChanged(LocationStatus status);
        
        IGraphicsDevice* GetGraphicsDevice() { return mDevice; }
        
    protected:

        void SetGraphicsDevice(IGraphicsDevice* device);

    private:
        
        IGraphicsDevice *mDevice;
        IGraphicsContext *mDeviceContext;
        
        InputReader *mInputReader;
        GPSReader *mGPSReader;
        std::weak_ptr<IApplicationListener> mListener;

        bool mRunMainLoop;
        double mPreviousUpdateTime;
        int mCurrentStepMS;
        bool mFontsLoaded;
    };
    
    
    void DispatchOnMainThread(DispatchCallback callback);
    void DispatchImmediateJob(TJobCallback callback, TJobCallback completion = [](){});
    void DispatchLongTermJob(TJobCallback callback, TJobCallback completion = [](){});
};

#include <CitymapsEngine/Core/Application.inl>
