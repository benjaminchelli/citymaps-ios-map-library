//
//  Exception.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/14/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Exception.h>

namespace citymaps
{
    class Exception
	{
    public:
        
        template <typename... Args>
        Exception(ExceptionType type, const std::string& name, const std::string& format, const Args & ... args)
        {
            try {
                Exception(type, name, fmt::sprintf(format, args...));
            } catch (std::exception e){
            }
            
        }
        
        Exception(ExceptionType type, const std::string& name, const std::string& desc);
	};
};
