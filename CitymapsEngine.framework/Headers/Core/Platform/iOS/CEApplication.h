//
//  CEApplication.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/16/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CitymapsEngine/CitymapsEngine.h>
#import <CitymapsEngine/Core/Platform/iOS/ApplicationIOS.h>

@interface CEApplication : NSObject

- (id)initWithApplication:(std::shared_ptr<citymaps::IApplication>)app;

@property (assign, nonatomic) std::shared_ptr<citymaps::IApplication> appInstance;

@end
