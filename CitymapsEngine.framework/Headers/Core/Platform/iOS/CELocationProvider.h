//
//  CELocationProvider.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 5/2/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>
#import <CitymapsEngine/Core/Platform/iOS/CELocationParams.h>

#import <CoreLocation/CoreLocation.h>

typedef struct {
    CELonLat position;
    double direction;
    double altitude;
    double accuracy;
    double speed;
    double course;
} CEUserLocation;

@class CELocationProvider;

@protocol CELocationProviderDelegate <NSObject>

- (void)locationProvider:(CELocationProvider *)provider locationDidChange:(CEUserLocation)location;
- (void)locationProvider:(CELocationProvider *)provider authorizationStatusChanged:(CLAuthorizationStatus)status;

@end

@interface CELocationProvider : NSObject

- (void)startUpdatingLocation;
- (void)startUpdatingHeading;
- (void)stopUpdatingLocation;
- (void)stopUpdatingHeading;

@property (weak, nonatomic) id<CELocationProviderDelegate> delegate;
@property (strong, nonatomic) CELocationParams *locationParams;
@property (assign, nonatomic, readonly) CEUserLocation location;

@end