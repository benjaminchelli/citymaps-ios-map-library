//
//  FastOutputStream.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/UUID.h>

namespace citymaps
{
    class FastOutputStream
    {
    public:
        FastOutputStream(const TByteArray &data) :
            mData(data),
            mCursor(0)
        {
        }
        
        ~FastOutputStream()
        {
        }

        template <typename T>
        T Read()
        {
            T outBuf;
            
            memcpy(&outBuf, &mData[mCursor], sizeof(T));
            mCursor += sizeof(T);
            
            return outBuf;
        }
        
        void Read(char *buf, int size)
        {
            memcpy(buf, &mData[mCursor], size);
            mCursor += size;
        }

        const byte* Cursor()
        {
            return &mData[mCursor];
        }
        
        void Advance(unsigned int numBytes)
        {
            mCursor += numBytes;
        }
        
        template <typename StringType = std::string>
        StringType ReadString()
        {
            typedef typename StringType::value_type CharType;
//            CharType c;
//            StringType outStr;
//            
//            while (true) {
//                c = this->Read<CharType>();
//                
//                if (c == 0) {
//                    return outStr;
//                }
//                
//                outStr.push_back(c);
//            }
//            return outStr;
            
            CharType c;
            int size = 0;
            int start = mCursor;
            
            while(true)
            {
                c = this->Read<CharType>();
                if (c == 0)
                {
                    break;
                }
                size++;
            }
            
            return StringType((CharType *)&mData[start], size);

        }
        
        UUID ReadUUID()
        {
            uint64_t leastSigBits = this->Read<uint64_t>();
            uint64_t mostSigBits = this->Read<uint64_t>();
            
            return UUID(mostSigBits, leastSigBits);
        }
        
        bool IsEmpty()
        {
            return mCursor >= mData.size();
        }
        
    private:
        const TByteArray &mData;
        int mCursor;
    };
};
