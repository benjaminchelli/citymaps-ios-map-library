//
//  ConfigData.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/23/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

#include <cstdlib>
#include <string>
#include <unordered_map>

namespace citymaps
{
    static const std::string kEmptyString = "";
    
	class Config
	{
	public:
		static bool LoadConfig(const TByteArray& configData);
        
		static const std::string& GetString(const std::string &key)
		{
			auto it = mEntries.find(key);
			if(it != mEntries.end()) {
				return it->second;
			}
            
			return kEmptyString;
		}
        
		static int GetInt(const std::string &key)
		{
			return atoi(Config::GetString(key).c_str());
		}
        
		static short GetShort(const std::string &key)
		{
			return atoi(Config::GetString(key).c_str());
		}
        
		static bool GetBool(const std::string &key)
		{
			const std::string &val = Config::GetString(key);
			if(val == "true") {
				return true;
			}
            
			return false;
		}
        
		static float GetFloat(const std::string &key)
		{
			return atof(Config::GetString(key).c_str());
		}
        
		static double GetDouble(const std::string &key)
		{
			return atof(Config::GetString(key).c_str());
		}
        
	private:
		static std::unordered_map<std::string, std::string> mEntries;
	};
};