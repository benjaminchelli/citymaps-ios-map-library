/*
 * Timer.h
 *
 *  Created on: Jul 29, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
	class Timer
	{
    public:
        
        Timer(int interval, TimerCallback callback, bool startImmediately, void* context = NULL)
        :mInterval(interval), mCallback(callback), mContext(context), mPlatformTimer(NULL), mRunning(false)
        {
            if (startImmediately)
            {
                this->Start();
            }
        }
        
        ~Timer()
        {
            this->Stop();
            if (mPlatformTimer != NULL)
                delete mPlatformTimer;
        }
        
        void Start()
        {
            if (mPlatformTimer == NULL)
            {
                mPlatformTimer = new PlatformTimer<CurrentPlatform>(mInterval, this);
            }
            else
            {
                mPlatformTimer->Stop();
            }

            mPlatformTimer->Start();
            mRunning = true;
        }
        
        void Stop()
        {
            mRunning = false;
            if (mPlatformTimer != NULL)
                mPlatformTimer->Stop();
        }
        
        void Tick()
        {
            mCallback(this, mContext);
        }
        
        bool IsRunning()
        {
            return mRunning;
        }
    
    private:
        bool mRunning;
        int mInterval;
        TimerCallback mCallback;
        void* mContext;
        IPlatformTimer* mPlatformTimer;
	};
}
