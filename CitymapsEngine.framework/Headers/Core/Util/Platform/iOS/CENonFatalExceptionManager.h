//
//  NonFatalExceptionManager.m
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 3/2/15.
//  Copyright (c) 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CENonFatalExceptionManager;
@protocol CENonFatalExceptionHanlder <NSObject>

- (void)handleNonfatalException:(CENonFatalExceptionManager *)manager exception:(NSException *)e;

@end

@interface CENonFatalExceptionManager : NSObject

+ (CENonFatalExceptionManager*) sharedInstance;

- (void)logException:(NSException *)e;

@property (nonatomic, strong) id<CENonFatalExceptionHanlder> handler;

@end