/*
 * DeviceSpecification.h
 *
 *  Created on: Aug 7, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <string>

namespace citymaps
{
	class DeviceSpecification
	{
    public:
        
        enum ScreenSize
        {
            ScreenSizeXLarge,
            ScreenSizeLarge,
            ScreenSizeNormal,
            ScreenSizeSmall
        };
        
        static std::string GetDeviceName();
        
        static ScreenSize GetDeviceScreenSize();
        
        static double GetDevicePixelDensity();
        
        static uint32_t GetDeviceVersion();
        
        static void SetDeviceName(const std::string& name);
        
        static void SetDeviceScreenSize(ScreenSize size);
        
        static void SetDevicePixelDensity(double density);
        
        static void SetDeviceVersion(uint32_t version);
        
    private:
        
        static std::string msDeviceName;
        static ScreenSize msDeviceScreenSize;
        static double msDeviceDensity;
        static uint32_t msDeviceVersion;
	};
}
