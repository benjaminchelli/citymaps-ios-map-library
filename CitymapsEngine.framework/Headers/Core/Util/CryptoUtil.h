//
//  CryptoUtil.h
//  MapEngineLibraryIOS
//
//  Created by Adam on 4/14/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class CryptoUtil
    {
    public:
        static int DecryptData(const TByteArray &inData, std::string algorithm, std::string passphrase, TByteArray &outData);
        static void SHA1Hash(const std::string &input, std::string &hash);
    };
};
