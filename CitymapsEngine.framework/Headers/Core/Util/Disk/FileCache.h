//
//  FileCache.h
//  vectormap2
//
//  Created by Lion User on 08/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    static const int kCEOneWeekTimeDifference = 604800; // 1 week
    static const int kCEOneDayTimeDifference = 86400; // 1 day
    static const int kCEOneHourTimeDifference = 3600; // 1 hour
    static const int kCEOneSecondTimeDifference = 1; // 1 second
    static const uint32_t kFileCacheVersion = 2;
    
    struct CacheFileDesc
    {
        CacheFileDesc()
        :AgeWritten(0), MaxAge(0)
        {}
        uint32_t Version;
        uint64_t AgeWritten;
        uint64_t MaxAge;
        // Accepted values are: empty string, "identity", "deflate", "gzip", "lzma"
        std::string InflationType;
    };
    
    class FileCache
    {
    public:
        virtual ~FileCache();
        
        virtual void PurgeFileCache();
        
        virtual bool RetrieveFile(const std::string& filename, TByteArray& outData);
        virtual bool ContainsFile(const std::string& filename, bool acceptStaleFile = false);
        
        virtual bool IsStale(const std::string& filename);
        
        virtual void DeleteFile(const std::string& filename);
        virtual void SaveFile(const std::string& filename, const TByteArray& outData, CacheFileDesc desc);
        
        static FileCache* CreateFileCache(std::string basePath, CacheDeleteStrategy deleteStrategy, uint64_t cacheExpireTime)
        {
            return new FileCache(basePath, deleteStrategy, cacheExpireTime);
        }
        
        bool ReadDataForFile(const std::string& filepath, CacheFileDesc& outDesc);
        bool WriteDataForFile(const std::string& filepath, const CacheFileDesc& desc);
        
    protected:
        FileCache(std::string basePath, CacheDeleteStrategy deleteStrategy, uint64_t cacheExpireTime);
        
        std::string GetFullPathForFilename(std::string filename);
        bool IsFileDeflated(const std::string& filename);
    private:
        CacheDeleteStrategy mDeleteStrategy;
        uint64_t mCacheExpireTime;
        std::string mBasePath;
    };
};
