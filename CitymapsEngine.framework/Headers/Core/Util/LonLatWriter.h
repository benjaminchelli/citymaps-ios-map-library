//
//  RapidJsonWriter.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 7/9/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <rapidjson/writer.h>
#include <rapidjson/rapidjson.h>

namespace citymaps
{
    template<typename Stream>
    class LonLatWriter : public rapidjson::Writer<Stream>
    {
    public:
        
        LonLatWriter(Stream& buffer)
        :rapidjson::Writer<Stream>(buffer)
        {}
        
        LonLatWriter& Double(double d)
        {
            rapidjson::Writer<Stream>::Prefix(rapidjson::kNumberType);
            WriteDouble(d);
            return *this; 
        }
        
        void WriteDouble(double d)
        {
            char buffer[100];
#if _MSC_VER
            int ret = sprintf_s(buffer, sizeof(buffer), "%.7f", d);
#else
            int ret = snprintf(buffer, sizeof(buffer), "%.7f", d);
#endif
            for (int i = 0; i < ret; i++)
                rapidjson::Writer<Stream>::os_.Put(buffer[i]);
        }
        
    };
}