//
//  LineSimplifier.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 8/26/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once
#include <vector>
#include <CitymapsEngine/Core/EngineTypes.h>
namespace citymaps
{
    class LineSimplifier
    {
        public:
        
        LineSimplifier(const std::vector<Point>& points, double tolerance);
        
        void Simplify(std::vector<Point>& outPoints);
        
        private:

        void SimplifySegment(size_t begin, size_t end);
        
        const std::vector<Point>& mPoints;
        double mTolerance;
        bool* mUsePoint;

    };
}