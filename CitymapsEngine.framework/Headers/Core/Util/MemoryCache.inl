//
//  MemoryCache.cpp
//  vectormap2
//
//  Created by Lion User on 08/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

namespace citymaps
{
    template <typename T>
    MemoryCache<T>::MemoryCache(int maxSize) :
        mMaxSize(maxSize)
    {
    }
    
    template <typename T>
    MemoryCache<T>::~MemoryCache()
    {
    }
    
    template <typename T>
    void MemoryCache<T>::AddObject(T object, uint64_t key)
    {
        MemoryCacheNode newNode(object, key);
        
        mCacheNodes.lock();
        
        mCacheNodes.push_front(newNode);
        while (mCacheNodes.size() > mMaxSize) 
        {
            //T obj = mCacheNodes.back().value;
            mCacheNodes.pop_back();
        }
        
        mCacheNodes.unlock();
    }
    
    template <typename T>
    T MemoryCache<T>::RetrieveObject(uint64_t key)
    {
        mCacheNodes.lock();
        
        for (auto it = mCacheNodes.begin(); it != mCacheNodes.end(); ++it) {
            if(it->key == key) {
                //mCacheNodes.erase(it);
                //mCacheNodes.push_front(*it);
                
                mCacheNodes.unlock();
                
                return it->value;
            }
        }
        
        mCacheNodes.unlock();
        
        return NULL;
    }
    
    template <typename T>
    void MemoryCache<T>::RemoveObject(T object)
    {
        mCacheNodes.lock();
        
        for (auto it = mCacheNodes.begin(); it != mCacheNodes.end(); ++it) {
            if (it->value == object) {
                mCacheNodes.erase(it);
                mCacheNodes.unlock();
                return;
            }
        }
        
        mCacheNodes.unlock();
    }
    
    template <typename T>
    void MemoryCache<T>::RemoveObjectForKey(uint64_t key)
    {
        mCacheNodes.lock();
        
        for (auto it = mCacheNodes.begin(); it != mCacheNodes.end(); ++it) {
            if (it->key == key) {
                mCacheNodes.erase(it);
                mCacheNodes.unlock();
                return;
            }
        }
        
        mCacheNodes.unlock();
    }
    
    template <typename T>
    void MemoryCache<T>::Clear()
    {
        mCacheNodes.lock();
        mCacheNodes.clear();
        mCacheNodes.unlock();
    }
}
