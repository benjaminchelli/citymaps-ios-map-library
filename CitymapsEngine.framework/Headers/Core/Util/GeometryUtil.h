//
//  GeometryUtil.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/8/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    struct GeometryUtilLineVertex
    {
        double position[2];
        float perp[2];
        float uv[2];
    };
    
    typedef std::vector<GeometryUtilLineVertex> GeometryUtilVertexVector;
    
    class GeometryUtil
    {
    public:
        
        static void TriangulateLineSimple(const std::vector<Point>& points, GeometryUtilVertexVector& vertices);
        
        static void TriangulateLine(const std::vector<Point>& points, GeometryUtilVertexVector& vertices);
        
        static void TriangulatePolygon(const std::vector<Point>& points, std::vector<Vector2f>& vertices);
        
        static void SimplifyLine(const std::vector<Point>& points, double tolerance, std::vector<Point>& outPoints);
        
    private:
        
        static bool TriangulateLineFirstStep(const std::vector<Point>& points, GeometryUtilVertexVector& vertices, Vector2& cachedPerp);
        static bool TriangulateLineStep(int index, float v, const std::vector<Point>& points, GeometryUtilVertexVector& vertices, Vector2& cachedPerp);
        static bool TriangulateLineFinalStep(const std::vector<Point>& points, GeometryUtilVertexVector& vertices, Vector2& cachedPerp);
    };
}
