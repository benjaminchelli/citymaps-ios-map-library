//
//  AsyncJobQueue.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/18/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <mutex>
#include <condition_variable>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>
#include <CitymapsEngine/Core/Util/AsyncJobOperation.h>

namespace citymaps
{
    static const int kDefaultNumImmediate = 3;
    static const int kDefaultNumLongTerm = 2;
    
    typedef SafeObject<std::vector<std::shared_ptr<AsyncJobOperation>>> TJobOperationList;
    
    class AsyncJobQueue
    {
    public:
        AsyncJobQueue(int numImmediate = kDefaultNumImmediate, int numLongTerm = kDefaultNumLongTerm);
        ~AsyncJobQueue();
        
        void AddImmediateJob(TJobCallback callback, TJobCallback completion);
        void AddLongTermJob(TJobCallback callback, TJobCallback completion);
        void AddGraphicsJob(TJobCallback callback, TJobCallback completion);

        void ProcessGraphicsJobs();

        void WaitForImmediateJobs();
        void ProcessLongTermJobs();
        void ProcessImmediateJobs();
        
        void ClearPendingOperations();
        
        void JobComplete(std::shared_ptr<AsyncJobOperation> job);
        
    private:
        OperationQueue mImmediateJobs;
        OperationQueue mLongTermJobs;
        std::list< std::shared_ptr<AsyncJobOperation> > mGraphicsJobs;
        std::mutex mGraphicsMutex;
        std::mutex mGraphicsConditionMutex;
        std::condition_variable mGraphicsCondition;

        TJobOperationList mCompletedLongTermJobs;
        TJobOperationList mCompletedImmediateJobs;
    };
};
