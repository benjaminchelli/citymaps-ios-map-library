/*
 * SafeVector.hpp
 *
 *  Created on: Jul 10, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <mutex>

namespace citymaps
{
	template <typename T, class Mutex=std::mutex>
	class SafeObject : public T
	{
		public:

			SafeObject<T, Mutex>()
			:T(), mMutex()
			 {

			 }

			SafeObject<T,Mutex>(const SafeObject<T,Mutex>& other):
			T(other), mMutex()
			{
			}

			void lock()
			{
				mMutex.lock();
			}

			void unlock()
			{
				mMutex.unlock();
			}

			bool tryLock()
			{
				mMutex.try_lock();
			}

		private:

			Mutex mMutex;

	};
}
