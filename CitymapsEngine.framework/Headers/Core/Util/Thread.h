/*
 * Thread.h
 *
 *  Created on: Jul 16, 2013
 *      Author: eddiekimmel
 */
#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

#include <thread>

#ifdef __ANDROID__
    #include <CitymapsEngine/Core/Platform/Android/JavaVirtualMachine.h>
#endif

namespace citymaps
{
	class Thread : public std::thread
	{
		public:

			Thread(std::thread&& other);

            Thread(std::function<void()> f) :
            std::thread(Thread::ThreadCallback, f)
            {
            }

			Thread(const Thread&) = delete;
        
			int Priority();

			bool SetPriority(int priority);
        
    private:
      
        static void ThreadCallback(std::function<void()> function);
	};
}
