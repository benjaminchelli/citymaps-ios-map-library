//
//  TarArchive.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 4/15/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

#include <libarchive/libarchive/archive.h>

#include <unordered_map>

namespace citymaps
{
    class Archive
    {
    public:
        Archive(const TByteArray &data);
        ~Archive();
        
        const TByteArray& GetFile(const std::string filename)
        {
            return mFiles[filename];
        }
        
    private:
        std::unordered_map<std::string, TByteArray> mFiles;
    };
};