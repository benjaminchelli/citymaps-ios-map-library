//
//  EarClipUtility.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/12/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <set>
#include <vector>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    class EarClipUtility
    {
        
    public:
        
        static void TriangulatePolygon(const std::vector<Point>& points, std::vector<Vector2f>& vertices);
        
    private:
        
        enum WindingOrder
        {
            WindingOrderCW,
            WindingOrderCCW
        };
        
        enum VertexType
        {
            ReflexVertex,
            ConvexVertex,
            EarVertex
        };
        
        typedef std::map<int, VertexType>::iterator MapIter;
        
        EarClipUtility(const std::vector<Point>& points);
        
        WindingOrder GetWindingOrder();
        
        float CalculateTriangleArea(int index);
        bool IsConvex(int index);
        bool IsEar(int index);
        
        bool PointsOnSameSide(const Point& p1, const Point p2, const Point& a, const Point& b);
        
        bool PointInTriangle(const Point& p, const Point& a, const Point& b, const Point& c);

        void CheckVertexType(int index, VertexType previousType = ReflexVertex);
        
        MapIter Next(MapIter current);
        
        MapIter Previous(MapIter current);
        
    private:
        
        const std::vector<Point>& mPointsRef;
        std::map<int, VertexType> mRemainingVerts;
        std::set<int> mEarVerts;
        WindingOrder mWindingOrder;
    };
}
