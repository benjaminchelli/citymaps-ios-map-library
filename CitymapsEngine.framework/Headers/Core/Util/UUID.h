//
//  UUID.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class UUID
    {
    public:
        UUID(uint64_t mostSignificantBits, uint64_t leastSignificantBits);
        UUID(const std::string &data);
        ~UUID() {}
        
        const std::string ToString();
        
        bool operator==(const UUID &other)
        {
            return (other.mLeastSignificantBits == mLeastSignificantBits) &&
            (other.mMostSignificantBits == mMostSignificantBits);
        }
        
    private:
        uint64_t mMostSignificantBits;
        uint64_t mLeastSignificantBits;
    };
};
