//
//  UnicodeUtils.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/2/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class UnicodeUtil
    {
    public:
        static void ToLowerInPlace(std::u16string &str);
        static void ToUpperInPlace(std::u16string &str);
        static std::u16string ToLower(const std::u16string &str);
        static std::u16string ToUpper(const std::u16string &str);
        
        static std::u16string ToUTF16(const std::string &str);
        
        static std::string ToUTF8(const std::u16string &str);
        
        static bool StartsWith(const std::u16string &str, const std::u16string &start);
        
        static void LoadUnicodeCaseMap(const std::string &filename);
        
    private:
        static std::map<char16_t, char16_t> msUpperToLower;
        static std::map<char16_t, char16_t> msLowerToUpper;
        
        static char16_t TransformLower(char16_t chr);
        static char16_t TransformUpper(char16_t chr);
    };
};
