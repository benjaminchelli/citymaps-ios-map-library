//
//  GZIPParser.h
//  vectormap2
//
//  Created by Lion User on 06/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    enum ZLIBEncoding
    {
        GZIPEncoding,
        DeflateEncoding
    };
    
    class ZLIBParser
    {
    public:
        static bool Inflate(const TByteArray &inData, TByteArray &outData, ZLIBEncoding encoding = GZIPEncoding);
    };
};

