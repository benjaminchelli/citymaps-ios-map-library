//
//  NetworkStatus.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 3/18/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    enum NetworkStatus
    {
        NetworkStatusNone,
        NetworkStatusCellular,
        NetworkStatusWifi,
        NetworkStatusLAN,
        NetworkStatusTransient,
        NetworkStatusUnknown
    };
    
    class NetworkConnectivity
    {
    public:
        static void UpdateStatus(NetworkStatus status)
        {
            msStatus = status;
        }
        
        static NetworkStatus GetCurrentStatus()
        {
            return msStatus;
        }
        
        static bool IsOnline()
        {
            return msStatus != NetworkStatusNone;
        }
        
        static bool IsWifi() { return msStatus == NetworkStatusWifi; }
        static bool IsCellular() { return msStatus == NetworkStatusCellular; }
        
    private:
        static NetworkStatus msStatus;
    };
}


