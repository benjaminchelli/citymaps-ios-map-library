//
//  AsyncJobOperation.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/18/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/OperationQueue.h>

namespace citymaps
{
    typedef std::function<void()> TJobCallback;
    
    enum AsyncJobType
    {
        AsyncJobTypeImmediate,
        AsyncJobTypeLongTerm,
        AsyncJobTypeGraphics
    };
    
    class AsyncJobQueue;
    
    class AsyncJobOperation : public Operation, public std::enable_shared_from_this<AsyncJobOperation>
    {
    public:
        AsyncJobOperation(AsyncJobType type, TJobCallback callback, TJobCallback completion, AsyncJobQueue *queue);
        ~AsyncJobOperation();
        
        void Start();
        void Cancel() {}
        
        AsyncJobType GetType() { return mType; }
        
        void Complete()
        {
            if (mCompletion) {
                mCompletion();
            }
        }
        
    private:
        AsyncJobQueue *mQueue;
        AsyncJobType mType;
        TJobCallback mCallback;
        TJobCallback mCompletion;
    };
};
