/*
 * TimeReader.h
 *
 *  Created on: Jul 29, 2013
 *      Author: eddiekimmel
 */

#pragma once

namespace citymaps
{
	class Timer;

    typedef std::function<void(Timer *, void *)> TimerCallback;
    typedef void(*TimerCallbackTarget)(Timer*, void*);

	class IPlatformTimer
	{
		public:

		virtual ~IPlatformTimer()
		{

		}

		virtual void Start() = 0;

		virtual void Stop() = 0;

	};

	class BasePlatformTimer : public IPlatformTimer
	{
		public:

        BasePlatformTimer(int interval, Timer* timer) :mInterval(interval), mTimer(timer){}

			virtual ~BasePlatformTimer()
			{

			}

		protected:

			int GetInterval() { return mInterval;}
			Timer* GetTimer() {return mTimer;}

		private:

			int mInterval;
			Timer* mTimer;
	};

	template <typename Platform>
	class PlatformTimer : public BasePlatformTimer
	{

	};
}

