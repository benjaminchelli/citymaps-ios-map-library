//
//  CEFontReader.h
//  vectormap
//
//  Created by Adam Eskreis on 5/17/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface CEFontReader : NSObject

+ (NSData *)fontDataForCGFont:(CGFontRef)cgFont;

@end
