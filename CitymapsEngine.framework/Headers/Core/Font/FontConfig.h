//
//  FontConfig.h
//  vectormap
//
//  Created by Adam Eskreis on 5/20/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Font/FontEngine.h>

namespace citymaps
{
    class FontConfig
    {
    public:
        static TFontDescMap ReadResource(const std::string &resource);
        
    private:
        static Range GetRangeForName(const std::string &name);
    };
};

