#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Font/Types.h>

namespace citymaps
{
	typedef std::vector<GlyphSpan> TSpanList;
    
    class FontFace;

	class Glyph
	{
		friend class Font;
        friend class FontFace;

	public:
		Glyph(FT_Library &library, FontFace &fontFace, int c, int outlineWidth, bool hinting);
        ~Glyph();

		void RenderToBuffer(FontTexel *buffer, Size &size, Point &offset, int lineHeight, Glyph *prevChar = NULL);

		const Point& GetAdvance() const { return mAdvance; }
        Size GetSize() const { return Size(mSize); }
        Size GetDataSize() const { return Size(mDataSize);}
        Vector2i GetDataSizeInt() const { return mDataSize;}
        
        const Point& GetBearing() const { return mBearing; }
        const int GetKerning(Glyph *prevChar);
        const int GetCharacter() { return mChar; }
        
        unsigned char *GetBuffer() { return mBuffer;}
        unsigned int GetBufferSize() const { return mBufferSize;}

	private:
        
		Vector2i mSize;
        Vector2i mDataSize;
		Point mBearing;
		Point mAdvance;
		Vector2 mMinTexCoord;
		Vector2 mMaxTexCoord;
        unsigned int mBufferSize;
		unsigned char *mBuffer;
		unsigned int mGlyphIndex;
		TSpanList mSpans;
		int mChar;
        FontFace &mFont;

        void CreateGlyph(FT_Library &library, FontFace &fontFace, int c, bool hinting);
        void CreateOutlineGlyph(FT_Library &library, FontFace &fontFace, int c, int outlineWidth, bool hinting);
        
		void RenderSpans(FT_Library &library, FT_Outline *outline);
		void AddSpan(int y, int count, const FT_Span *spans);

		static void SpanCallback(int y, int count, const FT_Span *spans, void *user);


	};
};