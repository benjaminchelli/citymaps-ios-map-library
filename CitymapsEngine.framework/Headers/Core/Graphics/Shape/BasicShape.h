//
//  BasicShape.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/6/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/Shape.h>

namespace citymaps
{
    struct BasicVertex
    {
        float position[3];
        float color[4];
        float texCoord[2];
    };
    
    class BasicShape : public Shape
    {
    public:
        BasicShape(IGraphicsDevice *device, const std::string& effect, BasicVertex *verts, int numVerts);
        ~BasicShape();
        
        bool IsReady() { return true; }
        void Draw(IGraphicsDevice *device, RenderState &state, PrimitiveType primitiveType = PrimitiveTypeTriangleList);
        
    private:
        IVertexBuffer *mVertexBuffer;
        int mNumVerts;
        int mMVPIndex;
    };
}
