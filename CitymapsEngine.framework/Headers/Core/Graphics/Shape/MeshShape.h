//
//  MeshShape.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/9/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/Shape.h>
#include <CitymapsEngine/Core/Graphics/VertexBuffer.h>

namespace citymaps
{
    class MeshShape : public Shape
    {
    public:
        MeshShape(IGraphicsDevice *device, const std::string& effect);
        MeshShape(IGraphicsDevice *device, const std::string& effect, const void *verts, uint32_t size, uint32_t numVerts);
        MeshShape(IGraphicsDevice *device, const std::string& effect, const void *verts, uint32_t vertSize, const void *indices, uint32_t indicesSize, uint32_t numVerts);
        
        void UpdateData(const void *verts, uint32_t size, uint32_t numVerts);
        void UpdateData(const void *verts, uint32_t vertSize, const void* indices, uint32_t indicesSize, uint32_t numVerts);
        
        void SetTechnique(const std::string& technique);
        
        ~MeshShape();
        
        bool IsReady()
        {
            return mVertexBuffer->IsReady() && (!mIndexed || mIndexBuffer->IsReady());
        }
        
        void Draw(IGraphicsDevice *device, RenderState &state, PrimitiveType primitiveType = PrimitiveTypeTriangleList);
        
    private:
        IVertexBuffer *mVertexBuffer;
        IVertexBuffer *mIndexBuffer;
        uint32_t mNumVerts;
        bool mIndexed;
        IndexType mIndexType;

        int mMVPIndex;
    };
}


