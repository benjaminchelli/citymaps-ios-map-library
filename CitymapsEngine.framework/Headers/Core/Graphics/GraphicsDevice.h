//
//  GraphicsDevice.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <set>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Types.h>
#include <CitymapsEngine/Core/Graphics/GraphicsContext.h>
#include <CitymapsEngine/Core/Graphics/Shader.h>
#include <CitymapsEngine/Core/Graphics/ShaderProgram.h>
#include <CitymapsEngine/Core/Graphics/VertexBuffer.h>

#include <CitymapsEngine/Core/Disk/File.h>
#include <CitymapsEngine/Core/Disk/Resource.h>
#include <CitymapsEngine/Core/Util/AsyncJobQueue.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Core/Graphics/Texture/TextureTypes.h>

namespace citymaps
{
    enum BlendFunction
    {
        BlendFunctionZero, BlendFunctionOne, BlendFunctionSourceColor, BlendFunctionOneMinusSourceColor, BlendFunctionSourceAlpha, BlendFunctionOneMinusSourceAlpha, BlendFunctionDestColor, BlendFunctionOneMinusDestColor, BlendFunctionDestAlpha, BlendFunctionOneMinusDestAlpha
    };
    
    enum StencilTest
    {
        StencilTestNever, StencilTestAlways, StencilTestEqual, StencilTestNotEqual, StencilTestLess, StencilTestLEqual, StencilTestGEqual, StencilTestGreater
    };
    
    enum StencilOp
    {
        StencilOpKeep, StencilOpReplace, StencilOpZero
    };
    
    
    class IGraphicsDevice
    {
    public:
        virtual bool Initialize(const Size& size) = 0;
        
        virtual void SetBlendFunction(BlendFunction sourceFunc, BlendFunction destFunc) = 0;
        virtual void EnableBlending() = 0;
        virtual void DisableBlending() = 0;
        
        virtual void DisableDepthTest() = 0;
        virtual void EnableDepthTest() = 0;
        virtual void DisableDepthWrite() = 0;
        virtual void EnableDepthWrite() = 0;
        
        virtual void EnableStencilTest() = 0;
        virtual void DisableStencilTest() = 0;
        
        virtual void ClearColorBuffer() = 0;
        virtual void ClearDepthBuffer() = 0;
        virtual void ClearStencilBuffer() = 0;
        
        virtual void SetStencilFunction(StencilTest test, int value, int mask) = 0;
        virtual void SetStencilOperation(StencilOp failStencil, StencilOp failDepth, StencilOp pass) = 0;
        virtual void SetStencilMask(int mask) = 0;
        
        virtual void SetBackgroundColor(const Vector4f& color) = 0;
        virtual Vector4f GetBackgroundColor() const = 0;
        virtual IGraphicsContext* GetContext() = 0;
        
        virtual void Resize(const Size& size) = 0;

        virtual void PreRender() = 0;
        virtual void PostRender() = 0;
        
        virtual void Draw(PrimitiveType type, int count, int start) = 0;
        virtual void DrawIndexed(PrimitiveType type, IndexType indexType, int count, int start) = 0;
        
        virtual IShader* CreateShader(const std::string &filename, ShaderType type) = 0;
        virtual IShader* CreateShader(const TByteArray &data, ShaderType type) = 0;
        virtual IShaderProgram* CreateShaderProgram() = 0;
        
        virtual IVertexBuffer* CreateVertexBuffer(VertexBufferType type, VertexBufferUsage usage) = 0;
        
        virtual ITexture2D* CreateTexture2D(const TEXTURE_DESC &desc) = 0;
        virtual ITexture2D* CreateTexture2D(const Image *image, const TEXTURE_DESC& desc) = 0;
        virtual ITexture2D* CreateTexture2D(const ImageResource& resource, const TEXTURE_DESC& desc) = 0;
        virtual ITexture2D* CreateTexture2D(const byte *data, const Vector2i &size, const TEXTURE_DESC& desc) = 0;
        
        virtual void AddGraphicsJob(TJobCallback callback, TJobCallback completion = [](){}) = 0;
        virtual void ProcessGraphicsJobs() = 0;
        
        virtual void OnWindowDestroyed() = 0;
        virtual void ReleaseGraphicsObject(GraphicsObject* object) = 0;
        
        virtual ~IGraphicsDevice() {};
    };
    
    class BaseGraphicsDevice : public IGraphicsDevice
    {
    public:
        
        BaseGraphicsDevice(GRAPHICS_DEVICE_DESC *desc);
        virtual ~BaseGraphicsDevice();
        
        IGraphicsContext* GetContext() { return mContext; }

        void AddGraphicsJob(TJobCallback callback, TJobCallback completion = [](){})
        {
            mJobQueue.AddGraphicsJob(callback, completion);
        }
        
        void ProcessGraphicsJobs()
        {
            mJobQueue.ProcessGraphicsJobs();
        }
        
        void ReleaseGraphicsObject(GraphicsObject* object)
        {
            if (object)
            {
                mGraphicsObjectMutex.lock();
        	    mGraphicsObjects.erase(object);
        	    mGraphicsObjectMutex.unlock();
        	    this->AddGraphicsJob([object](){delete object;});
        	}
        }

        virtual void PreRender()
        {
           
        }
        
        virtual void PostRender()
        {
            mJobQueue.ProcessGraphicsJobs();
            mContext->ResetState();
        }
        
        void SetGraphicsBackup(bool backupEnabled)
        {
            mBackupEnabled = backupEnabled;
        }
        
        bool ShouldBackupObjects() const { return mBackupEnabled; }
        
        void OnWindowDestroyed();
        
        void SetBackgroundColor(const Vector4f& color)
        {
            mBackgroundColor = color;
        }
        
        Vector4f GetBackgroundColor() const
        {
            return mBackgroundColor;
        }
        
    protected:

        void RegisterNewGraphicsObject(GraphicsObject* object)
        {
            object->SetShouldBackup(mBackupEnabled);
            mGraphicsObjectMutex.lock();
        	mGraphicsObjects.insert(object);
        	mGraphicsObjectMutex.unlock();
        }
        GRAPHICS_DEVICE_DESC& GetDesc() { return mDesc; }
        
    private:
        
        GRAPHICS_DEVICE_DESC mDesc;
        AsyncJobQueue mJobQueue;
        IGraphicsContext *mContext;
        std::mutex mGraphicsObjectMutex;
        std::set<GraphicsObject*> mGraphicsObjects;

        Vector4f mBackgroundColor;
        bool mBackupEnabled;
    };
    
    template <typename Renderer>
    class GraphicsDevice : public BaseGraphicsDevice
    {
    };
};
