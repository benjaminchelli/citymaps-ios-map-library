//
//  Shape.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/6/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/Effect.h>

namespace citymaps
{
    class IShape
    {
    public:
        
        virtual void Draw(IGraphicsDevice *device, RenderState &state, PrimitiveType primitiveType = PrimitiveTypeTriangleList) = 0;
        
        virtual bool IsReady() = 0;
        
        virtual ~IShape() {}
        
    protected:
        
        virtual void OnPreparePass(int passIndex) = 0;
    };
    
    class Shape : public IShape
    {
    public:
        
        Shape(const std::string& effect)
        :mEffectName(effect), mEffect(NULL)
        {
            
        }
        Shape(IGraphicsDevice *device, const std::string& effect)
        :mEffectName(effect), mEffect(device->GetContext()->GetEffect(effect))
        {
        }
        
        virtual ~Shape()
        {
        }
        
        virtual void OnPreparePass(int passIndex) {};
        
    protected:
        
        Effect* GetEffect(IGraphicsDevice* device = NULL)
        {
            if (device == NULL)
                return mEffect;

            mEffect = device->GetContext()->GetEffect(mEffectName);
            if (!mEffect)
            {
                throw Exception(BadParameterException, "Effect does not exist: ", mEffectName);
            }
            return mEffect;
        }

    private:
        
        std::string mEffectName;
        Effect* mEffect;
    };
};
