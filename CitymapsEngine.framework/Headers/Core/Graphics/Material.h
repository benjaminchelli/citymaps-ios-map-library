//
//  Material.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/6/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <unordered_map>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/ShaderData.h>


namespace citymaps
{
	class Material
	{

			typedef std::unordered_map<std::string, ShaderData*> ShaderDataMap;

		public:
			Material()
        {}
			~Material()
            {
            }

			void Set(const std::string& key, ShaderData* data)
			{
				mShaderDataMap[key] = data;
			}

			ShaderData* Get(const std::string& key)
			{
				ShaderDataMap::const_iterator iter = mShaderDataMap.find(key);
				if (iter != mShaderDataMap.end())
				{
					return iter->second;
				}

				return NULL;
			}

		private:

			ShaderDataMap mShaderDataMap;
	};
};
