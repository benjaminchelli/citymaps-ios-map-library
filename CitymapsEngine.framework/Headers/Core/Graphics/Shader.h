//
//  Shader.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Disk/Resource.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Types.h>
#include <CitymapsEngine/Core/Util/Util.h>
#include <CitymapsEngine/Core/Graphics/GraphicsObject.h>
#include <CitymapsEngine/Core/Disk/File.h>


namespace citymaps
{
    enum ShaderType
	{
		ShaderTypeVertexShader,
		ShaderTypePixelShader, // "Fragment Shader" in OpenGL
		ShaderTypeGeometryShader,
		ShaderTypeComputeShader, 
		ShaderTypeDomainShader, // "Tess Control" Shader in OpenGL
		ShaderTypeHullShader, // "Tess Eval" Shader in OpenGL
        ShaderTypeUnknown
	};
    
	static const int kShaderTypeMin = ShaderTypeVertexShader;
	static const int kShaderTypeMax = ShaderTypeHullShader;
    
	static const int kShaderDefaultCacheSize = 10;
    
	class IShaderProgram;
    
	class IShader : public GraphicsObject
	{
	public:
        IShader(IGraphicsDevice* device):GraphicsObject(device){}
        virtual void AddGlobalData(int engineIndex, const std::string& name) = 0;
        
        virtual std::vector<std::pair<int, std::string> > GetGlobalData() const = 0;
        
		virtual void Load() = 0;
		virtual void AttachToProgram(IShaderProgram *program) = 0;
		virtual void DetachFromProgram(IShaderProgram *program) = 0;
		virtual const unsigned char * GetShaderSource() = 0;
		virtual size_t GetSourceSize() const = 0;
        virtual ShaderType GetType() const = 0;
        
        virtual ~IShader() {};
	};
    
	class BaseShader : public IShader
	{
	public:
		BaseShader(IGraphicsDevice* device, std::string filename, ShaderType type);
        BaseShader(IGraphicsDevice* device, const TByteArray &data, ShaderType type);
        
		virtual ~BaseShader()
		{
			if(mShaderSource) {
				delete mShaderSource;
			}
		}
        
        void AddGlobalData(int engineIndex, const std::string& name)
        {
            mGlobalData.push_back(std::pair<int, std::string>(engineIndex, name));
        }
        
        std::vector<std::pair<int, std::string> > GetGlobalData() const
        {
            return mGlobalData;
        }
        
		const unsigned char * GetShaderSource() { return mShaderSource; }
        size_t GetSourceSize() const { return mSourceSize; }
        
		bool IsLoaded() const { return mLoaded; }
		void SetLoaded(bool loaded) { mLoaded = loaded; }
        
		ShaderType GetType() const { return mType; }
        
	private:
		ShaderType mType;
		unsigned char *mShaderSource;
		size_t mSourceSize;
		bool mLoaded;
        std::vector<std::pair<int, std::string> > mGlobalData;
	};
    
	template <typename Renderer>
	class Shader : public BaseShader {};
    
};
