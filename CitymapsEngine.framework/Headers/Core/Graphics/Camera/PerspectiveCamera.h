//
//  PerspectiveCamera.h
//  vectormap2
//
//  Created by Lion User on 10/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Camera.h>
#include <CitymapsEngine/Core/Graphics/Camera/Frustum.h>

namespace citymaps
{
    class PerspectiveCamera : public Camera
    {
    public:
        PerspectiveCamera(real aspect, real fovy, real nearZ, real farZ, Size screenSize);
        virtual ~PerspectiveCamera() {};
        
        virtual void Update();
        
        void UpdateProjection(real aspect, real fovy, real nearZ, real farZ);

        const Frustum& GetFrustum() const { return mFrustum; }
        
    protected:
        const real GetAspect() { return mAspect; }
        const real GetFovY() { return mFovY; }
        const real GetNearZ() { return mNearZ; }
        const real GetFarZ() { return mFarZ; }
        
    private:
        real mAspect;
        real mFovY;
        real mNearZ;
        real mFarZ;
        Frustum mFrustum;
    };
};
