//
//  Frustum.h
//  vectormap2
//
//  Created by Lion User on 10/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    static int const LEFT = 0, RIGHT = 1, BOTTOM = 2, TOP = 3, NEAR = 4, FAR = 5,
        NLT = 0, NLB = 1, NRT = 2, NRB = 3, FLT = 4, FLB = 5, FRT = 6, FRB = 7;
    
    class Frustum
    {
    public:
        Frustum() {}
        ~Frustum() {}
        
        void UpdatePlanes(const Matrix4 &matrix);
        
        bool ContainsPoint(const Vector3 &point);
        ContainmentType ContainsBounds(const Bounds &bounds);
        Bounds GetPlaneIntersection(Plane &plane);
        
    private:
        Plane mPlanes[6];
        Line mEdges[12];
        Vector3 mPoints[8];
    };
};
