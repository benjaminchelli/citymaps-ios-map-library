//
//  ShaderProgram.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Shader.h>
#include <CitymapsEngine/Core/Graphics/InputLayout.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture.h>

#include <CitymapsEngine/Core/Graphics/GraphicsObject.h>

namespace citymaps
{
	class ShaderData;
    class IVertexBuffer;
    
    struct GlobalData
    {
        GlobalData(const std::string& name, int engineIndex, unsigned int index, ShaderType type)
        :Name(name), EngineIndex(engineIndex), Index(index), Type(type)
        {}
        
        std::string Name;
        int EngineIndex;
        unsigned int Index;
        ShaderType Type;
    };

    class IShaderProgram : public GraphicsObject
	{
	public:
    	IShaderProgram(IGraphicsDevice* device):GraphicsObject(device){}
		virtual void SetShader(IShader *shader) = 0;
		virtual void Build() = 0;
		virtual void Activate() = 0;
        virtual void BindToVertexBuffer() = 0;
        virtual void Deactivate() = 0;
		virtual void SetInputLayout(InputLayout *inputLayout) = 0;
        
        virtual std::vector<GlobalData> GetGlobalDataList() = 0;
        
        virtual void SetShaderData(const GlobalData& globalData, const ShaderData* shaderData) = 0;

        virtual int GetGlobalVariableIndex(const char *name, ShaderType shader) = 0;
        
        virtual void SetGlobalData(int index, const float *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const int *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Vector2f *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Vector3f *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Vector4f *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Matrix2f *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Matrix3f *val, int count, ShaderType shader) = 0;
        virtual void SetGlobalData(int index, const Matrix4f *val, int count, ShaderType shader) = 0;
        
        virtual void SetConstantDataStruct(int index, const void *data, int size, ShaderType shader) = 0;
        
        virtual void SetTexture(int index, ITexture* texture, int texUnit, ShaderType shader) = 0;

        virtual ~IShaderProgram() {};
	};
    
	typedef std::map<ShaderType, IShader *> TShaderMap;
    
	class BaseShaderProgram : public IShaderProgram
	{
	public:
		BaseShaderProgram(IGraphicsDevice* device) :
            IShaderProgram(device), mInputLayout(NULL)
        {
        }
        
		~BaseShaderProgram()
        {
            if(mInputLayout) {
                delete mInputLayout;
            }
        }
        
		void Build();

		void SetShader(IShader *shader)
        {
            mShaders[shader->GetType()] = shader;
        }
        
		void SetInputLayout(InputLayout *inputLayout) { mInputLayout = inputLayout; }

		InputLayout* GetInputLayout() { return mInputLayout; }
        
		IShader* GetShader(ShaderType type)
        {
            if(mShaders.count(type)) {
                return mShaders[type];
            }
            
            return NULL;
        }
        
        virtual void Invalidate();

		void SetShaderData(const GlobalData& globalData, const ShaderData* shaderData);
        
	protected:
		TShaderMap& GetShaders() { return mShaders; }
        
	private:
		TShaderMap mShaders;
		InputLayout *mInputLayout;
		std::map<int, uint64_t> mGlobalDataIDs;
   
	};
    
	template <typename Renderer>
	class ShaderProgram : public BaseShaderProgram {};
};
