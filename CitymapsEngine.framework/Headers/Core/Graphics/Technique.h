//
//  Technique.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/Pass.h>

namespace citymaps
{
    typedef std::vector<Pass *> TPassList;
    
	class Technique
	{
	public:
        
        Technique(const std::string& name) :
        mName(name),
        mCurrentPass(0)
        {
        }
        
        ~Technique()
        {
            for(int i = 0; i < mPasses.size(); i++) {
                delete mPasses[i];
            }
        }
        
        std::string GetName() const
        {
            return mName;
        }
        
        void AddPass(Pass *pass) { mPasses.push_back(pass); }
        Pass* GetCurrentPass() { return mPasses[mCurrentPass]; }
        
        bool HasNextPass()
        {
            return mPasses.size() > mCurrentPass;
        }
        
        void ProcessNextPass(IGraphicsDevice *device, RenderState &state)
        {
            if(mPasses.size() > mCurrentPass)
            {
                Pass *pass = mPasses[mCurrentPass];
                pass->Apply(device, state);
                mCurrentPass++;
            }
        }
        
        void Begin()
        {
            mCurrentPass = 0;
        }
        
	private:
        
        std::string mName;
        int mCurrentPass;
		TPassList mPasses;
	};
};
