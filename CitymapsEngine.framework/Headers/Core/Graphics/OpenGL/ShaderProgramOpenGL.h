//
//  ShaderProgramOpenGL.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/ShaderProgram.h>

namespace citymaps
{
    class IVertexBuffer;
    
    template<>
    class ShaderProgram<RendererTypeOpenGL> : public BaseShaderProgram
    {
    public:
        ShaderProgram(IGraphicsDevice* device);
        ~ShaderProgram();
        
        void Build();
		void Activate();
        void BindToVertexBuffer();
        void Deactivate();
        
        GLuint GetProgramId() { return mProgramId; }
        
        int GetGlobalVariableIndex(const char *name, ShaderType shader)
        {
            return glGetUniformLocation(mProgramId, name);
        }
        
        std::vector<GlobalData> GetGlobalDataList()
        {
            return mGlobalDataList;
        }

        /* Uniform by index functions */
        void SetGlobalData(int index, const float *val, int count, ShaderType shader)
        {
            glUniform1fv(index, count, &val[0]);
        }
        
        void SetGlobalData(int index, const int *val, int count, ShaderType shader)
        {
            glUniform1iv(index, count, &val[0]);
        }
        
        void SetGlobalData(int index, const Vector2f *val, int count, ShaderType shader)
        {
            glUniform2fv(index, count, &val[0][0]);
        }
        
        void SetGlobalData(int index, const Vector3f *val, int count, ShaderType shader)
        {
            glUniform3fv(index, count, &val[0][0]);
        }
        
        void SetGlobalData(int index, const Vector4f *val, int count, ShaderType shader)
        {
            glUniform4fv(index, count, &val[0][0]);
        }
        
        void SetGlobalData(int index, const Matrix2f *val, int count, ShaderType shader)
        {
            glUniformMatrix2fv(index, count, GL_FALSE, &val[0][0][0]);
        }
        
        void SetGlobalData(int index, const Matrix3f *val, int count, ShaderType shader)
        {
            glUniformMatrix3fv(index, count, GL_FALSE, &val[0][0][0]);
        }
        
        void SetGlobalData(int index, const Matrix4f *val, int count, ShaderType shader)
        {
            glUniformMatrix4fv(index, count, GL_FALSE, &val[0][0][0]);
        }
        
        void SetConstantDataStruct(int index, const void *data, int size, ShaderType shader)
        {
            throw Exception(UnsupportedTypeException, "OpenGL Exception", "OpenGL constant data structs not supported");
        }
        
        void SetTexture(int index, ITexture* texture, int texUnit, ShaderType shader)
        {
        	SetGlobalData(index, &texUnit, 1, shader);
        }
        
        void Invalidate()
        {
            Validatable::Invalidate();
            mProgramId = 0;
        }
            

    private:
        GLuint mProgramId;
        std::vector<GlobalData> mGlobalDataList;
    };
    
    typedef ShaderProgram<RendererTypeOpenGL> ShaderProgramOpenGL;
};
