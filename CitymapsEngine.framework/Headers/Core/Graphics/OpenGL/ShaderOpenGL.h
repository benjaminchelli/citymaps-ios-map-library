//
//  ShaderOpenGL.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Shader.h>

namespace citymaps
{
    template<>
    class Shader<RendererTypeOpenGL> : public BaseShader
    {
    public:
        Shader(IGraphicsDevice* device, std::string filename, ShaderType type);
        Shader(IGraphicsDevice* device, const TByteArray &data, ShaderType type);
        ~Shader();
        
        void Load();
		void AttachToProgram(IShaderProgram *program);
		void DetachFromProgram(IShaderProgram *program);
        
    private:
        GLuint mShaderId;
    };
    
    typedef Shader<RendererTypeOpenGL> ShaderOpenGL;
};
