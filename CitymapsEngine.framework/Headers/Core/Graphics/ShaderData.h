/*
 * ShaderData.h
 *
 *  Created on: Jul 18, 2013
 *      Author: eddiekimmel
 */

#pragma

#pragma once

#include <CitymapsEngine/Core/Globals.h>
#include <CitymapsEngine/Core/Graphics/ShaderProgram.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture.h>

namespace citymaps
{
	class ShaderData
	{
		public:
			ShaderData() :
					mID(GetNextID())
			{
			}

			~ShaderData()
			{
			}

			virtual void Apply(IShaderProgram *program, int index,
					ShaderType type) const = 0;

			uint64_t GetID() const
			{
				return mID;
			}

		protected:

			void DataChanged()
			{
				mID = GetNextID();
			}

		private:

			static uint64_t GetNextID()
			{
				uint64_t id = msNextID;
				msNextID++;
				return id;
			}
		private:

			static uint64_t msNextID;

		private:

			uint64_t mID;
	};

    template<typename T>
	class ShaderDataPrimitive: public ShaderData
	{
    public:
        
        ShaderDataPrimitive() :
        ShaderData(), mData(0)
        {
        }
        
        ShaderDataPrimitive(const T data) :
        ShaderData(), mData(data)
        {
        }
        
        void SetData(const T data)
        {
            mData = data;
            DataChanged();
        }
        
        ~ShaderDataPrimitive()
        {
        }
        
        void Apply(IShaderProgram *program, int index, ShaderType type) const
        {
            program->SetGlobalData(index, &mData, 1, type);
        }
        
    private:
        T mData;
	};
    
	template<typename T>
	class ShaderDataPrimitiveArray: public ShaderData
	{
		public:

			ShaderDataPrimitiveArray() :
					ShaderData(), mData(NULL), mCount(0)
			{
			}

			ShaderDataPrimitiveArray(const T *data, int count) :
					ShaderData(), mData(data), mCount(count)
			{
			}

			void SetData(const T* data, int count)
			{
				mData = data;
				mCount = count;
				DataChanged();
			}

			~ShaderDataPrimitiveArray()
			{
			}
 
			void Apply(IShaderProgram *program, int index, ShaderType type) const
			{
				program->SetGlobalData(index, mData, mCount, type);
			}

		private:
			const T *mData;
			int mCount;
	};

    typedef ShaderDataPrimitive<float> ShaderDataFloat;
	typedef ShaderDataPrimitive<int> ShaderDataInt;
	typedef ShaderDataPrimitive<glm::vec2> ShaderDataVector2f;
	typedef ShaderDataPrimitive<glm::vec3> ShaderDataVector3f;
	typedef ShaderDataPrimitive<glm::vec4> ShaderDataVector4f;
	typedef ShaderDataPrimitive<glm::mat2> ShaderDataMatrix2f;
	typedef ShaderDataPrimitive<glm::mat3> ShaderDataMatrix3f;
	typedef ShaderDataPrimitive<glm::mat4> ShaderDataMatrix4f;
    
	typedef ShaderDataPrimitiveArray<float> ShaderDataFloatArray;
	typedef ShaderDataPrimitiveArray<int> ShaderDataIntArray;
	typedef ShaderDataPrimitiveArray<glm::vec2> ShaderDataVector2fArray;
	typedef ShaderDataPrimitiveArray<glm::vec3> ShaderDataVector3fArray;
	typedef ShaderDataPrimitiveArray<glm::vec4> ShaderDataVector4fArray;
	typedef ShaderDataPrimitiveArray<glm::mat2> ShaderDataMatrix2fArray;
	typedef ShaderDataPrimitiveArray<glm::mat3> ShaderDataMatrix3fArray;
	typedef ShaderDataPrimitiveArray<glm::mat4> ShaderDataMatrix4fArray;

	class ShaderDataStruct: public ShaderData
	{
		public:

			ShaderDataStruct() :
					ShaderData(), mData(NULL), mSize(0), mCount(0)
			{

			}

			ShaderDataStruct(const void *data, size_t size, int count) :
					ShaderData(), mData(data), mSize(size), mCount(count)
			{
			}

			void SetData(const void* data, size_t size, int count)
			{
				mData = data;
				mSize = size;
				mCount = count;
				DataChanged();
			}

			~ShaderDataStruct()
			{
			}

			void Apply(IShaderProgram *program, int index, ShaderType type) const
			{
				program->SetConstantDataStruct(index, mData, (int)mSize, type);
			}

		private:
			const void *mData;
			size_t mSize;
			int mCount;
	};

	class ShaderDataTexture: public ShaderData
	{
		public:

			ShaderDataTexture() :
					ShaderData(), mTexture(NULL), mTexUnit(0)
			{
			}

			ShaderDataTexture(ITexture *texture, int texUnit) :
					ShaderData(), mTexture(texture), mTexUnit(texUnit)
			{
			}

			void SetData(ITexture* texture, int texUnit)
			{
				mTexture = texture;
				mTexUnit = texUnit;
				DataChanged();
			}

			~ShaderDataTexture()
			{
			}

			void Apply(IShaderProgram *program, int index, ShaderType type) const
			{
				program->SetTexture(index, mTexture, mTexUnit, type);
			}

		private:
			ITexture *mTexture;
			int mTexUnit;
	};
}
;
