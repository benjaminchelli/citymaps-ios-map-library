//
//  GraphicsContext.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/VertexBuffer.h>

namespace citymaps
{
    class IShader;
    class IShaderProgram;
    class Effect;
    class ITexture;
    class ITexture1D;
    class ITexture2D;
    class IVertexBuffer;
    class GraphicsObject;
    class Sprite;
    
    typedef std::map<std::string, IShader *> TShaderCacheMap;
	typedef std::map<std::string, IShaderProgram *> TShaderProgramCacheMap;
	typedef std::map<std::string, Effect *> TEffectCacheMap;
	typedef std::map<std::string, ITexture*> TTextureCacheMap;
	typedef std::map<std::string, IVertexBuffer*> TVertexBufferCacheMap;
	typedef std::map<std::string, int32_t> TGlobalDataIndexMap;
    typedef std::map<std::string, GraphicsObject*> TGraphicsObjectCacheMap;
    typedef std::map<std::string, Sprite*> TSpriteCacheMap;
    
	class IGraphicsContext
	{
	public:
		virtual void AddShader(const std::string& key, IShader *shader) = 0;
		virtual IShader* GetShader(const std::string& key) const = 0;
		virtual bool ContainsShader(const std::string& key) const = 0;
        
		virtual void AddShaderProgram(const std::string& key, IShaderProgram *program) = 0;
		virtual IShaderProgram* GetShaderProgram(const std::string& key) const = 0;
		virtual bool ContainsShaderProgram(const std::string& key) const = 0;
        
		virtual void AddEffect(const std::string& key, Effect *technique) = 0;
		virtual Effect* GetEffect(const std::string& key) const = 0;
		virtual bool ContainsEffect(const std::string& key) const = 0;

		virtual void AddTexture(const std::string& key, ITexture* texture) = 0;
		virtual ITexture* GetTexture(const std::string& key) const = 0;
		virtual bool ContainsTexture(const std::string& key) const = 0;
        
		virtual void AddSharedVertexBuffer(const std::string& key, IVertexBuffer*) = 0;
		virtual IVertexBuffer* GetSharedVertexBuffer(const std::string& key) const = 0;
		virtual bool ContainsSharedVertexBuffer(const std::string& key) const = 0;

        virtual void AddGraphicsObject(const std::string& key, GraphicsObject*) = 0;
        virtual GraphicsObject* GetGraphicsObject(const std::string& key) const = 0;
        virtual bool ContainsGraphicsObject(const std::string& key) const = 0;
        
        virtual void AddSprite(const std::string& key, Sprite*) = 0;
        virtual Sprite* GetSprite(const std::string& key) const = 0;
        virtual bool ContainsSprite(const std::string& key) const = 0;
        
		virtual int RegisterGlobalData(const std::string& globalData) = 0;
		virtual int GetGlobalDataIndex(const std::string& globalData) = 0;

        virtual void SetActiveVertexBuffer(IVertexBuffer* buffer) = 0;
		virtual void SetActiveShaderProgram(IShaderProgram *shader) = 0;
		virtual void SetActiveTexture(ITexture* texture, int slot) = 0;
        
        virtual void ResetState() = 0;
        
        virtual ~IGraphicsContext() {};
	};
    
    class GraphicsContext : public IGraphicsContext
    {
    public:
		GraphicsContext();
		~GraphicsContext();
        
		void AddShader(const std::string& key, IShader *shader);
		IShader* GetShader(const std::string& key) const;
		bool ContainsShader(const std::string& key) const;
        
		void AddShaderProgram(const std::string& key, IShaderProgram *program);
		IShaderProgram* GetShaderProgram(const std::string& key) const;
		bool ContainsShaderProgram(const std::string& key) const;
        
        void AddEffect(const std::string& key, Effect *technique);
        Effect* GetEffect(const std::string& key) const;
        bool ContainsEffect(const std::string& key) const;

		void AddTexture(const std::string& key, ITexture* texture);
		ITexture* GetTexture(const std::string& key) const;
		bool ContainsTexture(const std::string& key) const;

		void AddSharedVertexBuffer(const std::string& key, IVertexBuffer*);
		IVertexBuffer* GetSharedVertexBuffer(const std::string& key) const;
		bool ContainsSharedVertexBuffer(const std::string& key) const;

        void AddGraphicsObject(const std::string& key, GraphicsObject*);
        GraphicsObject* GetGraphicsObject(const std::string& key) const;
        bool ContainsGraphicsObject(const std::string& key) const;
        
        void AddSprite(const std::string& key, Sprite*);
        Sprite* GetSprite(const std::string& key) const;
        bool ContainsSprite(const std::string& key) const;
        
		int RegisterGlobalData(const std::string& globalData);
		int GetGlobalDataIndex(const std::string& globalData);

        void SetActiveVertexBuffer(IVertexBuffer* buffer);
		void SetActiveShaderProgram(IShaderProgram *shader);
		void SetActiveTexture(ITexture *texture, int slot);
        
        void ResetState();

	private:

		TShaderCacheMap mShaderCache;
		TShaderProgramCacheMap mShaderProgramCache;
		TEffectCacheMap mEffectCache;
		TTextureCacheMap mTextureCache;
		TVertexBufferCacheMap mVertexBufferCache;
        TGraphicsObjectCacheMap mGraphicsObjectCache;
		TGlobalDataIndexMap mGlobalDataIndexMap;
        TSpriteCacheMap mSpriteCache;
        
		IShaderProgram *mActiveShader;
		std::map<int, ITexture*> mActiveTextures;
        std::map<VertexBufferType, IVertexBuffer*> mActiveVertexBuffers;
    };
};
