//
//  Effect.h
//  vectormap2
//
//  Created by Edward Kimmel on 7/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/Technique.h>

namespace citymaps
{
    typedef std::map<std::string, Technique *> TTechniqueMap;
    
	class Effect
	{
	public:
		Effect(const std::string& name): mName(name),
        mCurrentTechnique(NULL), mTechniques()
        {
        }
        
        ~Effect()
        {
            for(TTechniqueMap::value_type& technique : mTechniques)
            {
                delete technique.second;
            }
        }
        
        void AddTechnique(Technique *technique)
        {
           if (mTechniques.empty())
               mCurrentTechnique = technique;
            
           mTechniques[technique->GetName()] = technique;
        }
        
        Technique* GetCurrentTechnique() { return mCurrentTechnique; }
        
        bool SetCurrentTechnique(const std::string& name)
        {
            TTechniqueMap::const_iterator iter = mTechniques.find(name);
            if (iter != mTechniques.end())
            {
                mCurrentTechnique = iter->second;
                return true;
            }

            return false;
        }
        
        const std::string& GetName() { return mName; }
        
	private:
        
        std::string mName;
        Technique* mCurrentTechnique;
		TTechniqueMap mTechniques;
	};
};
