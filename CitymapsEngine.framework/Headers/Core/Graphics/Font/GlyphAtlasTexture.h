//
//  GlyphAtlasTexture.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 1/3/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Font/GlyphAtlas.h>
#include <CitymapsEngine/Core/Font/Glyph.h>

#include <CitymapsEngine/Core/Graphics/GraphicsObject.h>
namespace citymaps
{
    class ITexture2D;
    class IGraphicsDevice;
    class GlyphAtlasTexture : public GraphicsObject
    {
    public:
        
        GlyphAtlasTexture(IGraphicsDevice* device, const Size& atlasSize);
        
        GlyphData GetCoordinates(Font* font, int character, unsigned short outlineSize);
        bool ContainsGlyph(Font* font, int character, unsigned short outlineSize);
        
        ITexture2D* GetTexture() const;
        GlyphAtlas* GetGlyphAtlas() { return &mGlyphAtlas;}
        
        bool IsValid() const;
        
        // Will dispatch a graphics job to update the texture if required.
        void UpdateTextureAsync();
        
        void UpdateTexture();
      
    protected:
        ~GlyphAtlasTexture();
        
    private:
        
        GlyphAtlas mGlyphAtlas;
        ITexture2D *mGlyphTexture;
        std::atomic<int> mPendingTextureUploads;
    };
}