//
//  Pass.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/5/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <sstream>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/ShaderProgram.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/GraphicsContext.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/ShaderData.h>

namespace citymaps
{    
    class Pass
    {
    public:
        
        Pass(IShaderProgram* program)
        :mShaderProgram(program), mShaderDataList(program->GetGlobalDataList())
        {
        }
        
        ~Pass()
        {
        }
        
        void Apply(IGraphicsDevice *device, RenderState &state);
        
    private:
        
        IShaderProgram* mShaderProgram;
        std::vector<GlobalData> mShaderDataList;
    };
};
