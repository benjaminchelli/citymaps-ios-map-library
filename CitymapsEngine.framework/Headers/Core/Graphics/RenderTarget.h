//
//  RenderTarget.h
//  vectormap2
//
//  Created by Eddie Kimmel on 7/31/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Texture/TextureTypes.h>

namespace citymaps
{    
    enum DepthAttachment
    {
        DepthAttachment16,
        DepthAttachmentNone
    };
    
    class ITexture2D;
    
    class IRenderTarget
    {
        public:
        
        virtual ~IRenderTarget()
        {
        }
        
        virtual void Activate() = 0;
        
        virtual void Disable() = 0;
        
        virtual ITexture2D* GetColorAttachment() = 0;
        
        virtual Vector2i GetSize() const = 0;
    };
    
    class BaseRenderTarget : public IRenderTarget
    {
    public:
        
        BaseRenderTarget(int width, int height, ITexture2D* colorAttachment, DepthAttachment depthAttachment)
        :mSize(width, height)
        {}
        
        Vector2i GetSize() const
        {
            return mSize;
        }
        
    private:
        
        Vector2i mSize;
        
    };
    
    template <typename Renderer>
    class RenderTarget : public BaseRenderTarget
    {
        
    };
}
