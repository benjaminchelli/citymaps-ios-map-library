/*
 * Texture2DOpenGL.h
 *
 *  Created on: Jul 17, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/Core/EngineTypes.h>

#include <CitymapsEngine/Core/Graphics/Texture/Texture2D.h>

namespace citymaps
{
	template <>
	class Texture2D<RendererTypeOpenGL> : public BaseTexture2D
	{
		public:

			Texture2D(IGraphicsDevice* device, const TEXTURE_DESC& desc);

			~Texture2D();

			void CreateFromData(const void* data, unsigned int width,
					unsigned int height);
        
			void SetSubData(const void* data, unsigned int xOffset,
					unsigned int yOffset, int width, int height);

			void GenerateMipMaps(unsigned int numOfMipmaps);

			void Activate();
			void Activate(unsigned int texUnit);
			void Deactivate();
        
        void Invalidate()
        {
            Validatable::Invalidate();
            mTextureID = 0;
        }
        
        unsigned int GetTexID() const
        {
            return mTextureID;
        }

		private:

        	void CreateFromOurData();

		private:

			static const int msGLParameters[];

			static const int msGLFormats[];

			static const int msGLDataTypes[];

		private:

			GLuint mTextureID;
			GLuint mTexUnit;
	};
}
