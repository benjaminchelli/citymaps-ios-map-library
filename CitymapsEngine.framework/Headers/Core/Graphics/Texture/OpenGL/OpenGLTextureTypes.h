/*
 * TextureTypes.h
 *
 *  Created on: Jul 17, 2013
 *      Author: eddiekimmel
 */

//#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
//#endif

#pragma once

#ifndef GL_RED
#define GL_RED -1
#endif

#ifndef GL_RG
#define GL_RG -1
#endif
