/*
 * Texture2DOpenGL.h
 *
 *  Created on: Jul 17, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture1D.h>

namespace citymaps
{
	template <>
	class Texture1D<RendererTypeOpenGL> : public BaseTexture1D
	{
		public:

			Texture1D(IGraphicsDevice* device, const TEXTURE_DESC& desc);

			~Texture1D();

			void CreateFromData(void* data, unsigned int width);

			void SubFromData(void* data, unsigned int xOffset, int width);

			void GenerateMipMaps(unsigned int numOfMipmaps);

			void Activate();
			void Activate(unsigned int texUnit);
			void Deactivate();

		private:

			void CreateFromOurData();
		private:
			GLuint mTextureID;
	};
}
