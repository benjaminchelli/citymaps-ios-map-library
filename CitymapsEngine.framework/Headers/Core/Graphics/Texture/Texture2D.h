//
//  Texture.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Graphics/Texture/TextureTypes.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture.h>
#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Image.h>

namespace citymaps
{
	class ITexture2D: public ITexture
	{
		public:

			ITexture2D(IGraphicsDevice* device) :
					ITexture(device)
			{
			}
			virtual void CreateFromImage(const Image& image) = 0;

			virtual void CreateFromImage(const Image* image) = 0;

			virtual void CreateFromData(const void* data, unsigned int width,
					unsigned int height) = 0;

			virtual void SetSubData(const void* data, unsigned int xOffset,
					unsigned int yOffset, int width, int height) = 0;

			virtual Size GetSize() const = 0;
	};

	class BaseTexture2D: public ITexture2D
	{
		public:

			virtual ~BaseTexture2D()
			{
			}

			BaseTexture2D(IGraphicsDevice* device, const TEXTURE_DESC& desc) :
					ITexture2D(device), mTextureDesc(desc)
			{
			}

			void CreateFromImage(const Image& image)
			{
				Vector2i imageSize = image.GetDataSize();

                const void* data = image.GetImageDataPointer();
                if (data)
                {
                    this->CreateFromData(data, imageSize.x, imageSize.y);
                }
			}

			void CreateFromImage(const Image* image)
			{
			    if (image)
			    {
			        this->CreateFromImage(*image);
			    }
			}

			Size GetSize() const
			{
				return Size(mSize.x, mSize.y);
			}

		protected:

			const TEXTURE_DESC&
			GetDescription() const
			{
				return mTextureDesc;
			}

			void SetSize(const Vector2i& size)
			{
				mSize = size;
			}

			void SetBytesPerPixel(int num)
			{
				mBytesPerPixel = num;
			}

			Vector2i GetSizeInt()
			{
				return mSize;
			}
			TByteArray& GetData()
			{
				return mData;
			}
			int GetBytesPerPixel()
			{
				return mBytesPerPixel;
			}

		private:

			TByteArray mData;
			Vector2i mSize;
			int mBytesPerPixel;
			TEXTURE_DESC mTextureDesc;
	};

	template<typename Renderer>
	class Texture2D: public BaseTexture2D
	{

	};
}

