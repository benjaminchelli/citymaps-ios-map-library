/*
 * TextureTypes.h
 *
 *  Created on: Jul 18, 2013
 *      Author: eddiekimmel
 */

#pragma once

namespace citymaps
{
	enum TextureFormat
	{
        TextureFormatA,
		TextureFormatRGB,
		TextureFormatRGBA,
	};

	enum TextureDataType
	{
		TextureDataTypeUnsignedByte,
		TextureDataTypeByte,
		TextureDataTypeUnsignedShort,
		TextureDataTypeShort,
		TextureDataTypeUnsignedInt,
		TextureDataTypeInt,
		TextureDataTypeFloat
	};
    
    static const int TextureFormatNumComponents[] {1, 3, 4};
    static const int TextureDataTypeSize[] { 1, 1, 2, 2 ,4, 4, 4};

    // TODO: Add support for Mipmapping.
	enum TextureParameter
	{
		TextureParameterClampToEdge, TextureParameterRepeat, TextureParameterNearest, TextureParameterLinear
	};

	struct TEXTURE_DESC
	{
        TextureFormat DataFormat = TextureFormatRGBA;
        TextureDataType DataType = TextureDataTypeUnsignedByte;
        TextureFormat InternalFormat = TextureFormatRGBA;
        TextureParameter WrapMode = TextureParameterClampToEdge;
        TextureParameter MagFilter = TextureParameterLinear;
        TextureParameter MinFilter = TextureParameterLinear;
        int NumMipMaps = 0;
	};
}
