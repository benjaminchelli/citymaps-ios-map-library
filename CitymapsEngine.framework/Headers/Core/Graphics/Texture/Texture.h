/*
 * Texture.h
 *
 *  Created on: Jul 18, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/Core/Graphics/GraphicsObject.h>

namespace citymaps
{
	class IGraphicsDevice;
	class ITexture : public GraphicsObject
	{
		public:

        ITexture(IGraphicsDevice* device):GraphicsObject(device){}
		virtual ~ITexture()
		{}

		virtual void Activate() = 0;
		virtual void Activate(unsigned int texUnit) = 0;
		virtual void Deactivate() = 0;
	};
}
