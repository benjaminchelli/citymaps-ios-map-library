//
//  Texture.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/4/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Graphics/Texture/TextureTypes.h>
#include <CitymapsEngine/Core/Graphics/Texture/Texture.h>

namespace citymaps
{
	class ITexture1D: public ITexture
	{
		public:

        ITexture1D(IGraphicsDevice* device):ITexture(device){}
			virtual void CreateFromData(void* data, unsigned int width) = 0;

			virtual void SubFromData(void* data, unsigned int xOffset,
					int width) = 0;

			virtual void GenerateMipMaps(unsigned int numOfMipmaps) = 0;
	};

	class BaseTexture1D: public ITexture1D
	{
		public:

			virtual ~BaseTexture1D()
			{
			}

			BaseTexture1D(IGraphicsDevice* device, const TEXTURE_DESC& desc) :
					ITexture1D(device), mTextureDesc(desc)
			{
			}

		protected:

			TByteArray& GetData() {return mData;}
			int GetBytesPerPixel() {return mBytesPerPixel;}
			void SetBytesPerPixel(int num){mBytesPerPixel = num;}
			const TEXTURE_DESC&
			GetDescription() const
			{
				return mTextureDesc;
			}

		private:
			TByteArray mData;
			int mBytesPerPixel;
			TEXTURE_DESC mTextureDesc;
	};

	template<typename Renderer>
	class Texture1D: public BaseTexture1D
	{

	};
}

