//
//  GPSController.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class IGPSListener
    {
    public:
        virtual void OnPositionChange(const Point &position, double accuracyMeters = 0.0) = 0;
        virtual void OnDirectionChange(double direction) = 0;
        virtual void OnAltitudeChange(double altitude) = 0;
        
        virtual void OnLocationStatusChange(LocationStatus status) = 0;
    };
    
    class IGPSReader
    {
        
    };
    
    class GPSReader
    {
    public:
        GPSReader() :
            mUpdatingLocation(false),
            mUpdatingHeading(false),
            mLastDirection(0),
            mLastAltitude(0)
        {
        }
        
        virtual ~GPSReader()
        {
        }
        
        void AddListener(std::shared_ptr<IGPSListener> listener)
        {
            mListeners.push_back(std::weak_ptr<IGPSListener>(listener));
        }
        
        void RemoveListener(std::shared_ptr<IGPSListener> listener);
        
        void StartUpdatingAll()
        {
            this->StartGPSUpdates();
            this->StartDirectionUpdates();
        }
        
        void StopUpdatingAll()
        {
            this->StopGPSUpdates();
            this->StopDirectionUpdates();
        }
        
        virtual void StartGPSUpdates() { mUpdatingLocation = true; }
        virtual void StopGPSUpdates() { mUpdatingLocation = false; }
        virtual void StartDirectionUpdates() { mUpdatingHeading = true; }
        virtual void StopDirectionUpdates() { mUpdatingHeading = false; }
        
        virtual bool CanReceiveGPSUpdates() { return false; }
        
        void PositionChanged(Point &position, double accuracyMeters = 0.0);
        void DirectionChanged(double direction);
        void AltitudeChanged(double altitude);
        void LocationStatusChanged(LocationStatus status);
        
        const Point& GetLastPosition() const { return mLastPosition;}
        const double GetLastDirection() const { return mLastDirection;}
        const double GetLastAltitude() const { return mLastAltitude;}
        
    private:
        std::vector<std::weak_ptr<IGPSListener> > mListeners;
        bool mUpdatingLocation;
        bool mUpdatingHeading;
        Point mLastPosition;
        double mLastDirection;
        double mLastAltitude;
        LocationStatus mLastStatus;
        
    };
};
