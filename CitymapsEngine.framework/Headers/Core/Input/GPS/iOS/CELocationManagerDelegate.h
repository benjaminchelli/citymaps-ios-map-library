//
//  CELocationManagerDelegate.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/13/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#include <CitymapsEngine/Core/Input/GPS/GPSReader.h>

@interface CELocationManagerDelegate : NSObject<CLLocationManagerDelegate>

- (id)initWithGPSReader:(citymaps::GPSReader *)gpsReader;

- (BOOL)canReceiveUpdates;

@property (assign, nonatomic) citymaps::GPSReader *gpsReader;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

