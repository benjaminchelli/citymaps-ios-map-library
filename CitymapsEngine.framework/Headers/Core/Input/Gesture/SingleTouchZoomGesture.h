//
//  SingleTouchZoomGesture.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/6/15.
//  Copyright (c) 2015 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/Gesture/Gesture.h>

namespace citymaps
{
    class SingleTouchZoomGesture : public Gesture
    {
    public:
        SingleTouchZoomGesture(const std::vector<Point> &points);
        ~SingleTouchZoomGesture(){}
        
        void Update(const std::vector<Point> &points);
        
        float GetScaleFactor() const { return mScaleFactor; }
        const Point& GetInitialPoint() const { return mInitialPoint; }
        
        bool HasMoved() { return mHasMoved;}
        
    private:
        bool mHasInitialPoint;
        Point mInitialPoint;
        Point mLastPoint;
        float mScaleFactor;
        float mTotalScaleFactor;
        float mCurrentDist;
        bool mHasMoved;
    };
}