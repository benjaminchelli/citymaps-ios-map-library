//
//  TiltGesture.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 11/21/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/Gesture/Gesture.h>

namespace citymaps
{
    class TiltGesture : public Gesture
    {
    public:
        TiltGesture(const std::vector<Point> &points);
        ~TiltGesture();
        
        void Update(const std::vector<Point> &points);
        
        int GetDirection() { return mDirection; }
        
    private:
        int mDirection;
    };
}
