//
//  PanGesture.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/Gesture/Gesture.h>

namespace citymaps
{
    class PanGesture : public Gesture
    {
    public:
        PanGesture(const std::vector<Point> &points);
        ~PanGesture();
        
        void Update(const std::vector<Point> &points);
        
        const Point& GetCurrentDelta() { return mCurrentDelta; }
        const Point& GetTotalDelta() { return mTotalDelta; }
        
        bool HasMoved();
        void SetHasMoved();
        
    private:
        Point mCurrentDelta;
        Point mTotalDelta;
        Point mLastPoint;
        bool mStartMessageSent;
    };
};
