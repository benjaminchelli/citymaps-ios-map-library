#pragma once

#include <CitymapsEngine/Core/Util/AsyncJobQueue.h>

namespace citymaps
{
    typedef std::function<void()> DispatchCallback;
    
    typedef std::vector<DispatchCallback> DispatchList;
    
    class Dispatcher
    {
    public:
        
        /**
         * Dispatch a callback to be run on the main thread.
         */
        static void DispatchSynchronous(DispatchCallback callback, bool onNextFrame = false);
        
        /**
         * Dispatch a callback to be run on a background thread and optionally a completion callback to be called on the main thread.
         * Immediate jobs should be waited upon before finishing the current frame. The completion blocks of these will be called on WaitForImmediateJobs.
         */
        static void DispatchImmediateAsync(DispatchCallback callback, DispatchCallback completion = NULL);
        
        /**
         * Dispatch a callback to be run on a background thread and optionally a completion callback to be called on the main thread.
         * The completion block will called when finished during ProcessSynchronousCallbacks.
         */
        static void DispatchLongTermAsync(DispatchCallback callback, DispatchCallback completion = NULL);
        
        /**
         * Processes all synchronous jobs.
         */
        static void ProcessSynchronousCallbacks();
        
        /**
         * Clears the list of synchronous jobs.
         */
        static void ClearSynchronousCallbacks();
        
        /**
         * Halts until the immediate jobs are all finished.
         */
        static void WaitForImmediateJobs();
        
        static void SetThisAsMainThread();
        
    private:
        
        static SafeObject<DispatchList, std::recursive_mutex> mSynchronousJobs;
        static AsyncJobQueue sAsyncJobQueue;
        static std::thread::id sMainThreadId;
        static bool sMainThreadIdSet;
    };
}