#pragma once

#include <CitymapsEngine/Core/Globals.h>
#include <CitymapsEngine/Core/Logger.h>

#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/quaternion.hpp>

#ifdef __ANDROID__
namespace std
{
    template <typename T>
    std::string to_string(T value)
    {
        std::ostringstream os ;
        os << value ;
        return os.str() ;
    }
}
#endif

namespace citymaps
{
	typedef unsigned char byte;
	typedef double real;
	typedef long double real64;

	typedef glm::dvec2 Vector2;
	typedef glm::dvec3 Vector3;
	typedef glm::dvec4 Vector4;

	typedef glm::ivec2 Vector2i;
	typedef glm::ivec3 Vector3i;
	typedef glm::ivec4 Vector4i;

	typedef glm::vec2 Vector2f;
	typedef glm::vec3 Vector3f;
	typedef glm::vec4 Vector4f;

	typedef glm::dmat2 Matrix2;
	typedef glm::dmat3 Matrix3;
	typedef glm::dmat4 Matrix4;

	typedef glm::mat2 Matrix2f;
	typedef glm::mat3 Matrix3f;
	typedef glm::mat4 Matrix4f;

	static const real PI = 3.14159265359;

	static const double kMercatorMaxExtents[4] = { -20037508.34, -20037508.34,
			20037508.34, 20037508.34 };

    typedef std::vector<byte> TByteArray;
    typedef void (*AnimCompletionBlock)();
    

    /*
    * Static Initialization should be placed in EngineTypes.cpp
    */
    class PlatformType {};
    class PlatformTypeIOS : public PlatformType
    {
    public:
        static const std::string Name;
    };
    
    class PlatformTypeAndroid : public PlatformType
    {
    public:
        static const std::string Name;
    };
    
    class RendererType {};
    class RendererTypeOpenGL : public RendererType {};

    static const auto kEmptyLambda = []() {};

#ifdef __IOS__
	typedef PlatformTypeIOS CurrentPlatform;
	typedef RendererTypeOpenGL RecommendedRenderer;
#elif __ANDROID__
	typedef PlatformTypeAndroid CurrentPlatform;
	typedef RendererTypeOpenGL RecommendedRenderer;
#endif

	enum ImageType
	{
		ImageTypePNG,
		ImageTypeJPEG,
		ImageTypeGIF,
		ImageTypeBitmap,
		ImageTypeUnknown
	};

	enum NetworkResponseStatus
	{
		NetworkResponseSuccess,
		NetworkResponseTimeout,
        NetworkResponseRefused,
		NetworkResponseNotFound,
		NetworkResponseCancelled,
		NetworkResponseConnectFailed,
		NetworkResponseSendFailed,
		NetworkResponseInvalidInput,
        NetworkResponseInternalServerError,
		NetworkResponseUnknownError
	};

    // TODO: Refactor to "InputEventType"
    enum MapEventType
    {
        MapEventMoveStart,
        MapEventMoveEnd,
        MapEventZoomStart,
        MapEventZoomEnd,
        MapEventTouchUp,
        MapEventTouchDown,
        MapEventTouchMove,
        MapEventTap,
        MapEventDoubleTap,
        MapEventLongPress,
        MapEventRotateStart,
        MapEventRotateChange,
        MapEventRotateEnd,
        MapEventTiltStart,
        MapEventTiltChange,
        MapEventTiltEnd
    };
    
    enum LocationStatus
    {
        LocationStatusEnabled,
        LocationStatusDisabled
    };
    
	enum CacheDeleteStrategy
	{
		CacheDeleteStrategyLeastAccessed, CacheDeleteStrategyOldest
	};

	enum ContainmentType
	{
		ContainmentTypeContains,
		ContainmentTypeIntersects,
		ContainmentTypeDisjoint
	};

	template<typename T> int sgn(T val)
	{
		return (T(0) < val) - (val < T(0));
	}

	static inline char sign(int val)
	{
		return (val == 0 ? 0 : (val < 0 ? -1 : 1));
	}
    
    class Validatable
    {
    public:
        Validatable()
        :mIsValid(false)
        {
        }
        
        virtual bool IsValid() const {return mIsValid;}
        
        virtual void Invalidate() {mIsValid = false;}
        
    protected:
        
        bool IsValid(bool reset) {
            bool wasValid = mIsValid;
            if (reset)
                mIsValid = true;
            return wasValid;
        }
        void Validate() {mIsValid = true;}
        
    private:
        
        bool mIsValid;
    };

	class Point
	{
    public:
        Point() :
        x(0), y(0)
        {
        }
        Point(real _x, real _y) :
        x(_x), y(_y)
        {
        }
        
        Point(const Vector3& p)
        :x(p.x), y(p.y)
        {}
        
        Point(const Vector2& p)
        :x(p.x), y(p.y)
        {}
        
        double Angle(const Point &other) const
        {
            Point dif = *this - other;
            return glm::degrees(atan2(dif.x, dif.y)) + 180;
        }
        
        double Distance(const Point &other) const 
        {
            double dx = x - other.x;
            double dy = y - other.y;
            
            return std::sqrt((dx * dx) + (dy * dy));
        }
        
        double Distance2(const Point& other) const
        {
            double dx = x - other.x;
            double dy = y - other.y;
            
            return (dx * dx) + (dy * dy);
        }
        
        double Length() const
        {
            return std::sqrt( (x * x) + (y * y) );
        }
        
        double Length2() const
        {
            return (x * x) + (y * y);
        }
        
        bool Equals(const Point& other, double epsilon = 0.000001) const
        {
            return glm::epsilonEqual(x, other.x, epsilon) && glm::epsilonEqual(y, other.y, epsilon);
        }
        
        bool operator==(const Point &other) const
        {
            return x == other.x && y == other.y;
        }
        
        bool operator!=(const Point &other) const
        {
            return x != other.x || y != other.y;
        }
        
        operator Vector3() const
        {
            return Vector3(x, y, 0);
        }
        
        operator Vector2() const
        {
            return Vector2(x, y);
        }
        
        Point& operator+=(const Point &delta)
        {
            x += delta.x;
            y += delta.y;
            
            return *this;
        }
        
        template<typename T>
        Point& operator/=(T value)
        {
            x /= value;
            y /= value;
            return *this;
        }
        
        Point operator+(const Point &other) const
        {
            return Point(x + other.x, y + other.y);
        }
        
        Point& operator-=(const Point &delta)
        {
            x -= delta.x;
            y -= delta.y;
            
            return *this;
        }
        
        Point operator-(const Point &other) const
        {
            return Point(x - other.x, y - other.y);
        }
        
        Point operator/(double s) const
        {
        	return Point(x / s, y / s);
        }

        Point operator*(double s) const
        {
        	return Point(x * s, y * s);
        }
        
        void operator*=(double s)
        {
            x *= s;
            y *= s;
        }
        real x, y;
	};
    
    static const Point kZeroPoint;
    
	class Size
	{
    public:
        Size() :
        width(0), height(0)
        {
        }

        Size(real _width, real _height) :
        width(_width), height(_height)
        {
        }

        Size(const Vector2i& vec) :
        width(vec.x), height(vec.y)
        {
        }
        
        Size(const Point &point) :
        width(point.x), height(point.y)
        {
        }
        
        bool Equals(const Size& other, real epsilon = 0.000001) const
        {
            return glm::epsilonEqual(width, other.width, epsilon) && glm::epsilonEqual(height, other.height, epsilon);
        }
        
        double Area() const
        {
            return width * height;
        }
        
        bool operator==(Size &other)
        {
            return width == other.width && height == other.height;
        }
        bool operator!=(Size &other)
        {
            return width != other.width || height != other.height;
        }
        
        void operator+=(const Size& s)
        {
            width += s.width;
            height += s.height;
        }
        
        void operator-=(const Size& s)
        {
            width -= s.width;
            height -= s.height;
        }
        
        void operator/=(double s)
        {
            width /= s;
            height /= s;
        }
        
        void operator*=(double s)
        {
            width *= s;
            height *= s;
        }
        
        Size operator+(const Size& s) const
        {
            return Size(width + s.width, height + s.height);
        }
        
        Size operator-(const Size& s) const
        {
            return Size(width - s.width, height - s.height);
        }
        
        Size operator/(real s) const
        {
            return Size(width / s, height / s);
        }
        
        Size operator*(real s) const
        {
            return Size(width * s, height * s);
        }
        
        real width, height;
	};
    
    static const Size kZeroSize;
    
	class Bounds
	{
    public:
        Bounds();
        Bounds(const Point &_min, const Point &_max);
        Bounds(real minX, real minY, real maxX, real maxY);
        Bounds(const Point &origin, const Size &size);
        Bounds(const Point& position, const Size& size, const Point& anchorPoint, const Size& offset);
        
        Point GetCenter() const;
        real Width() const;
        real Height() const;
        
        Size GetSize() const;
        bool ContainsPoint(const Point &point) const;
        bool ContainsPoint(double x, double y) const;
        bool ContainsBounds(const Bounds &other) const;
        bool IntersectsBounds(const Bounds &other) const;
        bool IntersectsBounds(real minX, real minY, real maxX, real maxY);
        bool Intersection(const Bounds &other, Bounds &outBounds) const;
        
        Point WrapPoint(const Point& p);
        bool Equals(const Bounds& other, double epsilon = 0.000001) const;
        void Offset(const Point& p);
        
        bool operator==(const Bounds &other);
        bool operator!=(const Bounds &other);
        
        Bounds operator +(const Bounds& other);
        Bounds& operator+=(const Bounds &other);
        Bounds& operator+=(const Point &point);
        Bounds& operator*=(double s);
        Bounds operator/(double s) const;
        Bounds operator*(double s) const;

        Point min, max;
	};
    
    static const Bounds kZeroBounds;
    
	class GridPoint
	{
    public:
        GridPoint() :
        x(0), y(0), zoom(0), uid(0)
        {
        }
        
        GridPoint(uint64_t _uid) :
        uid(_uid)
        {
            zoom = (uid >> 56);
            x = (uid << 8 >> 36);
            y = (uid << 36 >> 36);
        }
        
        // 8-28-28
        GridPoint(int32_t _x, int32_t _y, short _zoom) :
        absX(_x), absY(_y), x(_x), y(_y), zoom(_zoom)
        {
            int maxX = (1 << zoom) - 1;
            if (absX > maxX) {
                x = absX - maxX - 1;
            } else if(absX < 0) {
                x = absX + maxX + 1;
            }
            
            uint64_t zoom64 = (uint64_t) zoom;
            uint64_t x64 = (uint64_t) (x << 4 >> 4);
            uint64_t y64 = (uint64_t) (y << 4 >> 4);
            uid = (zoom64 << 56) + (x64 << 28) + y64;
        }
        
        bool Equals(const GridPoint &other)
        {
            return uid == other.uid;
        }
        
        size_t operator() (const GridPoint& gp) const
        {
            return gp.uid;
        }
        
        bool operator==(const GridPoint& gp) const
        {
            return uid == gp.uid;
        }
        

        
        int32_t x;
        int32_t y;
        int32_t absX;
        int32_t absY;
        short zoom;
        uint64_t uid;
	};
    
	class Ray
	{
    public:
        Ray() :
        origin(Vector3()), direction(Vector3())
        {
        }
        Ray(const Vector3 &_origin, const Vector3 &_direction,
            bool normalize = false) :
        origin(_origin), direction(_direction)
        {
            if (normalize)
            {
                direction = glm::normalize(direction);
            }
        }
        
        Vector3 PointAtLocation(real t)
        {
            return origin + (t * direction);
        }
        
        Vector3 origin, direction;
	};
    
    class Polygon
    {
    public:
        Polygon()
        {
        }
        
        ~Polygon()
        {
        }
        
        void AddPoint(const Point &point) { points.push_back(point); }
        
        bool ContainsPoint(const Point &point) { return this->ContainsPoint(point.x, point.y); }
        bool ContainsPoint(real x, real y);
        
        std::vector<Point> points;
    };
    
	class Line
	{
    public:
        Line() :
        p1(Vector3()), p2(Vector3())
        {
        }
        Line(Vector3 _p1, Vector3 _p2) :
        p1(_p1), p2(_p2)
        {
        }
        
        bool IntersectsBounds(const Bounds &bounds, Line *clippedLine = NULL);
        bool ClippedLineToBounds(const Bounds &bounds, Line &outLine);
        bool ClipToPolygon(Polygon &poly, Line *clippedLine);
        
        Vector3 p1, p2;
	};
    
	class Plane
	{
    public:
        Plane() :
        plane(Vector4()), normal(Vector3()), d(0)
        {
        }
        Plane(const Vector4 &_plane, bool normalize = true) :
        plane(_plane), normal(Vector3(_plane)), d(_plane.w)
        {
            if (normalize)
            {
                Normalize();
            }
        }
        Plane(real _a, real _b, real _c, real _d, bool normalize = true) :
        plane(Vector4(_a, _b, _c, _d)), normal(Vector3(_a, _b, _c)), d(_d)
        {
            if (normalize)
            {
                Normalize();
            }
        }
        
        void Normalize();
        
        Plane Transform(Matrix4 transform)
        {
            transform = glm::inverse(glm::transpose(transform));
            return Plane(plane * transform);
        }
        
        Vector3 PointOnPlane(Vector3 point)
        {
            real t = (glm::dot(normal, point) + d)
            / glm::dot(normal, normal);
            return point - (normal * t);
        }
        
        bool Intersects(Ray ray, Vector3 *pOut);
        bool Intersects(Line line, Vector3 *pOut);
        
        static bool IntersectionThreePlanes(const Plane &p1,
                                            const Plane &p2, const Plane &p3, Vector3 *pOut);
        
        Vector4 plane;
        Vector3 normal;
        real d;
	};
};

