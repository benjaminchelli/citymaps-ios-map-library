#pragma once

// C RunTime Header Files
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <cstdlib>

// STL Libraries
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>
#include <memory>
#include <list>
#include <thread>
#include <algorithm>
#include <mutex>
#include <queue>

// GLM Libraries
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#ifdef __APPLE__
    #include "TargetConditionals.h"
    #if defined(TARGET_OS_IPHONE) || defined(TARGET_IPHONE_SIMULATOR)
        #define __IOS__
    #endif
    #include <libkern/OSAtomic.h>
#endif

#include <CitymapsEngine/Core/Util/DeviceSpecification.h>

static const int kCETileCacheVersion = 1;

// Notifications
static const std::string kCEMoveStartNotification = "moveStart";
static const std::string kCEMoveEndNotification = "moveEnd";
static const std::string kCEZoomStartNotification = "zoomStart";
static const std::string kCEZoomEndNotification = "zoomEnd";
static const std::string kCENetworkStatusChangedNotification = "networkStatusChanged";

// Messages
static const std::string kCEVectorLineRenderMessages = "vectorLineRender";
static const std::string kCEVectorPolygonRenderMessages = "vectorPolygonRender";
static const std::string kCELogoUpdateMessages = "logoLayerUpdate";
static const std::string kCELogoRenderMessages = "logoLayerRender";
static const std::string kCEEngineUpdateMessages = "engineUpdate";
static const std::string kCEMarkerRenderMessages = "markerLayerRender";
static const std::string kCEMarkerGroupUpdateMessages = "markerGroupUpdate";
static const std::string kCELabelRenderMessages = "labelLayerRender";
