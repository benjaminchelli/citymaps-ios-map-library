//
//  Exception.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/14/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

namespace citymaps
{
    enum ExceptionType
	{
		BadParameterException,
		NullException,
		IOException,
		UnsupportedTypeException,
		NetworkException,
		InternalException,
		NumberException,
        InvalidOperationException
	};
};
