//
//  Header.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    class BoundingBox
    {
    public:
        BoundingBox()
        {}
        
        BoundingBox(const Bounds& bounds)
        : min(bounds.min.x, bounds.min.y, 0), max(bounds.max.x, bounds.max.y, 0)
        {
        }
        
        BoundingBox(const Vector3 &_min, const Vector3 _max) :
            min(_min), max(_max)
        {}
        
        BoundingBox(real minX, real minY, real minZ, real maxX, real maxY, real maxZ) :
            min(minX, minY, minZ), max(maxX, maxY, maxZ)
        {}
        
        bool Contains(const Vector3 &point) const
        {
            return !(min.x > point.x ||
                     min.y > point.y ||
                     min.z > point.z ||
                     max.x < point.x ||
                     max.y < point.y ||
                     max.z < point.z);
        }
        
        bool Contains(const BoundingBox &other) const
        {
            return !(min.x > other.min.x ||
                     min.y > other.min.y ||
                     min.z > other.min.z ||
                     max.x < other.max.x ||
                     max.y < other.max.y ||
                     max.z < other.max.z);
        }
        
        bool Intersects(const BoundingBox &other) const
        {
            return !(min.x > other.max.x ||
                     min.y > other.max.y ||
                     min.z > other.max.z ||
                     max.x < other.min.x ||
                     max.y < other.min.y ||
                     max.z < other.min.z);
        }
        
        Vector3 Center() const
        {
            return (min + max) * 0.5;
        }
        
        real Width() const
        {
            return (max.x - min.x);
        }
        
        real Height() const
        {
            return (max.y - min.y);
        }
        
        real Depth() const
        {
            return (max.z - min.z);
        }
        
        Vector3 Dimensions() const
        {
            return max - min;
        }
        
        
        Vector3 min, max;
    };
};
