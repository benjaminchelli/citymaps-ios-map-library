//
//  CapsuleShape.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/8/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Physics/Shapes/GeometricShape.h>

namespace citymaps
{
    class CapsuleShape : public BaseGeometricShape
    {
    public:
        CapsuleShape();
        CapsuleShape(const Vector3 &p1, const Vector3 &p2, real radius);
        ~CapsuleShape();
        
        void SetPoints(const Vector3 &p1, const Vector3 &p2);
        void SetRadius(real radius);
        
        const Vector3& GetPoint1() const { return mAbsoluteP1; }
        const Vector3& GetPoint2() const { return mAbsoluteP2; }
        const Vector3& GetDirection() const { return mDirection; }
        real GetRadius() const { return mRadius; }
        
        bool Contains(const Vector3 &point) const
        {
            return this->GetBoundingBox().Contains(point);
        }
        
        bool Contains(const BoundingBox &box) const
        {
            return this->GetBoundingBox().Contains(box);
        }
        
        bool Intersects(const BoundingBox &box) const
        {
            return IntersectionTest::CapsuleIntersectsBoundingBox(this, box);
        }
        
        bool Intersects(const IGeometricShape* other) const { return other->Intersects(this); }
        bool Intersects(const AABBShape* other) const
        {
            return IntersectionTest::AABBIntersectsCapsule(other, this);
        }
        
        bool Intersects(const OBBShape* other) const
        {
            return IntersectionTest::OBBIntersectsCapsule(other, this);
        }
        
        bool Intersects(const CapsuleShape* other) const
        {
            return IntersectionTest::CapsuleIntersectsCapsule(this, other);
        }
        
    private:
        Vector3 mP1;
        Vector3 mP2;
        Vector3 mAbsoluteP1;
        Vector3 mAbsoluteP2;
        Vector3 mDirection;
        real mRadius;
        
        void CalculateBoundingBox();
        
        void Refresh();
    };
};

