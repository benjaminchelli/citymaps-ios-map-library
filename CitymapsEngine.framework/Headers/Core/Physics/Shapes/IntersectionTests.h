//
//  IntersectionTests.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Physics/Shapes/BoundingBox.h>

namespace citymaps
{
    class AABBShape;
    class OBBShape;
    class CapsuleShape;
    class IntersectionTest
    {
    public:
        
        static bool AABBIntersectsAABB(const AABBShape *aabb1, const AABBShape *aabb2);
        static bool AABBIntersectsOBB(const AABBShape *aabb, const OBBShape *obb);
        static bool AABBIntersectsCapsule(const AABBShape *aabb, const CapsuleShape *capsule);
        static bool OBBIntersectsOBB(const OBBShape *obb1, const OBBShape *obb2);
        static bool OBBIntersectsCapsule(const OBBShape *obb, const CapsuleShape *capsule);
        static bool CapsuleIntersectsCapsule(const CapsuleShape *capsule1, const CapsuleShape *capsule2);
        static bool CapsuleIntersectsBoundingBox(const CapsuleShape *capsule, BoundingBox box);
    };
};
