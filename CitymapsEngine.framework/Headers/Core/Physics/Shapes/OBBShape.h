//
//  OBBShape.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Physics/Shapes/GeometricShape.h>

namespace citymaps
{
    class OBBShape : public BaseGeometricShape
    {
    public:
        OBBShape();
        OBBShape(const Vector3 &center, const Vector3 &dimensions, const Matrix3 &axes);
        ~OBBShape()
        {
        }
        
        void SetCenter(const Vector3 &center)
        {
            mCenter = center;
            this->CalculateBoundingBox();
        }
        
        void SetDimensions(const Vector3 &dimensions)
        {
            mDimensions = dimensions;
            this->CalculateBoundingBox();
        }
        
        void Set(const Vector3 &center, const Vector3 &dimensions)
        {
            mCenter = center;
            mDimensions = dimensions;
            this->CalculateBoundingBox();
        }
        
        void SetTransform(const Matrix3 &transform);
        
        const Vector3& GetCenter() const { return mCenter; }
        const Vector3& GetDimensions() const { return mDimensions; }
        const Matrix3& GetAxes() const { return mAxes; }
        
        bool Contains(const Vector3 &point);
        bool Contains(const BoundingBox &box);
        bool Intersects(const BoundingBox &box);
        
        bool Intersects(const IGeometricShape* other) const { return other->Intersects(this); }
        bool Intersects(const AABBShape* other)const
        {
            return IntersectionTest::AABBIntersectsOBB(other, this);
        }
        
        bool Intersects(const OBBShape* other)const
        {
            return IntersectionTest::OBBIntersectsOBB(this, other);
        }
        
        bool Intersects(const CapsuleShape* other)const
        {
            return IntersectionTest::OBBIntersectsCapsule(this, other);
        }
        
        const Vector3& GetVertex(int i) const { return mVertices[i]; }
        
    private:
        Vector3 mCenter;
        Vector3 mDimensions;
        Matrix3 mAxes;
        Vector3 mVertices[8];
        
        void CalculateBoundingBox();
    };
};