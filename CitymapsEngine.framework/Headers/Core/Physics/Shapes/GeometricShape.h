//
//  GeometricShape.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Physics/Shapes/BoundingBox.h>
#include <CitymapsEngine/Core/Physics/Shapes/IntersectionTests.h>

namespace citymaps
{
    enum GeometricShapeType
    {
        GeometricShapeAABB,
        GeometricShapeOBB,
        GeometricShapeCapsule
    };
    
    class IGeometricShape
    {
    public:
        
        virtual ~IGeometricShape(){}
        virtual bool Contains(const Vector3 &point) const = 0;
        virtual bool Contains(const BoundingBox &box) const = 0;
        virtual bool Intersects(const BoundingBox &box) const = 0;
        
        virtual bool Intersects(const IGeometricShape* other) const = 0;
        virtual bool Intersects(const AABBShape* other) const = 0;
        virtual bool Intersects(const OBBShape* other) const = 0;
        virtual bool Intersects(const CapsuleShape* other) const = 0;
        
        virtual void SetPosition(const Vector3 &position) = 0;
        virtual void SetTransform(const Matrix3 &transform) = 0;
        
        virtual const Vector3& GetPosition() const = 0;
        virtual const Matrix3& GetTransform() const = 0;
        
        virtual const BoundingBox& GetBoundingBox() const = 0;
        
        virtual GeometricShapeType GetType() const = 0;
        
        virtual void SetActive(bool active) = 0;
        virtual bool IsActive() const = 0;
        
        virtual void SetUserPointer(void* ptr) = 0;
        virtual void* GetUserPointer() = 0;
    };
    
    class BaseGeometricShape : public IGeometricShape
    {
    public:
        BaseGeometricShape(GeometricShapeType type) :
        mType(type), mActive(true), mUserPtr(NULL)
        {
        }
        
        virtual ~BaseGeometricShape()
        {
        }
        
        virtual void SetPosition(const Vector3 &position)
        {
            mPosition = position;
            this->CalculateBoundingBox();
        }
        
        virtual const Vector3& GetPosition() const { return mPosition; }
        
        virtual void SetTransform(const Matrix3 &transform)
        {
            mTransform = transform;
            this->CalculateBoundingBox();
        }
        
        virtual const Matrix3& GetTransform() const { return mTransform; }
        
        const BoundingBox& GetBoundingBox() const { return mBoundingBox; }
        
        GeometricShapeType GetType() const { return mType; }
        
        void SetActive(bool active)
        {
            mActive = active;
        }
        
        bool IsActive() const
        {
            return mActive;
        }
        
        void SetUserPointer(void* ptr)
        {
            mUserPtr = ptr;
        }
        
        void* GetUserPointer()
        {
            return mUserPtr;
        }
        
    protected:
        BoundingBox& GetMutableBoundingBox() { return mBoundingBox; }
        
        void SetBoundingBox(const BoundingBox &box) { mBoundingBox = box; }
        
    private:
        BoundingBox mBoundingBox;
        Vector3 mPosition;
        Matrix3 mTransform;
        GeometricShapeType mType;
        bool mActive;
        void* mUserPtr;
        virtual void CalculateBoundingBox() = 0;
    };
    
    
};