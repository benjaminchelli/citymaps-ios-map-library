//
//  Physics.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma

#include <CitymapsEngine/Core/Physics/Shapes/AABBShape.h>
#include <CitymapsEngine/Core/Physics/Shapes/OBBShape.h>
#include <CitymapsEngine/Core/Physics/Shapes/CapsuleShape.h>
#include <CitymapsEngine/Core/Physics/Shapes/BoundingBox.h>
#include <CitymapsEngine/Core/Physics/Shapes/GeometricShape.h>
#include <CitymapsEngine/Core/Physics/Shapes/IntersectionTests.h>
#include <CitymapsEngine/Core/Physics/Collision/CollisionGroup.h>