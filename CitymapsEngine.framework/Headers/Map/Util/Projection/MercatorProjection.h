//
//  MercatorProjection.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Util/Projection/Projection.h>

namespace citymaps
{
    static const Bounds kMercatorMaxBounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34);
    
    class MercatorProjection : public Projection
    {
    public:
        MercatorProjection()
        {
        }
        
        ~MercatorProjection()
        {
        }
        
        Point Project(const Point &point);
        Point Unproject(const Point &point);
        Bounds Project(const Bounds &bounds);
        Bounds Unproject(const Bounds &bounds);
        Bounds GetMaxBounds() { return kMercatorMaxBounds; }
    };
};
