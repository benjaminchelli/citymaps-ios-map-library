//
//  MapTypes.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/20/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    typedef std::function<void(bool)> AnimationCallback;
    static const AnimationCallback kEmptyAnimationCallback = [](bool){};
    
    static const double kMaxZoomLevel = 30;
    
    class Region
    {
    public:
        Region() :
            radius(0)
        {
        }
        
        Region(const Point &_center, real _radius) :
            center(_center), radius(_radius)
        {
        }
        
        bool Contains(const Point &point) const;
        
        bool active;
        Point center;
        real radius;
    };
    
    enum MapOption
    {
        MapOptionKineticPan, // Turn kinetic pan on / off
        MapOptionKineticZoom, // Turn kinetic zoom on / off
        MapOptionKineticRotate, // Turn kinetic rotate on / off
        MapOptionAnimation, // Turn animations on / off
        MapOptionUpdate, // Level of complexity in the map update loop
        MapOptionRotation, // Turn rotation on / off
        MapOptionPanGesture, // Turn pan gesture on / off
        MapOptionZoomGesture, // Turn zoom gesture on / off
        MapOptionRotateGesture, // Turn rotate gesture on / off
        MapOptionTiltGesture, // Turn tilt gesture on / off
        MapOptionDynamicGestureTypes, // Turn dynamic gesture switching on / off
        MapOptionNumOptions
    };
    
    enum MapOptionValue
    {
        MapOptionOff = 0,
        MapOptionOn,
        MapOptionLow,
        MapOptionMedium,
        MapOptionHigh
    };
    
    enum MapFeatureType
    {
        MapFeatureLine = 0,
        MapFeaturePolygon = 1,
        MapFeaturePoint = 2
    };
    
    enum MapFeatureBadgeType
    {
        MapFeatureBadgeNone,
        MapFeatureBadgeLoop,
        MapFeatureBadgeCounty,
        MapFeatureBadgeState,
        MapFeatureBadgeNational,
        MapFeatureBadgeInterstate,
        MapFeatureBadgeNumBadgeTypes
    };
    
    struct MapPosition
    {
        MapPosition() :
        zoom(0), tilt(0), orientation(0) {}
        
        MapPosition(const Point &_center, double _zoom, double _orientation, double _tilt) :
        center(_center), zoom(_zoom), tilt(_tilt), orientation(_orientation) {}
        
        bool operator==(const MapPosition& other) const
        {
            if (zoom != other.zoom)
                return false;
            if (tilt != other.tilt)
                return false;
            if (orientation != other.orientation)
                return false;
            if (center != other.center)
                return false;
            
            return true;
        }
        
        bool operator!=(const MapPosition& other) const
        {
            return !(*this == other);
        }
        
        Point center;
        double zoom;
        double tilt;
        double orientation;
    };
};
