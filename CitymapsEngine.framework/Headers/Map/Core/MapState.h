//
//  MapState.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 8/23/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/EngineTypes.h>
#include <CitymapsEngine/Map/Core/MapTypes.h>
#include <CitymapsEngine/Core/Util/NetworkStatus.h>
#include <glm/gtc/epsilon.hpp>

namespace citymaps
{
    enum MapMovementState
    {
        MapMovementIdle,
        MapMovementSlow,
        MapMovementMedium,
        MapMovementFast
    };
    
    enum MapZoomState
    {
        MapZoomIdle,
        MapZoomIn,
        MapZoomOut
    };
    
    class Projection;
    
    class MapState
    {
        friend class Map;
        
    public:
        
        MapState()
        :mScale(0), mDuration(0), mFrameTimestep(0), mResolution(0), mBounds(0.0, 0.0, 0.0, 0.0), mMoveDirection(0), mMoveVelocity(0), mZoomState(MapZoomIdle), mSize(0,0), mProjection(NULL), mOrientation(0), mIsRotating(false), mIsTilting(false)
        {}

        MapMovementState GetMovementState(double slowThreshold = 0.01, double normalThreshold = 10.0, double FastThreshold = 50.0 ) const;
        
        bool IsMoving() const
        {
            return this->IsMoving(0);
        }
        
        bool IsMoving(double moveThreshold) const
        {
            return (mMoveVelocity > moveThreshold);
        }
        
        bool IsIdle(double moveThreshold = 0.01, double forDuration = 0) const
        {
            return ( mZoomState == MapZoomIdle && mMoveVelocity <= moveThreshold && !mIsRotating && !mIsTilting && !mAnimating && mDuration >= forDuration);
        }
        
        bool IsZooming() const
        {
            return mZoomState != MapZoomIdle;
        }
        
        bool HasResized() const
        {
            return mHasResized;
        }
        
        MapZoomState GetZoomState() const
        {
            return mZoomState;
        }
        
        bool IsZoomingOut() const
        {
            return mZoomState == MapZoomOut;
        }
        
        bool IsZoomingIn() const
        {
            return mZoomState == MapZoomIn;
        }
        
        bool IsRotating() const
        {
            return mIsRotating;
        }
        
        bool IsAnimating() const
        {
            return mAnimating;
        }
        
        bool IsTilting() const
        {
            return mIsTilting;
        }
        
        const MapPosition& GetMapPosition() const { return mPosition;}
        
        double GetMoveDirection() const {return mMoveDirection;}
        double GetMoveVelocity() const {return mMoveVelocity;}
        const Bounds& GetBounds() const {return mBounds;}
        const Point& GetCenter() const {return mPosition.center;}
        int GetZoomLevel() const {return mPosition.zoom;}
        int GetScale() const {return mScale;}
        const Size& GetSize() const { return mSize;}
        double GetResolution() const {return mResolution;}
        int GetDuration() const {return mDuration;}
        int GetFrameTimestep() const {return mFrameTimestep;}
        Projection* GetProjection() const {return mProjection;}
        double GetOrientation() const {return mOrientation;}
        
        bool IsAtZoom(int testZoom) const { return mPosition.zoom == testZoom;}
        bool IsAtResolution(double testResolution) const { return glm::epsilonEqual(testResolution, mResolution, 0.0001); }
            
    private:
        
        MapPosition mPosition;
        int mScale;
        int mDuration;
        int mFrameTimestep;
        double mOrientation;
        bool mIsRotating;
        bool mHasResized;
        bool mIsTilting;
        Bounds mBounds;
        Size mSize;
        double mMoveDirection;
        double mMoveVelocity;
        MapZoomState mZoomState;
        double mResolution;
        Projection* mProjection;
        bool mAnimating;
        NetworkStatus mNetworkStatus;
    };

}