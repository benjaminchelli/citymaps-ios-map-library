//
//  KineticZoom.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/27/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <deque>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    class IKineticZoomListener
    {
        
    public:
        virtual void OnKineticZoomStart() = 0;
        virtual void OnKineticZoomBy(double scale, const Point& midPoint) = 0;
        virtual void OnKineticZoomEnd() = 0;
    };
    
    class KineticZoom
    {
    public:
        
        KineticZoom(IKineticZoomListener* listener)
        :mListener(listener), mIsAnimating(false)
        {
            
        }
        
        void Start();
        void AddScale(double scale);
        void End(double scale, const Point& midpoint);
        bool IsAnimating() const {return mIsAnimating;}
        void Update(int deltaMS);
        void Stop();
        
    private:
        
        typedef struct {
            double scale = 0;
            double startScale = 0;
            double scaleDif = 0;
            double accumScale = 1;
            double damping = 0;
            double elapsedTime = 0;
            double animateTime = 0;
            Point midPoint = Point(0,0);
        } KineticZoomData;
        
        typedef struct {
            double scale = 0;
            double time = 0;
        } KineticZoomPoint;
        
        IKineticZoomListener* mListener;
        KineticZoomData mData;
        std::deque<KineticZoomPoint> mPoints;
        bool mIsAnimating;
    };
}