//
//  RotateAnimation.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/22/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Timer.h>

namespace citymaps
{
    class IRotateAnimationListener
    {
    public:
        virtual void OnRotateStart() = 0;
        virtual void OnRotateMove(double angle) = 0;
        virtual void OnRotateEnd() = 0;
    };
    
    class RotateAnimation
    {
    public:
        RotateAnimation(IRotateAnimationListener *listener);
        ~RotateAnimation();
        
        void Animate(real fromAngle, real toAngle, double duration, AnimationCallback callback = kEmptyAnimationCallback);
        bool IsAnimating() { return mAnimating; }
        
        void Update();
        void Stop()
        {
            if (mAnimating)
            {
                mCallback(false);
            }
            mAnimating = false;
        }
        
    private:
        int mCurrentStep;
        int mNumSteps;
        Timer *mAnimTimer;
        real mFromAngle;
        real mToAngle;
        real mDeltaOrientation;
        real mCurrentAngle;
        IRotateAnimationListener *mListener;
        AnimationCallback mCallback;
        bool mAnimating;
        
        void AnimationStep();
    };
};
