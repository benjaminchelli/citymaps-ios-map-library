//
//  PanZoomAnimation.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/2/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Renderer/MapCamera.h>
#include <CitymapsEngine/Core/Util/Timer.h>

namespace citymaps
{
    class IMapPositionAnimatorListener
    {
    public:
        virtual void OnAnimationStart() = 0;
        virtual void OnAnimationMove(const Vector3 &position, double tilt, double rotation) = 0;
        virtual void OnAnimationEnd() = 0;
    };
    
    class MapPositionAnimator
    {
    public:
        MapPositionAnimator(IMapPositionAnimatorListener *listener);
        ~MapPositionAnimator();
        
        void Animate(const MapPosition &fromPosition, const MapPosition &toPosition, double duration, MapCamera *camera, AnimationCallback callback = kEmptyAnimationCallback);
        bool IsAnimating() const { return mAnimating; }
        
        void Update(int deltaMs);
        
        void Stop()
        {
            if (mAnimating)
            {
                auto callback = mCallback;
                mCallback = NULL;
                if (callback)
                    callback(false);
            }
            mAnimating = false;
        }
        
    private:
        double mElapsedtime;
        double mDuration;
        Vector3 mPoints[3];
        MapPosition mFromPosition;
        MapPosition mToPosition;
        double mMinimumAngle;
        IMapPositionAnimatorListener *mListener;
        AnimationCallback mCallback;
        bool mAnimating;
        
        void AnimationStep(int deltaMs);
        
    };
};
