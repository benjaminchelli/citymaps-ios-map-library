//
//  KineticRotate.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 8/12/14.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <deque>
#include <CitymapsEngine/Core/EngineTypes.h>

namespace citymaps
{
    class IKineticRotateListener
    {
        
    public:
        virtual void OnKineticRotateStart() = 0;
        virtual void OnKineticRotateBy(double angleDelta) = 0;
        virtual void OnKineticRotateEnd() = 0;
    };
    
    class KineticRotate
    {
    public:
        
        KineticRotate(IKineticRotateListener* listener)
        :mListener(listener), mIsAnimating(false)
        {
            
        }
        
        void Start();
        void AddAngle(double angle, double time);
        void End(double angle, double time);
        bool IsAnimating() const {return mIsAnimating;}
        void Update(int deltaMS);
        void Stop();
        
    private:
        
        typedef struct {
            double accumDelta = 0;
            double finalDelta = 0;
            double elapsedTime = 0;
        } KineticRotateData;
        
        typedef struct {
            double angle = 0;
            double time = 0;
        } KineticRotatePoint;
        
        IKineticRotateListener* mListener;
        KineticRotateData mData;
        std::deque<KineticRotatePoint> mPoints;
        bool mIsAnimating;
    };
}