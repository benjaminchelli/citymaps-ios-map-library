//
//  AnimationScript.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/22/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Map.h>

namespace citymaps
{
    enum AnimationType
    {
        AnimationTypeMapPosition,
        AnimationTypeMove,
        AnimationTypeMoveZoom,
        AnimationTypeZoom,
        AnimationTypeRotate
    };
    
    struct AnimationStep
    {
        AnimationStep()
        :position(0,0), zoom(0), orientation(0), delay(0)
        {}
        
        AnimationStep(const MapPosition& p)
        :position(p.center), zoom(p.zoom), orientation(p.orientation), tilt(p.tilt), delay(0)
        {
        }
        
        AnimationType type;
        Point position;
        int zoom;
        double orientation;
        float delay;
        float tilt;
    };
    
    class AnimationScript
    {
    public:
        AnimationScript()
        {
        }
        
        ~AnimationScript()
        {
        }
        
        void MoveTo(const MapPosition &position, real delay = 0);
        void MoveTo(const Point &point, real delay = 0);
        void MoveTo(const Point &point, int zoom, real delay = 0);
        void ZoomTo(int zoom, real delay = 0);
        void RotateTo(double orientation, real delay = 0);
        
        void Play(std::shared_ptr<Map> map);
        
    private:
        std::vector<AnimationStep> mSteps;

        void NextStep(std::shared_ptr<Map> map, std::vector<AnimationStep> steps, bool animated);
    };
};
