//
//  MapMovementController.h
//  vectormap2
//
//  Created by Adam Eskreis on 6/11/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Input/TouchListener.h>
#include <CitymapsEngine/Map/Animation/KineticPan.h>
#include <CitymapsEngine/Map/Animation/KineticZoom.h>
#include <CitymapsEngine/Map/Animation/KineticRotate.h>

namespace citymaps
{
    struct MapEvent
    {
        MapEventType type;
        
        MapEvent() :
        type(MapEventTouchDown)
        {}
        
        MapEvent(MapEventType _type) :
        type(_type)
        {}
    };
    
    struct MapTouchEvent
    {
        MapEventType type;
        Point point;
        
        MapTouchEvent() :
        type(MapEventTouchDown)
        {}
        
        MapTouchEvent(MapEventType _type, const Point &_point) :
        type(_type),
        point(_point)
        {}
    };
    
    class IMapMovementListener
    {
    public:
        virtual void MovedBy(const Vector3 &delta) = 0;
        virtual void RotatedBy(const float angularDelta) = 0;
        virtual void ScaledBy(const float scale, const Point &midPoint) = 0;
        virtual void TiltedBy(const float tilt) = 0;
        
        virtual void OnMapEvent(const MapEvent &e) = 0;
        virtual void OnMapTouchEvent(const MapTouchEvent &e) = 0;
    };
    
    class Gesture;
    class Timer;
    class Map;
    class MapMovementController : public ITouchListener,
                                  public IKineticPanListener,
                                  public IKineticZoomListener,
                                  public IKineticRotateListener
    {
    public:
        MapMovementController(Map* map);
        ~MapMovementController();
        
        void AddMovementListener(IMapMovementListener *listener)
        {
            if (listener) {
                mListeners.push_back(listener);
            }
        }
        
        void RemoveMovementListener(IMapMovementListener* listener);

        void Update(int deltaMS);
        
        std::shared_ptr<Gesture> GetCurrentGesture() {return mCurrentGesture;}
        
        void StopKinetics()
        {
            mKineticPan.Stop();
            mKineticZoom.Stop();
            mKineticRotate.Stop();
        }
        void ResetGesture();
        
        /* ITouchListener methods */
        void OnTouchEvent(const TouchEvent &e);
        
        /* IKineticPanListener methods */
        void OnKineticMoveStart();
        void OnKineticMoveBy(const Vector3 &delta);
        void OnKineticMoveEnd();
        
        /* IKineticZoomListener methods */
        void OnKineticZoomStart();
        void OnKineticZoomBy(double scale, const Point& midPoint);
        void OnKineticZoomEnd();
        
        /* IKineticRotateListener methods */
        void OnKineticRotateStart();
        void OnKineticRotateBy(double angleDelta);
        void OnKineticRotateEnd();

        void StopKineticMove() { mKineticPan.Stop(); }
        void StopKineticZoom() { mKineticZoom.Stop(); }
        
    private:
        std::vector<IMapMovementListener *> mListeners;
        std::shared_ptr<Gesture> mCurrentGesture;
        int mNumTaps;
        bool mGestureIsMove;
        bool mGestureIsZoom;
        bool mGestureIsRotate;
        double mTouchStartTime;
        std::unique_ptr<Timer> mLongPressTimer;
        std::unique_ptr<Timer> mDoubleTapTimer;
        Map* mMap;
        KineticPan mKineticPan;
        KineticZoom mKineticZoom;
        KineticRotate mKineticRotate;
        
        Point mTapPoint;
        std::vector<TouchEvent> mMovePoints;
        
        void TouchStart(const TouchEvent &e);
        void TouchMove(const TouchEvent &e);
        void TouchEnd(const TouchEvent &e);
        void TouchCancel(const TouchEvent &e);
        
        void PostMapEvent(MapEventType type);
        void PostMapTouchEvent(MapEventType type, const Point &p);
        
        void MovedBy(const Vector3 &delta);
        void RotatedBy(const float angularDelta);
        void ScaledBy(const float scale, const Point &midPoint);
        void TiltedBy(const float tilt);
        
        void StopLongPressTimer();
        void StopDoubleTapTimer();
        
        void DetermineGesture(const TouchEvent &e);
        std::shared_ptr<Gesture> CreateGesture(const std::vector<TouchEvent> &points, int previousTaps = 0);
        
    };
}
