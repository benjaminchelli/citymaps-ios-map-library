//
//  TileLayer.h
//  vectormap2
//
//  Created by Lion User on 07/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Layer.h>
#include <CitymapsEngine/Core/Util/MemoryCache.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>
#include <CitymapsEngine/Map/Layer/TileSource/TileSource.h>
#include <CitymapsEngine/Core/Util/Image.h>

namespace citymaps
{
    class Sprite;
    class Tile;
    
    typedef SafeObject<std::map<uint64_t, std::shared_ptr<Tile>>, std::recursive_mutex> TTileMap;
    typedef SafeObject<std::set<uint64_t>, std::recursive_mutex> TProxyTileSet;
    
    struct TILE_LAYER_DESC : public LAYER_DESC
    {
        TILE_LAYER_DESC() :
            TileSize(256),
            Buffer(0),
            LayerTileSource(NULL),
            HasProxyTiles(false)
            {}
        
        TILE_LAYER_DESC& SetHasProxyTiles(bool has)
        {
            HasProxyTiles = has;
            return *this;
        }
        
        bool HasProxyTiles;
        int TileSize;
        int Buffer;
        TileSource* LayerTileSource;
        std::set<int> LODZoomLevels;
    };
    
    class TileLayer : public Layer, ITileSourceListener
    {
    public:
        TileLayer(TILE_LAYER_DESC &layerDesc);
        virtual ~TileLayer();
        
        virtual void SetMap(Map *map);
        virtual void SetTileSource(TileSource* tileSource);
        
        virtual void OnDisable();
        virtual void OnNetworkStatusChanged(NetworkStatus status);
        virtual void Update(const MapState& state);
        virtual void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        virtual bool RequiresUpdate(const MapState &state);
        
        void LimitMemory();
        
        bool GetLODZoomForMapZoom(int mapZoom, int& outLODZoom);
        int GetMaxLODZoom() { return *mLODZoomLevels.rbegin();}
        int GetMinLODZoom() { return *mLODZoomLevels.begin();}
        
        GridPoint GetGridPointContainingLonLat(const Point& lonLat, int zoom);
        
        void SetProxyTilesEnabled(bool enabled) {mProxyTilesEnabled = enabled;}
        void SetProxyTileImage(std::shared_ptr<Image> image);
        virtual void ResetTiles();
        
        void SetTileBuffer(int buffer) { mTileBuffer = buffer; }
        void SetPixelBuffer(int buffer) { mPixelBuffer = buffer; }
        
        double TileLength(int zoom);
        bool TilesReady();
        
        static GridPoint WrapGridPoint(const GridPoint& p);
        
        /* ITileSourceListener methods */
        void OnTileRetrieveSuccess(const GridPoint& gp, std::shared_ptr<Tile> tile);
        void OnTileRetrieveFailure(const GridPoint& gp, TileSourceStatus status);
        
        TTileMap GetOnScreenTiles()
        {
            std::lock_guard<TTileMap> lock(mOnScreenTiles);
        	return mOnScreenTiles;
        }
        
        TProxyTileSet GetProxyTiles()
        {
            std::lock_guard<TProxyTileSet> lock(mProxyTiles);
            return mProxyTiles;
        }
        
    protected:

        TileSource* GetTileSource() { return mTileSource; }
        
        const int GetTileSize() const { return mTileSize; }
        
        void StopAllTileDownloads();
        
        virtual void AddTile(std::shared_ptr<Tile> tile, uint64_t key);
        virtual void RemoveTile(uint64_t key);
        
    private:

        int mTileSize;
        TTileMap mOnScreenTiles;
        TProxyTileSet mProxyTiles;
        
        int mTileBuffer;
        int mPixelBuffer;
        Bounds mTileBounds;
        Bounds mBufferedTileBounds;
        int mMaxTileNumX;
        int mMaxTileNumY;
        std::shared_ptr<Sprite> mProxySprite;
        bool mProxyTilesEnabled;
        
        TileSource* mTileSource;
        
        int mZoom;
        int mLODZoom;
        
        std::set<int> mLODZoomLevels;
            
        void LoadSpiral(const Bounds &tileOrigin, int zoom);
        void LoadBackToFront(const Bounds &tileOrigin, int zoom);
        void RemoveOffscreenTiles(int zoom);
        Bounds CalculateTileOrigin(const Bounds &bounds, int zoom);
        Size CalculateGridSize(const Bounds &tileOrigin, const Bounds &mapBounds, int zoom);
        Bounds CalculateTileBounds(const Bounds &tileOrigin, const Bounds &mapBounds, int zoom);
        void CancelOffZoomDownloads(int zoom);
        
        std::shared_ptr<Tile> GetActiveTile(uint64_t key);
        
    };
};
