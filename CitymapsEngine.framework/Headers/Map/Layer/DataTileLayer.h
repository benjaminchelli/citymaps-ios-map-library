//
//  DataTileLayer.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/11/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileLayer.h>

namespace citymaps
{
    class DataTileDelegate
    {
    public:
        
        virtual void OnTileRetrieveSuccess(const GridPoint& gp, const TByteArray& data) = 0;
        virtual void OnTileRetrieveFailure(const GridPoint& gp, TileSourceStatus status) = 0;
        virtual void OnTileRemoved(const GridPoint& gp) = 0;
        
    };
    
    class DataTileLayer : public TileLayer
    {
    public:
        
        DataTileLayer(TILE_LAYER_DESC &layerDesc);

        virtual void Update(const MapState& state);

        void SetDelegate(DataTileDelegate* delegate);
        DataTileDelegate* GetDelegate() { return mDelegate;}
        
        virtual void OnTileRetrieveFailure(const GridPoint& gp, TileSourceStatus status);
        
    private:
        
        DataTileDelegate* mDelegate;
    };
}