//
//  CanvasCircle.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/Canvas/CanvasEllipse.h>

namespace citymaps
{
    class CanvasCircle : public CanvasEllipse
    {
    public:
        
        CanvasCircle():CanvasEllipse()
        {}
        
        void SetRadius(float r)
        {
            this->SetRadii(r,r);
        }
        
        float GetRadius()
        {
            return this->GetRadii().width;
        }
    };
}