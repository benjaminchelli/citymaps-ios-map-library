/*
 * Canvas.h
 *
 *  Created on: Aug 15, 2013
 *      Author: eddiekimmel
 */

#pragma once

#include <CitymapsEngine/Map/Layer/CanvasLayer.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasShapeGroup.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasLine.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasRectangle.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasSquare.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasEllipse.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasCircle.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasPolygon.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasImage.h>
