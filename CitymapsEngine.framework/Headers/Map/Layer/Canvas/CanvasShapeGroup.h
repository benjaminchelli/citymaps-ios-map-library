//
//  CanvasShapeGroup.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Map/Core/MapState.h>
namespace citymaps
{
    class IGraphicsDevice;
    class CanvasShape;
    class CanvasLayer;
    
    class CanvasShapeGroup
    {
    public:
        
        CanvasShapeGroup(const std::string &name, CanvasLayer* layer);
        ~CanvasShapeGroup();
        
        void AddCanvasShape(CanvasShape *shape);
        void RemoveCanvasShape(CanvasShape *shape);
        void RemoveAllCanvasShapes();
        
        void SetActive(bool active) {mActive = active;}
        bool IsActive() {return mActive;}
        void Update(const MapState& state);
        void Render(IGraphicsDevice* device, RenderState &perspectiveState, RenderState& orthoState);
        
        bool ProcessTouchEvent(MapEventType type, const Point &p);
        
    private:
        
        std::string mName;
        std::vector<CanvasShape *> mShapes;
        CanvasLayer* mLayer;
        bool mActive;
    };
}
