//
//  ImageTile.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/22/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Map/Layer/Tile.h>

namespace citymaps
{
    class TileLayer;
    class Sprite;
    class ImageTile : public Tile
    {
    public:
        ImageTile(GridPoint cell, TileLayer *layer);
        ~ImageTile();

        void Update(const MapState& state);
        void SetData(const TByteArray &data);
        
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        std::shared_ptr<Image> mTileImage;
        Sprite *mTileSprite;
        float mAlpha;
    };
};
