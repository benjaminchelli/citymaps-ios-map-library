//
//  DataTile.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/11/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/EngineTypes.h>
#include <CitymapsENgine/Map/Layer/TileSource/TileSource.h>
#include <CitymapsEngine/Map/Layer/Tile.h>

namespace citymaps
{
    class DataTile : public Tile
    {
    public:
        
        DataTile(GridPoint gridPoint, TileLayer *layer);
        ~DataTile();
        
        void SetData(const TByteArray &data);
    };
}