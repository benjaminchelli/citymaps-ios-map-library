//
//  ImageTileFactory.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/TileFactory.h>
#include <CitymapsEngine/Map/Layer/ImageTile.h>

namespace citymaps
{
    class ImageTileFactory : public BaseTileFactory
    {
    public:
        
        ITileFactory* Clone() { return new ImageTileFactory(*this); }
        
    protected:
        
        std::shared_ptr<Tile> AllocateTile(const GridPoint& gp, TileLayer* layer)
        {
            return std::make_shared<ImageTile>(gp, layer);
        }
    };
}