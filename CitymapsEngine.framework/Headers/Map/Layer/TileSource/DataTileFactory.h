//
//  DataTileFactory
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Layer/TileSource/TileFactory.h>
#include <CitymapsEngine/Map/Layer/DataTile.h>

namespace citymaps
{
    class DataTileFactory : public BaseTileFactory
    {
    public:
        
        ITileFactory* Clone() { return new DataTileFactory(*this); }
        
    protected:
        
        std::shared_ptr<Tile> AllocateTile(const GridPoint& gp, TileLayer* layer)
        {
            return std::make_shared<DataTile>(gp, layer);
        }
    };
}