//
//  DataSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/23/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/SafeObject.h>
#include <CitymapsEngine/Core/Util/Util.h>
#include <CitymapsEngine/Core/Util/MemoryCache.h>
#include <set>

namespace citymaps
{
    enum DataSourceStatus
    {
        DataSourceStatusSuccess,
        DataSourceStatusCancelled,
        DataSourceStatusNotFound,
        DataSourceStatusRefused,
        DataSourceStatusTimedOut,
        DataSourceStatusInternalServerError,
        DataSourceStatusUnknown
    };
    
    struct DATA_SOURCE_DESC
    {
        DATA_SOURCE_DESC()
        :MemoryCacheSize(3)
        {}
        
        virtual ~DATA_SOURCE_DESC(){}
        
        int MemoryCacheSize;
    };
    
    typedef SafeObject<std::set<uint64_t>, std::recursive_mutex> TDataSourceOperations;

    class IDataSourceListener
    {
    public:
        virtual void OnDataRetrieveSuccess(const GridPoint& gridPoint, std::shared_ptr<TByteArray> data) = 0;
        virtual void OnDataRetrieveFailure(const GridPoint& gridPoint, DataSourceStatus status) = 0;
    };
    
    class IPathTransformer
    {
    public:
        virtual std::string GeneratePath(const GridPoint& cell, const std::string uri) = 0;
        virtual std::string GenerateCacheFilename(const std::string &url) = 0;
        
        virtual ~IPathTransformer() {}
    };
    
    class IDataSource
    {
    public:
        
        virtual ~IDataSource(){}
        
        virtual void SetListener(IDataSourceListener* listener) = 0;
        virtual bool IsRetrievingData(const GridPoint& gp) = 0;
        
        virtual void SetCacheEnabled(bool enabled) = 0;
        
        /** Returns whether or not the new operation can / should begin **/
        virtual bool RetrieveData(const GridPoint& gp) = 0;
        virtual void Cancel(const GridPoint& gp) = 0;
        virtual void CancelAll() = 0;
        
        virtual TDataSourceOperations GetActiveOperations() = 0;
        
        virtual void ClearCache() = 0;
        
        virtual bool IsReady() = 0;
        
        virtual std::string GeneratePath(const GridPoint& cell, const std::string& uri) = 0;
        virtual std::string GenerateCachePath(const GridPoint& cell) = 0;
    };
    
    class BaseDataSource : public IDataSource
    {
    public:
        
        BaseDataSource(const DATA_SOURCE_DESC& desc);
        
        virtual ~BaseDataSource();
        
        void AddPathTransformer(IPathTransformer *transformer)
        {
            mPathTransformers.push_back(transformer);
        }
        
        virtual void SetListener(IDataSourceListener* listener) {mListener = listener;}
        
        virtual void SetCacheEnabled(bool enabled){mCacheEnabled = enabled;}
        
        virtual bool IsRetrievingData(const GridPoint& gp);
        
        /** This method will return true if the subclass is expeced to begin the retrieval.
         * False will be returned if either listener is NULL, or if the request is already being processed
         */
        virtual bool RetrieveData(const GridPoint& gp);
        
        virtual void Cancel(const GridPoint& gp);
        
        virtual void CancelAll();
        
        std::string GeneratePath(const GridPoint& cell, const std::string& uri);
        virtual std::string GenerateCachePath(const GridPoint& cell) = 0;
        
        virtual void ClearCache() { mMemoryCache.Clear(); }
        
        TDataSourceOperations GetActiveOperations();
        
        virtual bool IsReady() { return true; }
        
    protected:
        
        void OnRetrieveSuccess(const GridPoint& gp, std::shared_ptr<TByteArray> data, bool cache = true);
        void OnRetrieveFailure(const GridPoint& gp, DataSourceStatus status);
        
        const std::vector<IPathTransformer *>& GetPathTransformers()
        {
            return mPathTransformers;
        }
        
    private:
        
        TDataSourceOperations mOperations;
        MemoryCache<std::shared_ptr<TByteArray>> mMemoryCache;
        IDataSourceListener* mListener;
        bool mCacheEnabled;
        std::vector<IPathTransformer *> mPathTransformers;
        
        std::shared_ptr<TByteArray> GetCachedData(uint64_t key);
        
    };
}