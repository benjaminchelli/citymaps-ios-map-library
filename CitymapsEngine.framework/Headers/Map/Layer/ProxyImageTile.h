//
//  ProxyImageTile.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Image.h>
#include <CitymapsEngine/Map/Layer/Tile.h>

namespace citymaps
{
    class TileLayer;
    class Sprite;
    class ProxyImageTile : public Tile
    {
    public:
        ProxyImageTile(std::shared_ptr<Sprite> proxySprite, GridPoint cell, TileLayer *layer);
        
        void SetData(const TByteArray &data){};
        
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
    private:
        std::shared_ptr<Sprite> mSprite;
    };
};
