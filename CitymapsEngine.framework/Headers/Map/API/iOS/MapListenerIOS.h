//
//  MapListenerIOS.h
//  vectormap2
//
//  Created by Eddie Kimmel on 7/31/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Map.h>
#include <CitymapsEngine/Map/ApI/iOS/CEMapView.h>

namespace citymaps
{
    class MapListenerIOS : public IMapListener
    {
    public:
        
        void SetDelegate(CEMapView *view, id<CEMapViewDelegate> delegate)
        {
            mCEMapView = view;
            mDelegate = delegate;
        }
        
        void SetDelegate(id<CEMapViewDelegate> delegate)
        {
            mDelegate = delegate;
        }
        
        void SetMapView(CEMapView *view)
        {
            mCEMapView = view;
        }
        
        /* Map methods */
        void OnMoveStart();
        void OnMoveEnd();
        void OnZoomStart();
        void OnZoomEnd();
        void OnTouchUp(const Point &p);
        void OnTouchDown(const Point &p);
        void OnTap(const Point &p);
        void OnDoubleTap(const Point &p);
        void OnLongPress(const Point &p);
        void OnRotateStart(double orientation);
        void OnOrientationChange(double orientation);
        void OnRotateEnd(double orientation);
        void OnTiltStart(double tilt);
        void OnTiltChange(double tilt);
        void OnTiltEnd(double tilt);
        /* Location methods */
        void OnUserLocationChange(const UserLocation &location);
        void OnEnterRegion(int index);
        void OnExitRegion(int index);
        
    private:
        
        __weak id<CEMapViewDelegate> mDelegate;
        __weak CEMapView* mCEMapView;

    };
}
