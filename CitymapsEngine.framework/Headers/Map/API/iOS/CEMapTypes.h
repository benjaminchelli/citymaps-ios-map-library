//
//  CEMapTypes.h
//  vectormap2
//
//  Created by Lion User on 10/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#ifdef __cplusplus
extern "C"
{
#endif
    
#import <Foundation/Foundation.h>
typedef enum {
    kCEMapOptionKineticPan,
    kCEMapOptionKineticZoom,
    kCEMapOptionKineticRotate,
    kCEMapOptionAnimation,
    kCEMapOptionUpdate,
    kCEMapOptionRotation,
    kCEMapOptionPanGesture,
    kCEMapOptionZoomGesture,
    kCEMapOptionRotateGesture,
    kCEMapOptionTiltGesture,
    kCEMapOptionDynamicGestureTypes
} CEMapOption;

typedef enum {
    kCEMapOptionOff = 0,
    kCEMapOptionOn,
    kCEMapOptionLow,
    kCEMapOptionMedium,
    kCEMapOptionHigh
} CEMapOptionValue;

/**
 * This class defines a longitude, latitude pair.
 */
typedef struct {
    double lon;
    double lat;
} CELonLat;

typedef struct {
    CELonLat min, max;
} CELonLatBounds;

typedef enum {
    kCEMapProjectionMercator,
    kCEMapProjectionLonLat
} CEMapProjection;

typedef struct {
    CELonLat center;
    float zoom;
    double orientation;
    double tilt;
} CEMapPosition;
    
static inline CELonLat CELonLatMake(double lon, double lat)
{
    CELonLat point;
    point.lon = lon;
    point.lat = lat;
    return point;
}
    
extern const CELonLat CELonLatZero;
extern const CELonLatBounds CELonLatBoundsZero;

static inline bool CELonLatEqual(CELonLat l1, CELonLat l2)
{
    return l1.lon == l2.lon && l1.lat == l2.lat;
}
    
static inline bool CELonLatEqualEpsilon(CELonLat l1, CELonLat l2, float epsilon)
{
    return fabs(l1.lon - l2.lon) < epsilon && fabs(l1.lat - l2.lat) < epsilon;
}
    
static inline CELonLat CELonLatMakeCenterOfBounds(CELonLatBounds bounds)
{
    CELonLat center;
    center.lon = (bounds.min.lon + bounds.max.lon) * 0.5;
    center.lat = (bounds.min.lat + bounds.max.lat) * 0.5;
    return center;
}
static inline CELonLatBounds CELonLatBoundsMake(double minLon, double minLat, double maxLon, double maxLat)
{
    CELonLatBounds bounds;
    bounds.min = CELonLatMake(minLon, minLat);
    bounds.max = CELonLatMake(maxLon, maxLat);
    return bounds;
}
    
static inline BOOL CELonLatBoundsContainsLonLat(CELonLatBounds bounds, CELonLat point)
{
    return !(bounds.min.lon > point.lon ||
             bounds.min.lat > point.lat ||
             bounds.max.lon < point.lon ||
             bounds.max.lat < point.lat);
}
    
static inline BOOL CELonLatBoundsContainsLonLatBounds(CELonLatBounds bounds1, CELonLatBounds bounds2)
{
    return !(bounds1.min.lon > bounds2.min.lon ||
             bounds1.min.lat > bounds2.min.lat ||
             bounds1.max.lon < bounds2.max.lon ||
             bounds1.max.lat < bounds2.max.lat);
}

static inline CEMapPosition CEMapPositionMake(CELonLat center, float zoom, double orientation, double tilt)
{
    CEMapPosition position;
    position.center = center;
    position.zoom = zoom;
    position.orientation = orientation;
    position.tilt = tilt;
    
    return position;
}

static inline CELonLatBounds CELonLatBoundsMakeWithPoints(CELonLat min, CELonLat max)
{
    CELonLatBounds bounds;
    bounds.min = min;
    bounds.max = max;
    return bounds;
}
    
double CELonLatDistanceMeters(CELonLat p1, CELonLat p2);
#ifdef __cplusplus
}
#endif
