//
//  CEOverlay.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 12/30/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>
#import <CitymapsEngine/Map/API/iOS/CEMapTypes.h>

static const int kCEUnusedDimension = -1;

/**
 * A class representing an image overlayed on top of the map.
 */
@interface CEOverlay : CEFeature

- (id)initWithImage:(UIImage*)image;
- (id)initWithResource:(NSString*)resource;
- (id)initWithFile:(NSString*)file;

@property (assign, nonatomic) float alpha;
@property (assign, nonatomic) CGPoint anchorPoint;
@property (assign, nonatomic) CELonLat position;
@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) float rotation;

@end
