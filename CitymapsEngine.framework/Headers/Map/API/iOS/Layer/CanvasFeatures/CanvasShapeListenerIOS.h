//
//  CanvasFeatureListenerIOS.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 5/2/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Canvas/CanvasShape.h>

#import <CitymapsEngine/Map/API/iOS/Layer/CanvasFeatures/CEFeature.h>

namespace citymaps
{
    class CanvasShapeListenerIOS : public ICanvasShapeListener
    {
    public:
        CanvasShapeListenerIOS(id<CEFeatureDelegate> delegate, CEFeature *feature);
        ~CanvasShapeListenerIOS();
        
        bool OnCanvasShapeEvent(CanvasShapeEvent event, CanvasShape* shape);
        
    private:
        id<CEFeatureDelegate> mDelegate;
        CEFeature *mFeature;
    };
}
