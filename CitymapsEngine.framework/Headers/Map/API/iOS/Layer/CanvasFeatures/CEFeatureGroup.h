//
//  CEFeatureGroup.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/19/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CEFeature;

/** A feature group is a container which can be used to add CEFeatures to the map.
 
 Features groups allow you to organize your features so you can perform batch operations on multiple markers at once.
 Features groups should NOT be explicitely constructed.  Instead, you should use [CECanvasLayer featureGroupWithName:] to obtain a feature group correctly.
 
 */
@interface CEFeatureGroup : NSObject
{
    NSMutableArray *_features;
}

/**
 @name Initializers
 */

/** Initializes a new feature group for the given name.
 @param name The name of the feature group.
 @return a new feature group.
 */
- (id)initWithName:(NSString *)name;

/**
 * @name Features
 */

/** Adds a feature to the feature group.
 * @param feature The feature to add to the feature group.
 */
- (void)addFeature:(CEFeature*)feature;

/** Removes a feature from the feature group.
 * @param feature The feature to remove from the feature group.
 */
- (void)removeFeature:(CEFeature*)feature;

/** Removes all feature from the feature group.
 */
- (void)removeAllFeatures;

/** 
 * @name Operations
 */

/** 
 Activates all features in this feature group. Activated features will be shown.
 */
- (void)activate;
 
/** 
 Deactivates all features in this feature group. Deactivated features will not be shown.
 */
- (void)deactivate;

/**
 @name Properties
 */

/** The name of this feature group */
@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic, readonly) NSArray *features;

@end
