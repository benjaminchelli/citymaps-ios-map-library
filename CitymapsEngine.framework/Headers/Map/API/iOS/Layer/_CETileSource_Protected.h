//
//  _CETileSource_Protected.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/Layer/CETileSource.h>
#import <CitymapsEngine/Map/API/iOS/Layer/CEDataSource.h>
#import <CitymapsEngine/Map/Layer/TileSource/TileSource.h>
#import <CitymapsEngine/Map/Layer/TileSource/TileFactory.h>

@interface CETileSource()

@property (assign, nonatomic) citymaps::TileSource* tileSourcePointer;
@property (assign, nonatomic) citymaps::ITileFactory* tileFactoryPointer;

- (citymaps::ITileFactory *)createTileFactory;

@end