
#include <Foundation/Foundation.h>
#include <CitymapsEngine/Map/API/iOS/Layer/CEDataTileLayer.h>
#include <CitymapsEngine/Map/Layer/DataTileLayer.h>
#include <CitymapsEngine/Map/API/iOS/CETile.h>

namespace citymaps
{
    class DataTileLayerDelegateIOS : public DataTileDelegate
    {
    public:
        
        void SetDelegate(id <CEDataTileLayerDelegate> delegate)
        {
            mDelegate = delegate;
        }
        
        void SetLayer(CEDataTileLayer *layer)
        {
            mUserLayer = layer;
        }
        
        void OnTileRetrieveSuccess(const GridPoint& gp, const TByteArray& data);
        void OnTileRetrieveFailure(const GridPoint& gp, TileSourceStatus status);
        void OnTileRemoved(const GridPoint& gp);
        
    private:
        __weak CEDataTileLayer *mUserLayer;
        __weak id <CEDataTileLayerDelegate> mDelegate;
        
    };
}