//
//  CELayer_CELayerExtension.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/Layer/CELayer.h>
#import <CitymapsEngine/Map/Layer/Layer.h>

@interface CELayer ()

@property (assign, nonatomic) std::shared_ptr<citymaps::Layer> enginePointer;

- (citymaps::Layer *)createLayer:(CELayerOptions *)options;
- (void)applyOptions:(CELayerOptions *)options toDesc:(citymaps::LAYER_DESC *)desc;
- (void)preUpdate;
- (void)postUpdate;

@end
