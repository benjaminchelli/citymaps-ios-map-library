#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/Layer/CEDataSource.h>

/** This is an abstract base class for all tile source.
 
 A CETileSource provides the information needed for a CETileLayer to render properly.  A tile source consists of two parts: A data source and a tile factory.  
 
 The tile factory is internal to the engine is not a detail of concern for the user of this SDK.  All that needs to be known is that each derived class of CETileSource must implement a tile factory.
 
 The data source, implemented as a CEDataSource, is a description object which allows the user to specify a source for tile data.  This data source must be a Mercator based tile source.  What this means is that each tile must be accessible with an x and y coordinate in the Mercator projection of the world, plus a zoom level.  An example of a mercator tile source is the OpenStreetMap web tile server.  
 */

@interface CETileSource : NSObject

@property (strong, nonatomic, readonly) CEDataSource* dataSource;
@property (assign, nonatomic) BOOL cacheEnabled;

/** 
 * @name Initializers
 *  
 */

/** Initialize a tile source with a data source and a list of options.
 @param dataSource The CEDataSource to be used for this tile source.
 */
- (id)initWithDataSource:(CEDataSource *)dataSource;

@end
