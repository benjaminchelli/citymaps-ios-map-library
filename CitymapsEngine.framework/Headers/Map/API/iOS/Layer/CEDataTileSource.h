//
//  CEDataTileSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/11/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import "CETileSource.h"

/** The tile source to be used with a CEDataTileLayer. **/
@interface CEDataTileSource : CETileSource

/** Initializes the CEDataTileSource to read tiles from disk, in the given directory. */
- (id)initWithFilepath:(NSString *)filepath;

/** Initializes the CEDataTileSource to read tiles from a url. */
- (id)initWithURL:(NSString *)url;

@end
