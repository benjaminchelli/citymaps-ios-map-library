//
//  CEMarker_Protected.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/CEMarker.h>

#import <CitymapsEngine/Map/Marker/ImageMarker.h>
#import <CitymapsEngine/Map/API/iOS/MarkerListenerIOS.h>

@interface CEMarker()

@property (assign, nonatomic) citymaps::ImageMarker *enginePointer;
@property (assign, nonatomic) std::shared_ptr<citymaps::MarkerListenerIOS> markerListener;

/** The map this marker is attached to currently. */
@property (nonatomic, weak) CEMapView *map;

- (void)postUpdate;

@end
