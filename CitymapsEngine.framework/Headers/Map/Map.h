#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Application.h>
#include <CitymapsEngine/Map/Animation/MapPositionAnimator.h>
#include <CitymapsEngine/Map/Input/MapMovementController.h>
#include <CitymapsEngine/Map/Input/MapUserLocationController.h>
#include <CitymapsEngine/Map/Renderer/MapCamera.h>
#include <CitymapsEngine/Map/Util/Projection/Projection.h>
#include <CitymapsEngine/Map/Core/MapState.h>
#include <CitymapsEngine/Map/Util/MapUtil.h>

namespace citymaps
{
    
    class OrthoCamera;
	class Layer;
	class Marker;
	class MarkerGroup;
    class Scene;
    class IGraphicsDevice;
    class AsyncJobQueue;
    class MarkerManager;
    class MapMovementController;
    class CollisionGroup;
    class IGPSPredictor;
    
	typedef std::vector<std::shared_ptr<Layer> > TLayerList;
    typedef std::vector<Marker *> TMarkerList;
    
    static const std::string kDefaultMarkerGroup = "ENGINE_DEFAULT_MARKER_GROUP";

    class IMapListener
    {
    public:
        
        virtual ~IMapListener(){}
        
        /* Map methods */
        virtual void OnMoveStart() = 0;
        virtual void OnMoveEnd() = 0;
        virtual void OnZoomStart() = 0;
        virtual void OnZoomEnd() = 0;
        virtual void OnTouchUp(const Point &p) = 0;
        virtual void OnTouchDown(const Point &p) = 0;
        virtual void OnTap(const Point &p) = 0;
        virtual void OnDoubleTap(const Point &p) = 0;
        virtual void OnLongPress(const Point &p) = 0;
        virtual void OnRotateStart(double orientation) = 0;
        virtual void OnOrientationChange(double orientation) = 0;
        virtual void OnRotateEnd(double orientation) = 0;
        virtual void OnTiltStart(double tilt) = 0;
        virtual void OnTiltChange(double tilt) = 0;
        virtual void OnTiltEnd(double tilt) = 0;
        
        /* Location methods */
        virtual void OnUserLocationChange(const UserLocation &location) = 0;
        virtual void OnEnterRegion(int index) = 0;
        virtual void OnExitRegion(int index) = 0;
    };

    struct MAP_DESC
    {
        MapPosition Position = MapPosition(Point(0,0), 2.0f, 0.0, 0.0);
        float MinZoom = 2;
        float MaxZoom = 30;
        Size MapSize = Size(0,0);
        ProjectionType Projection = ProjectionMercator;
    };
    
    enum TrackingMode
    {
        TrackingModeNone,
        TrackingModePosition,
        TrackingModePositionOrientation
    };
    
	class Map : public IMapMovementListener,
                public IMapPositionAnimatorListener,
                public std::enable_shared_from_this<Map>,
                public Validatable,
                public IApplicationListener,
                public ITouchListener
	{
	public:
		Map(MAP_DESC &mapDesc);
		virtual ~Map();
        
        bool Initialize();
        void Enable(std::shared_ptr<IApplication> app);
        void Disable();

        void SetOption(MapOption option, MapOptionValue setting);
        MapOptionValue GetOption(MapOption option);
        
        void LimitMemory();
        
        virtual void Invalidate();
        
        void SetBaseLayer(std::shared_ptr<Layer> layer);
		void AddLayer(std::shared_ptr<Layer> layer);
        void RemoveLayer(std::shared_ptr<Layer> layer);

        /* Not exposed to user */
        void SetGPSPredictor(IGPSPredictor *predictor);
        
        void SetTrackingMode(TrackingMode mode);
        
        void SetLocationMarkerVisible(bool visible);
        
        /* Will still not appear if the marker itself is not visible. */
        void SetLocationAccuracyVisible(bool visible);
        
        /* IApplicationListener methods */
        void OnResize(int width, int height);
        void OnUpdate(IApplication* application, int deltaMS);
        void OnRender(IApplication* application, int deltaMS);
        void OnNetworkStatusChanged(IApplication* application, NetworkStatus status);
        
		/* Moving methods */
		void SetCenter(const Point &point, double durationMs = 0.0, AnimationCallback = kEmptyAnimationCallback);
        void SetCenter(const Point &point, float zoom, double durationMs = 0.0, AnimationCallback = kEmptyAnimationCallback);
        
        void MoveByPixels(const Point& delta, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
        void MoveToScreenPosition(const Point& pos, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
        void MovePixelToPixel(const Point& from, const Point& to, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
        
        void SetMapPosition(const MapPosition &newPosition, double durationMs = 0.0, AnimationCallback = kEmptyAnimationCallback);
        
		/* Zooming methods */
        void SetZoom(float zoom, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
		void ZoomIn(double durationMs = 350.0) { this->SetZoom(mPosition.zoom + 1, durationMs); }
		void ZoomOut(double durationMs = 350.0) { this->SetZoom(mPosition.zoom - 1, durationMs); }
		void ZoomToBounds(const Bounds &bounds, int padding = 0, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
		void ZoomToBounds(const Bounds &bounds, const Size& pixelSize, int padding = 0, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
        void SetMinZoom(float zoom);
        void SetMaxZoom(float zoom);
        void ZoomTowards(const Point& position, double duration = 0.0, double zoomDelta = 1.f, AnimationCallback callback = kEmptyAnimationCallback);
        
        /* Orientation methods */
        void SetOrientation(double orientation, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);

        /* Animations */
        void StopAnimations()
        {
            mPositionAnimator.Stop();
            mMovementController->StopKinetics();
        }

        /* Tilt methods */
        void SetTilt(double tilt, AnimationCallback callback = kEmptyAnimationCallback);
        void SetMaxTilt(double maxTilt);
        double GetMaxTilt() { return mMaxTilt;}

        /* Project */
        Point Project(const Point &lonlat) const;
        Point Unproject(const Point &point) const;
        Bounds Project(const Bounds &bounds) const;
        Bounds Unproject(const Bounds &bounds) const;
        Point ScreenProject(const Point &position) const;
        Point ScreenUnproject(const Point &pixel) const;
        
        /* Collisions */
        CollisionGroup* GetCollisionGroup(const std::string& name);
        
        /* Markers */
        MarkerGroup* GetMarkerGroup(const std::string& name);
        void AddMarker(Marker *marker);
        void RemoveMarker(Marker *marker);
        void RemoveAllMarkers();
        
        /* Canvas */
        CanvasShapeGroup* GetCanvasShapeGroup(const std::string& name);
        void AddCanvasShape (CanvasShape *shape);
        void RemoveCanvasShape (CanvasShape *shape);
        void RemoveAllCanvasShapes();
        
        /* Regions */
        void AddRegion(const Region &region);
        void RemoveRegion(int index);
        void RemoveAllRegions();
        const Region* GetRegionAtIndex(int index);
        
        /* Listener */
        void SetMapListener(std::shared_ptr<IMapListener> listener);

        /* Getters */
        double GetTilt() const { return mPosition.tilt; }
        Point GetCenter() const { return mProjection->Unproject(mPosition.center); }
        const Point& GetMercatorCenter() const {return mPosition.center; }
        Bounds GetBounds() const { return mProjection->Unproject(mBounds); }
        const Bounds& GetMercatorBounds() const {return mBounds;}
        const Bounds& GetMaxExtent() const { return mMaxExtent; }
        const float GetZoom() const { return mPosition.zoom; }
        const float GetMinZoom() const { return mMinZoom; }
        const float GetMaxZoom() const { return mMaxZoom; }
        const double GetScale() const { return mScale; }
        const double GetResolution() const { return mResolution; }
        const Size& GetSize() const { return mSize; }
        const double GetOrientation() { return 360 - mPosition.orientation; }
        const UserLocation& GetUserLocation() { return mLocationController->GetUserLocation(); }
        TrackingMode GetTrackingMode() { return mTrackingMode; }
        const TMarkerList& GetVisibleMarkers() const;
        IGraphicsDevice* GetGraphicsDevice() { return mApp ? mApp->GetGraphicsDevice() : NULL;}
        bool IsAnimating() const { return mPositionAnimator.IsAnimating(); }
        float GetZoomContainingBounds(const Bounds& bounds) const;
        
        const double GetResolutionAtZoom(float zoom) const
        {
            return MapUtil::GetResolutionForZoom(zoom);
        }
        
        const double GetScaleAtZoom(float zoom) const
        {
            double res = this->GetResolutionAtZoom(zoom);
            return MapUtil::GetScaleFromResolution(res);
        }

        void ResetToPosition(const MapPosition& position);
        
        /* Camera */
        MapCamera* GetCameraSnapshot();
        Projection* GetProjectionCopy();
        void SetPadding(const Bounds &padding);
        
        /* ITouchListener methods */
        void OnTouchEvent(const TouchEvent &e);
        
        /* IMapMovementListener methods */
        void OnMapEvent(const MapEvent &e);
        void OnMapTouchEvent(const MapTouchEvent &e);
        
        void MovedBy(const Vector3 &delta);
        void RotatedBy(const float angularDelta);
        void ScaledBy(const float scale, const Point &midPoint);
        void TiltedBy(const float tilt);
        
        /* IPanZoomAnimationListener methods */
        void OnAnimationStart();
        void OnAnimationMove(const Vector3 &position, double tilt, double rotation);
        void OnAnimationEnd();
        
	private:
        std::shared_ptr<IApplication> mApp;
        
        MapPosition mPosition;
		Bounds mBounds;
		Bounds mMaxExtent;
		Bounds mMaxBounds;
		int mNumZooms;
		double mMinZoom;
		double mMaxZoom;
		int mScale;
		//std::vector<int> mScales;
		double mResolution;
		//std::vector<double> mResolutions;
		Size mSize;
        MapState mPreviousMapState;
        NetworkStatus mNetworkStatus;
        Bounds mPadding;
        double mMaxTilt;
    
        ProjectionType mProjectionType;
        MapCamera *mMapCamera;
        OrthoCamera *mOrthoCamera;
        MarkerManager *mMarkerManager;
        std::shared_ptr<CanvasLayer> mDefaultCanvasLayer;
        std::shared_ptr<Layer> mBaseLayer;
        
        std::map<std::string, CollisionGroup*> mCollisionGroups;
        std::vector<MapOptionValue> mMapOptions;
        
        MapPositionAnimator mPositionAnimator;
        std::unique_ptr<Projection> mProjection;
        
        std::shared_ptr<MapMovementController> mMovementController;
        std::shared_ptr<MapUserLocationController> mLocationController;
        std::shared_ptr<IMapListener> mMapListener;

		TLayerList mLayers;
        
        TrackingMode mTrackingMode;
        
		void MoveTo(Point point, float zoom, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
		void MoveBy(Point delta);
        
		void ZoomTo(float zoom, double durationMs = 0.0, AnimationCallback callback = kEmptyAnimationCallback);
		void ZoomToResolution(double res);
        float ClampToZoomBounds(float zoom) const;

        void UpdateTracking();
        void UpdateRotation(MapState &state, int deltaMS);
        void UpdateMapState(MapState &state, int deltaMillis);
        void CalculateMaxTilt();
        
        void InitializeMapOptions();
	};
};
