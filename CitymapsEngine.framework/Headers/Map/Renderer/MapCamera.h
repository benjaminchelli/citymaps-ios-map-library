//
//  MapCamera.h
//  vectormap2
//
//  Created by Lion User on 10/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/Camera/PerspectiveCamera.h>

namespace citymaps
{
    static const real kMinZ = 50.0;
    
    class MapCamera : public PerspectiveCamera
    {
    public:
        MapCamera(float aspect, float fovy, float nearZ, float farZ, Size screenSize, Bounds maxBounds, real minRes, real maxRes, real worldRes);
        ~MapCamera() {};
        
        void MoveBy(Vector3 delta);
        void MoveTo(Vector3 position);
        
        real GetTilt() const { return mTilt; }
        void SetTilt(real tilt)
        {
            mTilt = tilt;
        }

        void Resize(const Size& size);

        void ZoomToResolution(double resolution);
        
        void Update();
        
        Ray CalculateZoomRay(Point screenPoint);
        Bounds GetMapBounds() { return mBounds; }
        real GetOrientation() { return this->GetRotation().z; }
        double GetResolution() { return this->GetPosition().z / mBaseResolution; };
        double GetResoluionAtZ(double z) { return z / mBaseResolution; };
        double GetZPositionAtResolution(double res) { return res * mBaseResolution; }
        
        void SetMinResolution(double minRes);
        void SetMaxResolution(double maxRes);
        
        void SetPadding(const Bounds &padding);
        
        Point MapUnproject(Point screenPos);
        
        const Vector3& GetCameraPosition() { return mCameraPosition; }
        
    private:
        Matrix4 mTopDownViewMatrix;
        Matrix4 mInvTopDownViewMatrix;
        Bounds mBounds;
        Bounds mMaxBounds;
        Plane mMapPlane;
        Frustum mClipFrustum;
        real mBaseResolution;
        real mWorldResolution;
        real mMinZ;
        real mMaxZ;
        real mMinRes;
        real mMaxRes;
        real mTilt;
        Vector3 mCameraPosition;
        Bounds mPadding;
        
        void FindCameraZ();
    };
};
