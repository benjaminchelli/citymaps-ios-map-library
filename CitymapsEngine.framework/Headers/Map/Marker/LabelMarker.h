//
//  ImageMarker.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 1/21/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Map/Marker/Marker.h>
#include <CitymapsEngine/Core/Font/Types.h>
namespace citymaps
{
    class Font;
    struct LABEL_MARKER_DESC : public MARKER_DESC
    {
        std::u16string Text;
        Size MaxSize;
        Font* TextFont = NULL;
        Vector4f TextColor = Vector4f(0, 0, 0, 1);
        Vector4f TextOutlineColor = Vector4f(1, 1, 1, 1);
        unsigned char OutlineSize = 0;
        TextAlignHorizontal HorizontalAlignment = TextAlignCenter;
        TextAlignVertical VerticalAlignment = TextAlignMiddle;
    };
    
    class Label;
    class LabelMarker : public Marker
    {
    public:
        
        LabelMarker(const LABEL_MARKER_DESC& desc = LABEL_MARKER_DESC());
        ~LabelMarker();
        
        void SetSize(const Size& size);
        void SetText(const std::u16string& text);
        void SetFont(Font* font);
        void SetTextColor(const Vector4f& color);
        void SetOutlineColor(const Vector4f& color);
        void SetOutlineSize(unsigned char size);
        void SetHorizontalAlignment(TextAlignHorizontal h);
        void SetVerticalAlignment(TextAlignVertical h);
        
        const std::u16string& GetText() const { return mText;}
        const Size& GetSize() const { return mSize;}
        Font* GetFont() const { return mFont;}
        const Vector4f& GetTextColor() const { return mTextColor;}
        const Vector4f& GetOutlineColor() const { return mOutlineColor;}
        unsigned char GetOutlineSize() const { return mOutlineSize;}
        TextAlignHorizontal GetHorizontalAlignment() const { return mHorizontalAlignment;}
        TextAlignVertical GetVerticalAlignment() const { return mVerticalAlignment;}
        
        void Update(const MapState& state);
        
    protected:
        
        void OnRender(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        Size CalculateSize();
        
    private:
        
        bool mLabelChanged;
        Size mSize;
        std::u16string mText;
        Font* mFont;
        Vector4f mTextColor;
        Vector4f mOutlineColor;
        char mOutlineSize;
        TextAlignHorizontal mHorizontalAlignment;
        TextAlignVertical mVerticalAlignment;
        
        Label* mLabel;
    };
}