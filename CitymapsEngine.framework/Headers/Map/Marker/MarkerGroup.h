//
//  MarkerGroup.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <set>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Core/MapState.h>
#include <CitymapsEngine/Core/Physics/Collision/CollisionGroup.h>

namespace citymaps
{
    class Marker;
    class MarkerManager;
    class MarkerGroup
    {
    public:
        MarkerGroup(MarkerManager *markerManager, const std::string &name);
        ~MarkerGroup();
        
        void AddMarker(Marker *marker);
        void RemoveMarker(Marker *marker);
        void RemoveAllMarkers();
        
        void Update(const MapState& state);
        
        bool Intersects(const Bounds& bounds);
        bool Intersects(Marker* marker);
        
        void SetCollisionsEnabled(bool enabled)
        {
            mCollisionsEnabled = enabled;
        }
        
        bool IsCollisionEnabled() { return mCollisionsEnabled;}
        
    private:
        MarkerManager *mMarkerManager;
        std::string mName;
        CollisionGroup mCollisionGroup;
        bool mCollisionsEnabled;
        std::mutex mMutex;
        std::set<Marker*> mMarkers;
    };
};
