#ifdef __cplusplus

#ifndef __CITYMAPSENGINE_H__
#define __CITYMAPSENGINE_H__

#include <CitymapsEngine/Core/Globals.h>
#include <CitymapsEngine/Core/Logger.h>
#include <CitymapsEngine/Core/EngineTypes.h>
#include <CitymapsEngine/Core/PlatformIncludes.h>
#include <CitymapsEngine/Core/Util/NetworkStatus.h>

#include <CitymapsEngine/Map/Core/MapTypes.h>

#endif

#endif
