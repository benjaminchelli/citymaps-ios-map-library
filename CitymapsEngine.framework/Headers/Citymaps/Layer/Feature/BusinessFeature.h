//
//  BusinessFeature.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Util/Util.h>
#include <CitymapsEngine/Map/Map.h>
#include <CitymapsEngine/Citymaps/Layer/Util/BusinessData.h>

#include <rapidjson/document.h>

namespace citymaps
{
    class BusinessFeature
    {
        friend class BusinessLayer;
        friend class BusinessTile;
        friend class BusinessMarker;
        
    public:
        
        BusinessFeature();
        BusinessFeature(const BusinessData &data, const Point& mercPoint);
        BusinessFeature(const BusinessData &data, short visibility = 50);
        ~BusinessFeature();
        
        void SetBid(const std::string &bid)
        {
            mData.businessId = bid;
            mBidHash = Util::Hash<std::string>(bid);
        }
        
        void SetMercPoint(const Point& p)
        {
            mMercPoint = p;
        }
        
        BusinessData& GetData() { return mData;}
        const BusinessData& GetData() const { return mData;}
        
        const std::string& GetBid() const { return mData.businessId; }
        const std::string& GetPhone() const { return mData.phone;}
        
        const uint64_t GetBidHash() const {  return mBidHash; }
        
        const Point& GetPoint() { return mMercPoint; }
        const Point& GetScreenPosition() { return mScreenPoint; }
        short GetVisibilityRating() const { return mVisibilityRating; }
        
        short GetCustomVisibilityRating() const { return mCustomRating;}
        
        void SetCustomVisibilityRating(short rating) { mCustomRating = rating;}
        
        const int32_t GetCount() { return mCount; }
        
        const Point& UpdateScreenPosition(Map *map)
        {
            mScreenPoint = map->ScreenProject(mMercPoint);
            
            return mScreenPoint;
        }
        
        // Does not write into the document. It is used to get the allocator.
        bool ToJSON(rapidjson::Document& document, rapidjson::Value& outValue) const;
        
        
        void FromJSON(rapidjson::Value &object, Map *map);
        
        const BusinessFeature* GetSubFeature(int index)
        {
            return &mSubFeatures[index];
        }
        
    private:
        Point mMercPoint;
        Point mScreenPoint;
        short mVisibilityRating;
        short mCustomRating;
        
        bool mHot;
        uint64_t mBidHash;
        int32_t mCount;
        std::vector<BusinessFeature> mSubFeatures;
        
        BusinessData mData;
    };
};

namespace std
{
    template<>
    struct less<citymaps::BusinessFeature>
    {
        bool operator()(const citymaps::BusinessFeature *lhs, const citymaps::BusinessFeature *rhs ) const
        {
            return lhs->GetBidHash() < rhs->GetBidHash();
        }
    };
};
