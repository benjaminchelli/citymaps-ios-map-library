//
//  MapFeature.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Font/FontManager.h>
#include <CitymapsEngine/Citymaps/Layer/Style/StyleLayer.h>

namespace citymaps
{
    typedef std::vector<Point> TPointsList;
    typedef std::vector<unsigned short> TIndexList;
    
    class MapConfiguration;
    
    class MapFeature
    {
        friend class VectorTile;
        
    public:
        MapFeature();
        //MapFeature(MapFeatureType type);
        ~MapFeature();
        
        void Initialize()
        {
            mType = MapFeatureLine;
            mLayerId = 0;
            mLayerIndex = -1;
            mFont = FontManager::kSystemFont;
            mFontSize = FontManager::kDefaultFontSize;
            mBadgeType = MapFeatureBadgeNone;
            mNumPoints = 0;
            mLabelPriority = 0;
            mLabelHash = 0;
            mHeuristicSize = Size(0,0);
            mOrigin = Point(0,0);
            mPoints.clear();
            mAbsolutePoints.clear();
            mIndices.clear();
            mSegmentAngles.clear();
            mLabelPositions.clear();
            mStyleLayer = NULL;
        }
        
        void SetType(MapFeatureType type)
        {
            mType = type;
        }
        
        // Must be set before adding points or centroid.
        void SetOrigin(const Point& p)
        {
            mOrigin = p;
        }
        
        void SetCentroid(const Point &centroid)
        {
            mCentroid = centroid;
            mAbsoluteCentroid = centroid + mOrigin;
        }
        
        void AddPoint(const Point& point)
        {
            mPoints.push_back(point);
            mAbsolutePoints.push_back(point + mOrigin);
        }
        
        void AddIndex(int index)
        {
            mIndices.push_back(index);
        }
        
        void LoadConfigData(const MapConfiguration &config);
        
        void CalculateSegmentAngles();
        void CalculateLabelPositions();
        
        //real GetSegmentAngle(int index) { return mSegmentAngles[index]; }
        //const Point& GetLabelPosition(int index) { return mLabelPositions[index]; }
        
        /* Getters */
        
        uint32_t GetFeatureId() { return mFeatureId; }
        const Point& GetCentroid() { return mCentroid; }
        const Point& GetAbsoluteCentroid() { return mAbsoluteCentroid; }
        const Point& GetOrigin() const {return mOrigin;}
        const MapFeatureType GetType() { return mType; }
        const TPointsList& GetPoints() const { return mPoints; }
        const TPointsList& GetAbsolutePoints() const {return mAbsolutePoints; }
        MapFeatureBadgeType GetBadgeType() const {return mBadgeType; }
        short GetLayerId() { return mLayerId; }
        const std::u16string& GetLabel() const
        {
            return (mBadgeType != MapFeatureBadgeNone ||
                    mLabel.length() == 0) ? mRef : mLabel;
        }
        const std::u16string& GetCapsLabel() const
        {
            return (mBadgeType != MapFeatureBadgeNone ||
                    mLabel.length() == 0) ? mAllCapsRef : mAllCapsLabel;
        }
        const std::u16string& GetLabelForZoom(int zoomLevel);
        int GetLabelPriority() const { return mLabelPriority; }
        uint64_t GetLabelHash() const { return mLabelHash; }
        const std::string& GetFont() const { return mFont; }
        int GetFontSize() const { return mFontSize; }
        const Vector4& GetColor() const { return mColor; }
        int GetMinZoom() const { return mMinZoom; }
        int GetMaxZoom() const { return mMaxZoom; }
        int GetOutlineSize() const { return mOutlineSize; }
        const StyleLayer* GetStyleLayer() const { return mStyleLayer; }
        
        const Size& GetHeuristicSize() const { return mHeuristicSize;}
        void SetHeuristicSize(const Size& size) {mHeuristicSize = size;}
        
        bool HasCacheForStyle(const StyleData *data)
        {
            return mCachedStyle == data;
        }
        
        const Size& GetCachedSize()
        {
            return mCachedSize;
        }
        
        void SetCachedSize(const StyleData *data, const Size& size)
        {
            mCachedStyle = data;
            mCachedSize = size;
        }
        
    private:
        uint32_t mFeatureId;
        MapFeatureType mType;
        Point mOrigin;
        TPointsList mPoints;
        TPointsList mAbsolutePoints;
        TIndexList mIndices;
        std::vector<real> mSegmentAngles;
        TPointsList mLabelPositions;
        
        Point mCentroid;
        Point mAbsoluteCentroid;
        Size mHeuristicSize;
        unsigned short mNumPoints;
        unsigned short mNumIndices;
        short mLayerId;
        short mLayerIndex;
        std::u16string mLabel;
        std::u16string mAllCapsLabel;
        std::u16string mRef;
        std::u16string mAllCapsRef;
        uint64_t mLabelHash;
        int mLabelPriority;
        std::string mFont;
        int mFontSize;
        int mMinZoom;
        int mMaxZoom;
        int mOutlineSize;
        int mPage;
        Vector4 mColor;
        MapFeatureBadgeType mBadgeType;
        const StyleLayer *mStyleLayer;
        
        // Cache
        const StyleData *mCachedStyle = NULL;
        Size mCachedSize;
    };
};
