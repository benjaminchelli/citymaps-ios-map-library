#pragma once

#include <rapidxml/rapidxml.hpp>

namespace citymaps
{
    struct LabelStyle
    {
        LabelStyle()
        :Zoom(0), MaxLabels(0), LineFeaturePadding(0), BadgeFeaturePadding(0), PointFeaturePadding(0), PolygonFeaturePadding(0)
        {}
        
        LabelStyle(rapidxml::xml_node<> *xmlNode);
        
        unsigned short Zoom;
        unsigned short MaxLabels;
        unsigned int LineFeaturePadding;
        unsigned int BadgeFeaturePadding;
        unsigned int PointFeaturePadding;
        unsigned int PolygonFeaturePadding;
    };
}