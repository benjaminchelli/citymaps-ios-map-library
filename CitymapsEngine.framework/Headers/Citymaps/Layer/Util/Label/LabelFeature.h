//
//  LabelFeature.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/29/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/Shape/Label.h>
#include <CitymapsEngine/Map/Map.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/MapFeature.h>
#include <CitymapsEngine/Citymaps/Layer/Tile/VectorTile.h>

#include <CitymapsEngine/Core/Physics/Physics.h>

namespace citymaps
{
    class StyleLayer;
    class MapFeature;
    class Sprite;
    class LabelFeature : public Validatable
    {
        friend class LabelProcessor;
        
    public:
        LabelFeature();
        ~LabelFeature();
        
        void SetFeature(TileLabelFeature* feature, std::unique_ptr<IGeometricShape>& shape, Map* map);
        
        void SetVisible(bool visible)
        {
            mIsVisible = visible;
        }
        
        float GetAlpha() { return mAlpha; }
        
        uint64_t GetHash() const { return mLabelHash; }
        
        int GetPriority() const { return mLabelPriority;}
        
        const std::u16string& GetText() { return mText; }
        
        MapFeatureBadgeType GetBadgeType() { return mFeatureBadgeType; }
        
        IGeometricShape *GetCollisionShape()
        {
            return mCollisionShape.get();
        }
        
        bool IsWithinBounds(const Bounds &bounds)
        {
            return mCollisionShape->Intersects(bounds);
        }
        
        bool IsOnScreen()
        {
            return mOnScreen;
        }
        
        bool IsTextHiddenAtZoom(int zoom, MapFeatureBadgeType badgeType = MapFeatureBadgeNone)
        {
            return mStyleLayer->IsTextHiddenAtZoom(zoom, badgeType);
        }
        
        bool IsWithinZoom(int zoom)
        {
            return mMinZoom <= zoom && mMaxZoom >= zoom;
        }
        
        bool Intersects(LabelFeature* other)
        {
            return other->mCollisionShape->Intersects(mCollisionShape.get());
        }
        
        bool ContainedBy(const Bounds& other)
        {
            BoundingBox box(other);
            box.min.z = -1000;
            box.max.z = 1000;
            return box.Contains(mCollisionShape->GetBoundingBox());
        }
        
        const Size& GetSize() const { return mSize;}
        
        const Point& GetScreenPosition() const { return mProjPoint;}
        
        bool Intersects(const Bounds& other)
        {
            return mCollisionShape->Intersects(other);
        }
        
        bool IsVisible() {return mIsVisible;}
        void Update(Map *map, const MapState &state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
    
        static Size CalculateSize(MapFeature* feature, int zoom);
        static std::unique_ptr<IGeometricShape> DetermineCollisionShape(Map* map, TileLabelFeature* feature, MapFeatureBadgeType badgeType, int zoomLevel, const Point& pos, bool cullIfInvalid = false);
        
    private:
        
        const StyleLayer* mStyleLayer;
        const StyleData* mCurrentStyleData;
        Label *mLabel;
        MeshShape* mDebugShape;
        std::unique_ptr<IGeometricShape> mCollisionShape;
        Sprite* mBadgeSprite;
        Font *mCurrentFont;
        Vector4f mTextColor;
        
        Point mPoint;
        Point mProjPoint;
        Point mSegment[2];
        Size mSize;
        MapFeatureType mFeatureType;
        MapFeatureBadgeType mFeatureBadgeType;
        uint64_t mLabelHash;
        std::u16string mText;
        std::u16string mAllCapsText;
        int mLabelPriority;
        int mMinZoom;
        int mMaxZoom;
        
        float mAlpha;
        bool mIsVisible;
        double mRotation;
        int mZoomLevel;
        bool mReloadStyle;
        bool mOnScreen;
        
        static std::string msBadgeImageFiles[MapFeatureBadgeNumBadgeTypes];
        static std::map<MapFeatureBadgeType, Sprite*> msBadgeSprites;
        static Vector4f msBadgeTextColors[MapFeatureBadgeNumBadgeTypes];
        static Font *msDefaultBadgeFont;
        static Font *msSmallBadgeFont;
        
        void UpdateAlpha(const MapState& state);
        void UpdateRotation(Map* map);

    };
};
