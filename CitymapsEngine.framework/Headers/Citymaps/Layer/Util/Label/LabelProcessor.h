//
//  LabelProcessor.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/29/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Core/Physics/Physics.h>

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Core/Graphics/GraphicsDevice.h>
#include <CitymapsEngine/Core/Graphics/RenderState.h>
#include <CitymapsEngine/Core/Graphics/Shape/Label.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>
#include <CitymapsEngine/Map/Map.h>
#include <CitymapsEngine/Citymaps/Layer/Util/Label/LabelFeature.h>
#include <CitymapsEngine/Citymaps/Layer/Util/Label/ArrowFeature.h>

namespace citymaps
{
    class Tile;
    class VectorLayer;
    class MapFeature;
    
    typedef std::vector<std::shared_ptr<LabelFeature>> TLabelFeatureList;
    
    class LabelProcessor
    {
    public:
        LabelProcessor(const std::string& collisionGroup, VectorLayer *layer);
        ~LabelProcessor();
        
        void SetMap(Map *map);
        
        void AddConstraint(const std::string& collisionGroup);
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        bool NeedsReset(const MapState& state);
        
        bool IsValid() const;
        size_t GetNumLabels() const {return mActiveLabels.size();}
        void ForceReset()
        {
            mForceReset = true;
        }
        
        void ResetLabels();
        
    private:
        
        enum LabelProcessorState
        {
            LabelProcessorStateWaiting,
            LabelProcessorStateProcessing,
            LabelProcessorStateFinished
        };
        
        LabelProcessorState mState;
        
        Map *mMap;
        
        std::string mLabelCollisionGroupName;
        CollisionGroup *mLabelCollisionGroup;
        std::vector<CollisionGroup*> mConstraints;
        std::vector<size_t> mConstraintSizes;
        
        ObjectPool<LabelFeature> mLabelPool;
        std::vector<std::shared_ptr<LabelFeature>> mDisappearingLabels;
        VectorLayer *mLayer;
        std::vector<std::shared_ptr<LabelFeature> > mActiveLabels;
        
        ObjectPool<ArrowFeature> mArrowPool;
        std::vector<std::shared_ptr<ArrowFeature>> mVisibleArrows;
        std::vector<std::shared_ptr<ArrowFeature>> mDisappearingArrows;
        CollisionGroup mArrowCollisionGroup;
        
        std::vector<std::shared_ptr<VectorTile> > mTiles;
        bool mForceReset;
        int mLastZoomLevel;
        unsigned int mTileIndex;
        unsigned int mFeatureIndex;
        
        void UpdateActiveLabels(const MapState& state);
        void UpdateActiveArrows(const MapState& state);
        
        void GetTileList(const MapState& state);
        void ProcessFeatures(const MapState& state);
        void ResetState();
        
        bool IntersectsConstraints(IGeometricShape* shape);
        std::shared_ptr<LabelFeature> AddLabelFeature(TileLabelFeature* feature, std::unique_ptr<IGeometricShape>& shape);
        void HideLabelFeature(std::shared_ptr<LabelFeature> feature);
        void CullActiveLabels(const MapState& state);
        
        void ProcessFeaturesSelective(const MapState& state);
        void ProcessFeaturesRobust(const MapState& state);
    };
};