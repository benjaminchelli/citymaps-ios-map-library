//
//  RegionTile.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Tile.h>
#include <CitymapsEngine/Citymaps/Layer/Feature/RegionFeature.h>

namespace citymaps
{
    class RegionLayer;
    
    class RegionTile : public Tile
    {
    public:
        RegionTile(GridPoint gridPoint, TileLayer *layer);
        ~RegionTile();
        
        RegionFeature* GetFeature(int index)
        {
            if(index < mFeatures.size()) {
                return &mFeatures[index];
            }
            
            return NULL;
        }
        
        int GetNumFeatures()
        {
            return mNumFeatures;
        }
        
    private:
        int mNumFeatures;
        std::vector<RegionFeature> mFeatures;
        
        void SetData(const TByteArray &data);
    };
};
