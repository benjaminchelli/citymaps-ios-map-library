//
//  BusinessLayer.h
//  vectormap2
//
//  Created by Lion User on 09/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/TileLayer.h>
#include <CitymapsEngine/Citymaps/Layer/Util/BusinessFilter.h>
#include <CitymapsEngine/Citymaps/Layer/Util/LogoRepository.h>
#include <CitymapsEngine/Citymaps/Marker/BusinessMarker.h>
#include <CitymapsEngine/Core/Util/ObjectPool.h>
#include <CitymapsEngine/Core/Util/MemoryCache.h>

namespace citymaps
{
    typedef std::map<uint64_t, std::shared_ptr<BusinessMarker>> TBusinessMarkerMap;
    
    static const std::string kCEDefaultImageHostname = "r.citymaps.com";
    static const std::string kCEDefaultLogoURL = "/riak/business_logos/{id}_150x150";
    static const std::string kCEDefaultCategoryURL = "/riak/category_icons5_app/{id}";
    
    struct CITYMAPS_BUSINESS_LAYER_DESC : TILE_LAYER_DESC
    {
        CITYMAPS_BUSINESS_LAYER_DESC() :
        TILE_LAYER_DESC(), BusinessImageHostname(kCEDefaultImageHostname), BusinessLogoURL(kCEDefaultLogoURL), BusinessCategoryURL(kCEDefaultCategoryURL)
        {
            
        }
        std::string BusinessImageHostname;
        std::string BusinessLogoURL; 
        std::string BusinessCategoryURL;
    };
    
    class IBusinessListener
    {
    public:
        
        virtual ~IBusinessListener() {}
        
        virtual void OnBusinessTapped(const BusinessData& data) = 0;
        virtual void OnBusinessDoubleTapped(const BusinessData& data) = 0;
        virtual void OnBusinessLongPressed(const BusinessData& data) = 0;
        
        virtual void OnBusinessEnterZone(const BusinessData& data, int index) = 0;
        virtual void OnBusinessExitZone(const BusinessData& data, int index) = 0;
        
        virtual void OnBusinessAdded(const BusinessData& data) = 0;
        virtual void OnBusinessRemoved(const BusinessData& data) = 0;
    };
    
    struct BusinessZone
    {
        BusinessZone()
        {
        }
        
        BusinessZone(const Bounds &bounds) :
            zoneBounds(bounds)
        {
        }
        
        Bounds zoneBounds;
        std::vector<uint64_t> activeBusinesses;
    };
    
    class BusinessFeature;
    typedef std::function<bool(const BusinessFeature *, const BusinessFeature *)> TBusinessFeatureSortComparator;
    
    enum BusinessQuerySortMethod
    {
        BusinessQuerySortDistance,
        BusinessQuerySortVisibility
    };
    
    struct BusinessQuery
    {
        Bounds bounds;
        uint32_t limit;
        uint32_t offset;
        BusinessQuerySortMethod sortMethod;
    };
    
    class BusinessLayer : public TileLayer, public IMarkerListener
    {
    public:
        BusinessLayer(CITYMAPS_BUSINESS_LAYER_DESC &desc);
        ~BusinessLayer();
        
        void SetMap(Map *map);
        
        virtual void OnDisable();
        
        virtual void ResetTiles();
        
        bool RequiresUpdate(const MapState& state);
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        void SetMiniPinsEnabled(bool enabled) {mMiniPinsEnabled = enabled;}
        bool AreMiniPinsEnabled() const { return mMiniPinsEnabled;}
        
        void ApplyFilter(const BusinessFilter &filter);
        void RemoveFilter();
        void AddBusinessToActiveFilter(const BusinessFilterObject& obj);
        void RemoveBusinessFromActiveFilter(const std::string &bid);
        
        void AddToBlacklist(const std::string &bid);
        void RemoveFromBlacklist(const std::string &bid);
        
        void SetBusinessState(const std::string &bid, BusinessMarkerState state);
        BusinessMarkerState GetBusinessState(const std::string &bid);
        void ResetBusinessStates();
        
        void AddBusinessAttachment(const std::string &bid, Marker* marker);
        void RemoveBusinessAttachment(const std::string& bid, Marker* marker);
        void RemoveBusinessAttachments(const std::string& bid);
        void RemoveAllBusinessAttachments();
        
        BusinessMarker* GetBusinessMarkerAtPoint(const Point& p);
        
        void SetBusinessListener(std::shared_ptr<IBusinessListener> listener) { mBusinessListener = listener; }
        
        std::shared_ptr<BusinessMarker> GetBusinessMarker(const std::string &bid);
        
        void AddBusinessConstraint(MarkerGroup* group);
        void RemoveBusinessConstraint(MarkerGroup* group);
        void RemoveAllBusinessConstraints();
        
        void AddBusinessZone(const Bounds &zone);
        void RemoveBusinessZone(int index);
        void RemoveAllBusinessZones();
        
        void SetBusinessVisibilityScore(const std::string& bid, short score);
        short GetBusinessVisiblityScore(const std::string& bid);
        void ClearBusinessVisibilityScores();
        
       bool OnMarkerEvent(MarkerEvent event, Marker* marker);
        
        /** Citymaps API **/
        bool InsertBusinessIntoTile(const BusinessData& data, short visibility);
        
        void GetAllBusinessesInMapBounds(std::vector<BusinessData>& data);
        void GetBusinesses(const BusinessQuery &query, std::vector<BusinessData> &result);
        
    private:
        typedef std::vector<Marker*> BusinessAttachments;
        
        ObjectPool<BusinessMarker> *mMarkerPool;
        MarkerGroup *mMarkerGroup;
        CollisionGroup *mMarkerCollisionGroup;
        CollisionGroup *mVectorLabelCollisionGroup;
        
        std::set<MarkerGroup*> mMarkerConstraints;
        
        TBusinessMarkerMap mActiveMarkers;
        TBusinessMarkerMap mDisappearingMarkers;
        LogoRepository *mLogoRepository;
        std::unique_ptr<BusinessFilter> mActiveFilter;
        std::map<uint64_t, BusinessMarkerState> mBusinessStates;
        std::map<uint64_t, short> mBusinessVisibilityScores;
        
        std::mutex mBusinessAttachmentMutex;
        std::map<uint64_t, BusinessAttachments> mBusinessAttachments;
        
        std::shared_ptr<IBusinessListener> mBusinessListener;
        std::map<int, int> mUsedLogos;
        std::set<uint64_t> mBlacklist;
        
        std::string mImageHostname;
        std::string mLogoURL;
        std::string mCategoryURL;
        
        std::vector<BusinessZone> mBusinessZones;
        
        std::vector<BusinessFeature *> mBusinessFeatureList;
        std::map<BusinessQuerySortMethod, std::vector<BusinessFeature *>> mSortedFeatureLists;
        
        int mNumMini;
        bool mMiniPinsEnabled;
        
        void UpdateMarkersFromFilter(const MapState& state);
        void UpdateMarkersFromTiles(const MapState& state);
        
        void UpdateLogos(const MapState& state);
        void ShowAllFeatures(const MapState& state);
        void UpdateBusinessInfoVisibility(const MapState& state);
        void UpdateDisappearingMarkers();
        
        BusinessMarkerState GetBusinessState(uint64_t key);
        
        std::shared_ptr<BusinessMarker> ShowBusiness(BusinessFeature *feature, bool addToConstraints = true, bool animated = true, bool mini = false);
        void HideBusiness(const BusinessFeature *feature, bool force = false);
        void HideBusiness(uint64_t key, bool force = false);
        void ResetBusinesses(bool force = false);
        
        uint64_t RemoveClusteredBid(uint64_t bid);
        bool IsClusterChildBid(uint64_t testBid);
        void AddClusteredBid(uint64_t clusterBid, uint64_t bidAddedToCluster);
        
        void ApplyAttachments(uint64_t key, BusinessMarker* marker);
        
        void EvaluateBusinessZones(BusinessMarker *marker, uint64_t key, const Point &position);
        void RemoveFromBusinessZones(BusinessMarker *marker, uint64_t key);
        
        short DetermineVisibilityScoreForFeature(BusinessFeature* feature);
        
        bool IntersectsConstraints(const Bounds& b);

        void CreateMarkerPool();
    };
};
