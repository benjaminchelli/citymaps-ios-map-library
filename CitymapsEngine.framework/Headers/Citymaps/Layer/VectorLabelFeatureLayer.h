//
//  VectorLabelFeatureLayer.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 8/11/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Map/Layer/Layer.h>
#include <CitymapsEngine/Citymaps/Layer/Util/Label/LabelProcessor.h>

namespace citymaps
{
    class VectorLayer;
    class Map;
    
    class VectorLabelFeatureLayer : public Layer
    {
        public:
        VectorLabelFeatureLayer(VectorLayer *vectorLayer, LAYER_DESC &layerDesc);
        ~VectorLabelFeatureLayer();
        
        void SetMap(Map *map);
        virtual bool RequiresUpdate(const MapState& state);
        virtual void OnResize(int width, int height)
        {
            this->ResetLabels();
        }
        
        void Update(const MapState& state);
        void Render(IGraphicsDevice *device, RenderState &perspectiveState, RenderState &orthoState);
        
        void ResetLabels()
        {
            DispatchOnMainThread([this]()
             {
                 mLabelProcessor.ResetLabels();
             });
        }
        
        void ForceLabelReset()
        {
            DispatchOnMainThread([this]()
             {
                 mLabelProcessor.ForceReset();
             });
        }
        
    private:
        
        LabelProcessor mLabelProcessor;
    };
};

