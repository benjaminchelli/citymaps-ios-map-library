//
//  CERoutingTypes.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CERoutingSuccessBlock)(NSArray *routes);

typedef void (^CERoutingErrorBlock)(NSError *error);

typedef enum {
    CERoutingUnitSystem_Imperial,
    CERoutingUnitSystem_Metric
} CERoutingUnitSystem;

typedef enum {
    CERoutingMode_Driving = (1 << 1),
    CERoutingMode_Walking = (1 << 2),
    CERoutingMode_PublicTransport = (1 << 3),
    CERoutingMode_Bicycle = (1 << 4),
    CERoutingMode_Shortest = (1 << 5),
    CERoutingMode_Fastest = (1 << 6),
    CERoutingMode_TrafficEnabled = (1 << 7),
    CERoutingMode_TrafficDisabled = (1 << 8)
} CERoutingMode;