//
//  CEBusinessFilter_Protected.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/7/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <CitymapsEngine/Citymaps/API/iOS/CEBusinessFilter.h>
#import <CitymapsEngine/Citymaps/Layer/Util/BusinessFilter.h>

@interface CEBusinessFilter ()

@property (assign, nonatomic) citymaps::BusinessFilter enginePointer;

@end
