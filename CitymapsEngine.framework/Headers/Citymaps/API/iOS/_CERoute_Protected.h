//
//  CERoute.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import "CERoute.h"
#import <CitymapsEngine/Citymaps/Routing/Route.h>

@interface CERoute()

- (id)initWithRoute:(citymaps::Route*)route;

@property (nonatomic, assign) citymaps::Route *route;

@end