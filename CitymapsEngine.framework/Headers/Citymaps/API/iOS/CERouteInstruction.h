//
//  CERouteInstruction.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CitymapsEngine/Citymaps.h>

typedef enum {
    CERouteInstructionDirectionForward,
    CERouteInstructionDirectionBearRight,
    CERouteInstructionDirectionLightRight,
    CERouteInstructionDirectionRight,
    CERouteInstructionDirectionHardRight,
    CERouteInstructionDirectionUTurnRight,
    CERouteInstructionDirectionUTurnLeft,
    CERouteInstructionDirectionHardLeft,
    CERouteInstructionDirectionLeft,
    CERouteInstructionDirectionLightLeft,
    CERouteInstructionDirectionBearLeft,
    CERouteInstructionDirectionEnterTransit,
    CERouteInstructionDirectionExitTransit,
    CERouteInstructionDirectionUnknown
} CERouteInstructionDirection;

typedef enum {
    CERouteInstructionActionTransitEnter,
    CERouteInstructionActionTransitExit,
    CERouteInstructionActionDepart,
    CERouteInstructionActionArrive,
    CERouteInstructionActionArriveLeft,
    CERouteInstructionActionArriveRight,
    CERouteInstructionActionLeftLoop,
    CERouteInstructionActionLeftUTurn,
    CERouteInstructionActionSharpLeftTurn,
    CERouteInstructionActionLeftTurn,
    CERouteInstructionActionSlightLeftTurn,
    CERouteInstructionActionContinue,
    CERouteInstructionActionSlightRightTurn,
    CERouteInstructionActionRightTurn,
    CERouteInstructionActionSharpRightTurn,
    CERouteInstructionActionRightUTurn,
    CERouteInstructionActionRightLoop,
    CERouteInstructionActionLeftExit,
    CERouteInstructionActionRightExit,
    CERouteInstructionActionLeftRamp,
    CERouteInstructionActionRightRamp,
    CERouteInstructionActionLeftFork,
    CERouteInstructionActionMiddleFork,
    CERouteInstructionActionRightFork,
    CERouteInstructionActionLeftMerge,
    CERouteInstructionActionRightMerge,
    CERouteInstructionActionNameChange,
    CERouteInstructionActionTrafficCircle,
    CERouteInstructionActionFerry,
    CERouteInstructionActionUnknown
} CERouteInstructionAction;

@class CERoute;

@interface CERouteInstruction : NSObject

@property (nonatomic, strong) NSString *instructionDescription;
@property (nonatomic, strong) NSString *roadName;

@property (nonatomic, assign) CELonLat location;
@property (nonatomic, assign) CELonLatBounds boundingBox;
@property (nonatomic, assign) CERouteInstructionDirection direction;
@property (nonatomic, assign) CERouteInstructionAction action;
@property (nonatomic, assign) CGFloat distance;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, assign) BOOL isStart;
@property (nonatomic, assign) BOOL isEnd;
@property (nonatomic, assign) BOOL isTransitStart;
@property (nonatomic, assign) BOOL isTransitEnd;
@property (nonatomic, assign) CELonLat *points;
@property (nonatomic, assign) NSUInteger numPoints;
@property (nonatomic, assign) CERoute *route;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) CGFloat compassDegrees;

@end