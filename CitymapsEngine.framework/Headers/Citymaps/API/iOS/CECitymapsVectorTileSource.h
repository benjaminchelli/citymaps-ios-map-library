//
//  CECitymapsVectorTileSource.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/25/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <CitymapsEngine/Map/API/iOS/Layer/CETileSource.h>

@interface CECitymapsVectorTileSource : CETileSource

- (id)initWithAPIKey:(NSString *)apiKey;
- (id)initWithURL:(NSString *)url andCacheVersion:(NSInteger)cacheVersion apiKey:(NSString *)apiKey;// andStyle:(NSString *)style

@end
