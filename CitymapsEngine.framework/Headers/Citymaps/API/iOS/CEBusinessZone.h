//
//  CEBusinessZone.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 12/2/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** Defines a region of the screen which can be used to identifiy when businesses enter and exit this region */

@interface CEBusinessZone : NSObject

/**
 * @name Initializers
 *
 */

/** Initialize a new business zone with a bounds, expressed in screen coordinates 
  * @param bounds Bounds to represent this CEBusinessZone.
 */
- (id)initWithBounds:(CGRect)bounds;

/** The region bounds, expressed in screen coordinates */
@property (assign, nonatomic) CGRect bounds;

@end
