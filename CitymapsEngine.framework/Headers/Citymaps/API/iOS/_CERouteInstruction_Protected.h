//
//  CERoute.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import "CERouteInstruction.h"
#import <CitymapsEngine/Citymaps/Routing/RouteInstruction.h>

@interface CERouteInstruction()

- (id)initWithRouteInstruction:(const citymaps::RouteInstruction*)routeInstruction;

@end