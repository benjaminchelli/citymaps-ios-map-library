#import <Foundation/Foundation.h>

#import <CitymapsEngine/Map/API/iOS/Layer/CEWebDataSource.h>

@interface CECitymapsDataSourceOptions : CEWebDataSourceOptions

- (id)initWithURL:(NSString*)url andCacheVersion:(NSInteger)cacheVersions andApiKey:(NSString *)apiKey;

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *apiKey;
@property (assign) NSInteger cacheVersion;
@property (assign, nonatomic) BOOL signedURLs;
@property (strong, nonatomic) NSString *signedURLPattern;


@end

/** Abstract base class for Citymaps data sources. */
@interface CECitymapsDataSource : CEWebDataSource

- (id)initWithOptions:(CECitymapsDataSourceOptions *)option;
- (id)initWithURL:(NSString *)url andCacheVersion:(NSInteger)cacheVersion andApiKey:(NSString *)apiKey;

+ (CECitymapsDataSource*)defaultCitymapsMapDataSource:(NSString *)apiKey;
+ (CECitymapsDataSource*)defaultBusinessDataSource:(NSString *)apiKey;
+ (CECitymapsDataSource*)defaultRegionDataSource:(NSString *)apiKey;

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *apiKey;

@end
