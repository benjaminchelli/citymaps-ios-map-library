//
//  CitymapsTypes.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/4/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#pragma once
#include <string>

namespace citymaps
{
    static const std::string kCitymapsBusinessMarkerCollisionGroup = "CitymapsBusinessMarker";
    static const std::string kCitymapsVectorLabelCollisionGroup = "CitymapsVectorLabels";
    static const std::string kCitymapsRegionMarkerCollisionGroup = "CitymapsRegionMarker";
    static const std::string kCitymapsRoutingCanvasShapeGroup = "CitymapsRoutingInternalShapeGroup";
}