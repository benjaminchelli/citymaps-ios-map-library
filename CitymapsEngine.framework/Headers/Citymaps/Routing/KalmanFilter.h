//
//  KalmanFilter.h
//  CityMaps
//
//  Created by Adam Eskreis on 7/30/14.
//  Copyright (c) 2014 CityMaps. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

namespace citymaps
{
    template <typename Vector, typename Matrix>
    class KalmanFilter
    {
    public:
        KalmanFilter(const Matrix & pA, const Matrix & pB, const Matrix & pH, const Vector & pX, const Matrix & pP, const Matrix & pQ, const Matrix & pR) :
            A(pA), B(pB), H(pH), x(pX), P(pP), Q(pQ), R(pR)
        {
        }
        
        ~KalmanFilter() {}
        
        void Step(const Vector &control, const Vector &measurement);
        
        const Vector& GetCurrentState() const { return x; }
        
    private:
        Matrix A;
        Matrix B;
        Matrix H;
        Matrix Q;
        Matrix R;
        Matrix P;
        
        Vector x;
    };
};

#import "KalmanFilter.inl"
