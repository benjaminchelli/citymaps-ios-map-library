//
//  RoutingControllerListener.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/8/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/Citymaps/Routing/RouteState.h>

namespace citymaps
{
    class Route;
    class IRoutingControllerListener
    {
    public:
        
        // The listener assumes ownership of this route!
        //virtual void OnReroute(Route *route) = 0;
        //virtual void OnRouteFinished(bool reachedDestination) = 0;
        virtual void OnRouteNotification(const std::string& notification) = 0;
        virtual void OnRouteInstructionBegan(int index) = 0;
        virtual void OnRouteStateUpdate(const RouteState& state) = 0;
    };
}