//
//  KalmanGPSPredictor.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 7/30/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Routing/KalmanFilter.h>
#include <CitymapsEngine/Map/Input/GPSPredictor.h>

namespace citymaps
{
    class VectorLayer;
    class MapFeature;
    
    class KalmanGPSPredictor : public IGPSPredictor
    {
    public:
        KalmanGPSPredictor(VectorLayer *layer);
        ~KalmanGPSPredictor();
       
        void BeginSimulation(const Point &startPosition, const Point &startVelocity);
        void Update(int deltaMillis);
        void OnLocationUpdate(const UserLocation &newPosition);
        
        UserLocation GetCurrentState();
        Point GetCurrentVelocity() { return mVelocity; }
        
    private:
        VectorLayer *mLayer;
        UserLocation mLastReading;
        Point mLastMercLocation;
        Point mVelocity;
        Point mAcceleration;
        double mLastReadingTime;
        double mStartTime;
        Point mStartPoint;
        
        double mMinDistance;
        int mMinSegment;
        MapFeature *mMinFeature;
        
        int mIterations;
        
        std::list<std::pair<Point, double>> mPreviousPoints;
        
        KalmanFilter<Vector2, Matrix2> *mPositionFilter;
    };
};