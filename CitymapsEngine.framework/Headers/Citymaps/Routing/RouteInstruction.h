//
//  RouteInstruction.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>

#include <rapidjson/document.h>

namespace citymaps
{
    class RouteInstruction
    {
    public:
        
        enum Direction
        {
            DirectionForward,
            DirectionBearRight,
            DirectionLightRight,
            DirectionRight,
            DirectionHardRight,
            DirectionUTurnRight,
            DirectionUTurnLeft,
            DirectionHardLeft,
            DirectionLeft,
            DirectionLightLeft,
            DirectionBearLeft,
            DirectionEnterTransit,
            DirectionExitTransit,
            DirectionUnknown
        };
        
        enum Action
        {
            ActionPublicTransitEnter,
            ActionPublicTransitExit,
            ActionDepart,
            ActionArrive,
            ActionArriveLeft,
            ActionArriveRight,
            ActionLeftLoop,
            ActionLeftUTurn,
            ActionSharpLeftTurn,
            ActionLeftTurn,
            ActionSlightLeftTurn,
            ActionContinue,
            ActionSlightRightTurn,
            ActionRightTurn,
            ActionSharpRightTurn,
            ActionRightUTurn,
            ActionRightLoop,
            ActionLeftExit,
            ActionRightExit,
            ActionLeftRamp,
            ActionRightRamp,
            ActionLeftFork,
            ActionMiddleFork,
            ActionRightFork,
            ActionLeftMerge,
            ActionRightMerge,
            ActionNameChange,
            ActionTrafficCircle,
            ActionFerry,
            ActionUnknown
        };
        
        RouteInstruction();
        ~RouteInstruction();
        
        void Set(rapidjson::Value &jsonObject);
        
        const Point& GetPosition() const { return mPosition; }
        
        const Bounds& GetBounds() const { return mBoundingBox; }
        
        const std::string& GetDescription() const { return mDescription; }
        
        Direction GetDirection() const { return mDirection; }
        
        Action GetAction() const { return mAction; }
        
        double GetDistance() const { return mDistance; }
        
        const uint32_t& GetDuration() const { return mDuration; }
        
        const std::string& GetRoadName() const { return mRoadName; }
        
        bool IsStart() const { return mIsStart; }
        
        bool IsEnd() const { return mIsEnd; }
        
        bool IsEnterTransit() const { return mIsTransitEnter; }
        
        bool isExitTransit() const { return mIsTransitExit; }
        
        bool IsPublicTransit() const { return mIsPublicTransit; }
        
        const std::vector<Point>& GetPoints() const { return mPoints; }
            
    private:
        
        Point mPosition;
        Bounds mBoundingBox;
        std::string mDescription;
        
        Direction mDirection;
        Action mAction;
        
        double mDistance;
        uint32_t mDuration;
        std::string mRoadName;
        bool mIsStart;
        bool mIsEnd;
        bool mIsTransitEnter;
        bool mIsTransitExit;
        bool mIsPublicTransit;
        
        std::vector<Point> mPoints;
    };
};
