//
//  DirectionsRequest.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/4/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#pragma once

#include <CitymapsEngine/CitymapsEngine.h>
#include <CitymapsEngine/Citymaps/Routing/RoutingTypes.h>

namespace citymaps
{
    
    class RoutingRequest
    {
    public:
        RoutingRequest() :
            mNumAlternatives(0),
            mUnits(UnitSystemImperial),
            mMode(kDefaultRoutingMode)
        {
        }
        
        ~RoutingRequest()
        {
        }
        
        void Execute(TRouteSuccess success, TRouteError failure);

        Point mStart;
        Point mEnd;
        uint32_t mNumAlternatives;
        RoutingUnitSystem mUnits;
        uint32_t mMode;
    };
};