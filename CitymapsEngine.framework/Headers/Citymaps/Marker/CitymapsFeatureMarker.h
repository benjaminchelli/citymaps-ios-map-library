#pragma once

#include <CitymapsEngine/Map/Marker/Marker.h>

namespace citymaps
{
    enum MarkerLoadingState
    {
        MarkerStateIdle,
        MarkerStateLoading,
        MarkerStateLoaded
    };
    
    class CitymapsFeatureMarker : public Marker
    {
    public:
        
        CitymapsFeatureMarker(const MARKER_DESC& markerDesc = MARKER_DESC());
        
        virtual void SetLoadingState(MarkerLoadingState state)
        {
            mState = state;
        }
        
        virtual void RemoveAdditionalChildren();
        
        MarkerLoadingState GetLoadingState() { return mState;}
        
        bool IsLoaded() { return mState == MarkerStateLoaded;}
        bool IsLoading() { return mState == MarkerStateLoading;}
        bool IsIdle() { return mState == MarkerStateIdle;}
        
    private:
        
        MarkerLoadingState mState;
    };
}