// -1, -1
// -1,  1
//  1, -1
//  1,  1
attribute vec2 a_direction;

varying vec2 v_texCoord;

uniform mat4 u_mvp;
uniform vec2 u_widths;
uniform vec2 u_position;

void main()
{
    vec2 v2Pos = u_position + (a_direction * u_widths);
    gl_Position = u_mvp * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
    v_texCoord = a_direction;
}