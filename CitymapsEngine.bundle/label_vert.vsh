attribute vec4 a_posTex;
attribute float a_lineNum;

uniform mat4 u_mvp;
uniform vec4 u_originAngle;
uniform vec2 u_position;
uniform float u_scale;
uniform float u_lineOffsets[20];

varying mediump vec2 v_texCoord;

void main()
{
    int lineInt = int(a_lineNum);
    float lineOffset = u_lineOffsets[lineInt];
	v_texCoord = a_posTex.zw;
    
    vec2 transPos = a_posTex.xy - u_originAngle.xy;
    transPos.x += lineOffset;
    float sinA = u_originAngle.z;
    float cosA = u_originAngle.w;
    
    vec2 finalPos;
    finalPos.x = (cosA * transPos.x) - (sinA * transPos.y);
    finalPos.y = (sinA * transPos.x) + (cosA * transPos.y);
    
    vec2 position = u_position + u_scale * finalPos;
	gl_Position = u_mvp * vec4(position, 0.0, 1.0);
}
