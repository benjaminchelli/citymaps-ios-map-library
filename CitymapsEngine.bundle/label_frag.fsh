precision mediump float;

uniform sampler2D u_glyphAtlasTexture;
uniform float u_alpha;
uniform vec4 u_color;

varying mediump vec2 v_texCoord;

void main()
{
	float alpha = texture2D(u_glyphAtlasTexture, v_texCoord).a;
    gl_FragColor = u_color * alpha * u_alpha;
}