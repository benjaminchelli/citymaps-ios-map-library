precision lowp float;

varying vec4 v_color;

void main()
{
    gl_FragColor = vec4(v_color.xyz * v_color.a, v_color.a);
}
