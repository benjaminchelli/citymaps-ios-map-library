attribute vec4 a_posAndPerp;
attribute vec2 a_layerAndU;

varying vec4 v_color;

uniform mat4 u_mvp;
uniform vec4 u_colors[75];
uniform float u_widths[75];

void main()
{
    int layerIdInt = int(a_layerAndU.x);
    float width = u_widths[layerIdInt];
    v_color = u_colors[layerIdInt];
    
    vec2 v2Pos = a_posAndPerp.xy + (a_posAndPerp.zw * width);
    gl_Position = u_mvp * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
}