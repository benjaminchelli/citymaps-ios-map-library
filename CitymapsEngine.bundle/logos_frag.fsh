varying lowp vec2 TexCoord;

uniform sampler2D Texture; 
uniform sampler2D PinTexture;
uniform lowp float LogoAlpha;
uniform lowp float PinAlpha;

void main()
{
    //lowp vec4 pinColor = texture2D(PinTexture, gl_PointCoord);
    //lowp vec4 logoColor = texture2D(Texture, gl_PointCoord);
    //lowp vec4 pinColor = texture2D(PinTexture, TexCoord);
    //lowp vec4 logoColor = texture2D(Texture, TexCoord);
    //lowp vec4 whiteColor = vec4(1.0, 1.0, 1.0, 1.0);

    //lowp float r = floor(pinColor.r);
    //lowp float b = floor(pinColor.b);
    //lowp float g = clamp(1.0 - ceil(pinColor.g - 0.7), 0.0, 1.0);
    
    //lowp float colorMult = r * g * b;

    //pinColor *= abs(1.0 - colorMult);
    //logoColor *= colorMult;
    
    //lowp float transparentMult = float((logoColor.r == 0.0) && (logoColor.g == 0.0) && (logoColor.b == 0.0) && (logoColor.a == 0.0));
    //whiteColor *= transparentMult;
    
    //pinColor.a *= PinAlpha;
    //logoColor.a *= LogoAlpha;
    
    //lowp vec4 finalColor = pinColor + logoColor;
    //finalColor.a *= PinAlpha;
    
    lowp vec4 finalColor = texture2D(Texture, TexCoord);
    finalColor.a *= PinAlpha;

    gl_FragColor = finalColor;
    //gl_FragColor = vec4(r, g, b, 1.0);
    //gl_FragColor = vec4(TexCoord.x, TexCoord.y, 1.0, 1.0);
    //gl_FragColor = vec4(gl_TexCoord[0].x, gl_TexCoord[0].y, 1.0, 1.0);
    //gl_FragColor = pinColor;
    //gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
