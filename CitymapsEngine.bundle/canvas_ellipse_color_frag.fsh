precision mediump float;

varying vec2 v_texCoord;

uniform vec4 u_fillColor;
uniform vec4 u_strokeColor;
uniform float u_outlineCutoff;

void main()
{
    float length = length(v_texCoord);
    float outside = step(1.0, length);
    
    if (outside > 0.1)
        discard;

    if (length > u_outlineCutoff)
        gl_FragColor = vec4(u_strokeColor.xyz * u_strokeColor.a, u_strokeColor.a);
    else
        gl_FragColor = vec4(u_fillColor.xyz * u_fillColor.a, u_fillColor.a);
}
